package de.paul2708.cloud.groupme;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.groupme.Bot;

import java.util.concurrent.Executors;

/**
 * Created by Paul on 05.01.2017.
 */
public class GroupMeManager {

    private Bot bot;

    public void login() {
        this.bot = new Bot(CloudServer.getAccountFile().getGroupMeToken());
    }

    public void sendMessage(Player player, String message) {
        String format = message.replaceAll("ß", "ss")
                    .replaceAll("ö", "oe")
                    .replaceAll("ä", "ae")
                    .replaceAll("ü", "ue");

        Executors.newSingleThreadExecutor().submit(() -> bot.sendTextMessage(format));

        player.sendMessage(CloudServer.TAG + "§7Der Bug wurde gemeldet. §eDanke!");

        CloudLogger.info("Bug: " + message);
    }
}

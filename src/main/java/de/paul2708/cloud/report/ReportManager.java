package de.paul2708.cloud.report;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.report.Report;
import lombok.Getter;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 18.08.2016.
 */
public class ReportManager {

    private List<Report> reports = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<Report> openReports = new CopyOnWriteArrayList<>();
    private int totalReports = 0;

    private Map<String, Long> delay = new HashMap<>();
    @Getter
    private Map<Player, Player> reportMessage = new HashMap<>();

    public void report(Report report) {
        reports.add(report);
        // Check existing reports
        int count = 1;
        List<Report> temp = new ArrayList<>();
        for (Report open : openReports) {
            if (open.getTargetName().equalsIgnoreCase(report.getTargetName())) {
                count++;
                temp.add(open);
            }
        }
        temp.forEach(tempReport -> openReports.remove(tempReport));
        report.amount(count);

        openReports.add(report);

        delay.put(report.getReporterName(), System.currentTimeMillis());

        totalReports++;

        String message = CloudServer.TAG +
                CloudServer.getPlayerManager().getPlayer(report.getReporterName()).getRank().getPrefix() + report.getReporterName() + " §7hat " +
                CloudServer.getPlayerManager().getPlayer(report.getTargetName()).getRank().getPrefix() + report.getTargetName() + " §7wegen §e" + report.getCause().getName() + " §7reportet.";

        for (Player online : CloudServer.getPlayerManager().getOnlinePlayers()) {
            if (online.hasNotification() && online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
                online.sendClickableMessage(message, "§eÜbernehme den Report!", "/takereport " + report.getTargetName(), "");
            }
        }

        reportMessage.put(CloudServer.getPlayerManager().getPlayer(report.getReporterName()),
                CloudServer.getPlayerManager().getPlayer(report.getTargetName()));

        CloudLogger.info("Report: " + totalReports + ". Report (seit Cloud-Start) von " +
                CloudServer.getPlayerManager().getPlayer(report.getReporterName()).getName());
    }

    public void removeReport(Report report) {
        Report temp = null;
        for (Report all : openReports) {
            if (all.getTargetName().equalsIgnoreCase(report.getTargetName())) {
                temp = all;
                break;
            }
        }
        openReports.remove(temp);

        for (Player online : CloudServer.getPlayerManager().getOnlinePlayers()) {
            if (online.hasNotification() && online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
                online.sendMessage(CloudServer.TAG + "§7Der Report gegen " +
                        CloudServer.getPlayerManager().getPlayer(report.getTargetName()).getRank().getPrefix() + report.getTargetName() +
                        " §7(§e" + report.getCause().getName() + "§7) §7wurde übernommen.");
            }
        }

        CloudLogger.info("Report: Report (" + report.getTargetName() + ") wird bearbeitet");
    }

    public boolean canReport(Player player) {
        if (delay.get(player.getName()) == null) {
            return true;
        } else {
            long timeStamp = delay.get(player.getName());
            return timeStamp + (1000 * 60 * 1) <= System.currentTimeMillis();
        }
    }

    public void startReportBroadcast() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if (openReports.size() == 0) {
                    return;
                }

                for (Player online : CloudServer.getPlayerManager().getOnlinePlayers()) {
                    if (online.hasNotification() && online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
                        if (openReports.size() == 1) {
                            online.sendMessage(CloudServer.TAG + "§7Es ist §eein Report §7offen. §e/reports");
                        } else {
                            online.sendMessage(CloudServer.TAG + "§7Es sind §e" + openReports.size() + " Reports §7offen. §e/reports");
                        }
                    }
                }
            }
        }, TimeUnit.MINUTES.toMillis(3), TimeUnit.MINUTES.toMillis(3));
    }

    public ArrayList<Report> getOpenReports() {
        ArrayList<Report> list = new ArrayList<>();
        list.addAll(openReports);
        return list;
    }

    public List<Report> getReports() {
        return reports;
    }
}

package de.paul2708.cloud.achievement;

import de.paul2708.common.achievement.Achievement;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 11.03.2017.
 */
public class AchievementData {

    private String game;
    private List<Achievement> achievements;

    public AchievementData(String game) {
        this.game = game;
        this.achievements = new CopyOnWriteArrayList<>();
    }

    public void add(Achievement achievement) {
        this.achievements.add(achievement);
    }

    public boolean contains(int achievementId) {
        for (Achievement all : achievements) {
            if (all.getId() == achievementId) {
                return true;
            }
        }

        return false;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public String getGame() {
        return game;
    }
}

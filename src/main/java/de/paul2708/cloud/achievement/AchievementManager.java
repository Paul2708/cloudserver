package de.paul2708.cloud.achievement;

import de.paul2708.cloud.CloudServer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 11.03.2017.
 */
public class AchievementManager {

    private Map<UUID, List<AchievementData>> achievements = new ConcurrentHashMap<>();

    public void createTable(String game) {
        String query = "CREATE TABLE IF NOT EXISTS `" + game.toLowerCase() + "_achievements` ("
                + "`name` varchar(16) NOT NULL,"
                + "`uuid` varchar(200) NOT NULL,"
                + "`achievements` varchar (200) NOT NULL"
                + "UNIQUE KEY `uuid` (`uuid`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        CloudServer.getMySQL().queryUpdate(query);
    }

    public void addAchievement(UUID uuid, String game, int achievementId) {
        AchievementData data = getAchievementData(uuid, game);

        if (!data.contains(achievementId)) {
            // data.add(achievementId);
        }
    }

    public AchievementData getAchievementData(UUID uuid, String game) {
        List<AchievementData> data = achievements.get(uuid);

        if (data == null) {
            data = new ArrayList<>();
            data.add(new AchievementData(game));

            achievements.put(uuid, new ArrayList<>());

            return data.get(0);
        } else {
            for (AchievementData all : data) {
                if (all.getGame().equalsIgnoreCase(game)) {
                    return all;
                }
            }
        }

        return null;
    }
}

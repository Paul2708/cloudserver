package de.paul2708.cloud.survey;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.survey.SurveyOpenPacket;
import de.paul2708.common.survey.Question;
import de.paul2708.common.survey.Result;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Paul on 17.06.2017.
 */
public class SurveyManager {

    private Survey survey;

    public void resolveSurvey() {
        boolean created = createFile();

        if (created) {
            this.survey = createDefault();
            this.survey.save();

            CloudLogger.info("Survey: Standard wurde geladen");
        } else {
            String context = Util.getContext(Paths.get(CloudServer.getConfigFile().getPathToCloud() + "survey.json"));
            this.survey = resolve(context);

            CloudLogger.info("Survey: Umfrage wurde geladen");
        }
    }

    public boolean hasVoted(Player player) {
        return survey.contains(UUID.fromString(player.getUuid()));
    }

    public void vote(Player player, Result result) {
        survey.apply(UUID.fromString(player.getUuid()), result);
        survey.save();

        player.sendMessage(CloudServer.TAG + "§eDanke, dass du dir kurz Zeit genommen hast. (+" + survey.getReward() + " Tokens)");
        player.updateTokens(survey.getReward());
    }

    public boolean isActive() {
        return survey.isActive();
    }

    public void reload() {
        this.survey = null;

        resolveSurvey();
        for (SpigotServer server : CloudServer.getServerManager().getServers()) {
            if (CloudServer.getServerManager().isLobbyServer(server)) {
                server.sendPacket(new SurveyOpenPacket(UUID.randomUUID(), new ArrayList<>()));
            }
        }
    }

    public Survey getSurvey() {
        return survey;
    }

    private boolean createFile() {
        Path path = Paths.get("survey.json");

        try {
            // Create file
            if (!Files.exists(path)) {
                Files.createFile(path);
                return true;
            }
        } catch (Exception e) {
            CloudLogger.error("File: Survey-Datei konnte nicht erstellt werden", e);
        }

        return false;
    }

    public Survey createDefault() {
        boolean active = true;
        String topic = "Willkommen";
        int reward = 500;

        Question[] array = new Question[3];
        array[0] = question("Wie bist du auf das Netzwerk gekommen?", "Sozial Media", "Twitch/Youtube", "Freunde", "Sonstiges");
        array[1] = question("Was ist dein Lieblingsspielmodus?", "BedWars", "Quiz", "Nexus");
        array[2] = question("Würdest du Ozeangames weiterempfehlen?", "Ja klar!", "Nein..", "Ich weiss es nicht.");

        return new Survey(active, topic, reward, array, new ArrayList<>());
    }

    private Question question(String question, String... answers) {
        Map<String, Integer> map = new HashMap<>();
        for (String answer : answers) {
            map.put(answer, 0);
        }

        return new Question(question, map);
    }

    private Survey resolve(String context) {
        JsonObject header = Jsoner.deserialize(context, new JsonObject());

        // Information
        JsonObject information = (JsonObject) header.get("information");
        boolean active = information.getBoolean("active");
        String topic = information.getString("topic");
        int reward = information.getInteger("reward");

        // Questions
        JsonArray questionArray = (JsonArray) header.get("questions");
        Question[] questions = new Question[questionArray.size()];

        for (int i = 0; i < questionArray.size(); i++) {
            JsonObject object = (JsonObject) questionArray.get(i);
            questions[i] = Question.build(object);
        }

        // Voter
        JsonArray voterArray = (JsonArray) header.get("voters");
        List<UUID> voters = new ArrayList<>();

        for (int i = 0; i < voterArray.size(); i++) {
            voters.add(UUID.fromString(voterArray.getString(i)));
        }

        return new Survey(active, topic, reward, questions, voters);
    }
}

package de.paul2708.cloud.survey;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.common.survey.Question;
import de.paul2708.common.survey.Result;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Paul on 17.06.2017.
 */
public class Survey {

    private boolean active;
    private String topic;
    private int reward;

    private Question[] questions;

    private List<UUID> voters;

    public Survey(boolean active, String topic, int reward, Question[] questions, List<UUID> voters) {
        this.active = active;
        this.topic = topic;
        this.reward = reward;

        this.questions = questions;

        this.voters = voters;
    }

    public boolean contains(UUID uuid) {
        return voters.contains(uuid);
    }

    public void apply(UUID uuid, Result result) {
        voters.add(uuid);

        Map<Integer, String> map = result.getResult();

        for (int i = 0; i < questions.length; i++) {
            String answer = map.get(i);

            int current = questions[i].getVotes(answer);
            current++;

            questions[i].setVotes(answer, current);
        }
    }

    public void save() {
        JsonObject survey = new JsonObject();

        // Basic
        JsonObject information = new JsonObject();
        information.put("active", active);
        information.put("topic", topic);
        information.put("reward", reward);

        // Question
        JsonArray array = new JsonArray();

        for (int i = 0; i < questions.length; i++) {
            array.add(questions[i]);
        }

        // Voters
        JsonArray voters = new JsonArray();

        for (int i = 0; i < this.voters.size(); i++) {
            voters.add(this.voters.get(i).toString());
        }

        // Add to header
        survey.put("information", information);
        survey.put("questions", array);
        survey.put("voters", voters);

        // Write to file
        try {
            String path = CloudServer.getConfigFile().getPathToCloud() + "survey.json";

            FileWriter writer = new FileWriter(path);
            writer.write(Jsoner.prettyPrint(survey.toJson()));
            writer.flush();
        } catch (IOException e) {
            CloudLogger.error("Survey: Fehler beim Speichern", e);
        }
    }

    public boolean isActive() {
        return active;
    }

    public String getTopic() {
        return topic;
    }

    public int getReward() {
        return reward;
    }

    public Question[] getQuestions() {
        return questions;
    }

    public List<UUID> getVoters() {
        return voters;
    }

}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.chatlog.Chatlog;
import de.paul2708.common.packet.other.ChatlogPacket;

import java.util.ArrayList;

/**
 * Created by Paul on 03.09.2016.
 */
public class ChatlogCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/chatlog <Spieler>");
            return;
        }

        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);

        // Check valid arguments
        if (!CloudServer.getChatlogManager().canReport(player)) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst nur jede Minute einen Chatlog erstellen.");
            return;
        }
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst keinen Chatlog von dir selbst erstellen.");
            return;
        }

        // Report request
        Chatlog chatlog = new Chatlog(player.getUuid(), target.getUuid(),
                player.getName(), target.getName(), new ArrayList<>(), 1L);

        CloudServer.getChatlogManager().report(player);
        target.getServer().sendPacket(new ChatlogPacket(chatlog));

        player.sendMessage(CloudServer.TAG + "§7Ein Chatlog für " + target.getRank().getPrefix() + target.getName() + " §7wird erstellt...");
    }
}

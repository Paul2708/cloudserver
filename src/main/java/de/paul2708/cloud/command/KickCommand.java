package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.abuse.AbuseAction;

import java.util.UUID;

/**
 * Created by Paul on 11.09.2016.
 */
public class KickCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 100) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/kick <Spieler> [Grund]");
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst dich nicht selbst kicken.");
            return;
        }
        if (target.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                !(player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getAdminPermissionLevel())) {
            player.sendMessage(CloudServer.TAG + "§cDu darfst den Spieler " + target.getRank().getPrefix() + target.getName() +
                    " §cnicht kicken.");
            return;
        }

        String reason = "";
        if (args.length != 1) {
            for (int i = 1; i < args.length; i++) {
                reason += args[i] + " ";
            }

            reason = reason.replaceAll("&", "§");
        } else {
            reason = "Kein Grund angegeben";
        }

        // Kick player
        Abuse abuse = new Abuse(AbuseAction.KICK, player.getName(), target.getName(), reason,
                System.currentTimeMillis(), -1L, UUID.fromString(player.getUuid()),
                UUID.fromString(target.getUuid()));
        CloudServer.getAbuseManager().addAbuse(abuse);

        target.kick("§cDu wurdest gekickt.\n\n§7Grund: §e" + reason);

        for (Player online : CloudServer.getPlayerManager().getOnlinePlayers()) {
            if (online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                    online.hasNotification()) {
                online.sendMessages(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + " §7hat " +
                                target.getRank().getPrefix() + target.getName() + " §7vom Netzwerk gekickt.",
                        CloudServer.TAG + "§7Grund: §e" + reason);
            }
        }
    }
}

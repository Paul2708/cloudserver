package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.template.Template;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Paul on 28.11.2016.
 */
public class TemplateCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            sendHelp(player);
            return;
        }

        /*
            /template list
            /template info <Name>
            /template start <Name|All> [Anzahl]
            /template stop <Name>
            /template kill <Servername>
         */

        Template template;
        List<String> message;

        if (args[0].equalsIgnoreCase("list")) {
            // List all templates
            String templates = CloudServer.getTemplateManager().getTemplates().stream()
                    .map(Template::getName).collect(Collectors.joining("§7, §e"));
            player.sendMessage(CloudServer.TAG + "§7Geladene Templates: §e" + templates);
        } else if (args[0].equalsIgnoreCase("info")) {
            // Give info for template
            if (args.length == 2) {
                template = CloudServer.getTemplateManager().getTemplate(args[1]);
                if (template == null) {
                    player.sendMessage(CloudServer.TAG + "§cDas Template '" + args[1] + "' existiert nicht.");
                } else {
                    message = new ArrayList<>();
                    message.add(CloudServer.TAG + "§eInformationen §7zu dem Template §e" + template.getName() + "§7:");
                    message.add(CloudServer.TAG + "§7Name: §e" + template.getName());
                    message.add(CloudServer.TAG + "§7Ram (pro Server): §e" + template.getRam() + "MB");
                    message.add(CloudServer.TAG + "§7Mininamle/Maximale Anzahl: §e" + template.getMinServer() + "/" + template.getMaxServer());

                    long count = CloudServer.getServerManager().getServers().stream().filter(server -> server.getName().startsWith(template.getName())).count();
                    message.add(CloudServer.TAG + "§7Laufende Server: §e" + count + " Server");

                    player.sendMessages(message);
                }
            } else {
                sendHelp(player);
            }
        } else if (args[0].equalsIgnoreCase("start")) {
            // Start templates
            if (args.length == 2) {
                if (args[1].equalsIgnoreCase("all")) {
                    player.sendMessage(CloudServer.TAG + "§eAlle Templates werden nun gestartet...");
                    CloudServer.getTemplateManager().startAll();
                } else {
                    template = CloudServer.getTemplateManager().getTemplate(args[1]);
                    if (template == null) {
                        player.sendMessage(CloudServer.TAG + "§cDas Template '" + args[1] + "' existiert nicht.");
                    } else {
                        player.sendMessage(CloudServer.TAG + "§7Du hast einen §e" + template.getName() + "§7-Server gestartet..");
                        CloudServer.getTemplateManager().start(template, 1);
                    }
                }
            } else if (args.length == 3) {
                template = CloudServer.getTemplateManager().getTemplate(args[1]);
                if (template == null) {
                    player.sendMessage(CloudServer.TAG + "§cDas Template '" + args[1] + "' existiert nicht.");
                } else {
                    try {
                        int amount = Integer.valueOf(args[2]);

                        if (amount <= 0) {
                            player.sendMessage(CloudServer.TAG + "§cDu kannst nur mindestens einen Server starten.");
                            return;
                        }

                        if (amount > template.getMaxServer()) {
                            player.sendMessage(CloudServer.TAG + "§cDu kannst nicht mehr Server starten als maximal eingetragen");
                        } else {
                            player.sendMessage(CloudServer.TAG + "§7Du hast §e" + amount + " " + template.getName() + "§7-Server gestartet...");
                            CloudServer.getTemplateManager().start(template, amount);
                        }
                    } catch (NumberFormatException e) {
                        player.sendMessage(CloudServer.TAG + "§c'" + args[2] + "' ist keine gültige Zahl.");
                    }
                }
            } else {
                sendHelp(player);
            }
        } else if (args[0].equalsIgnoreCase("stop")) {
            // Stop template
            if (args.length == 2) {
                template = CloudServer.getTemplateManager().getTemplate(args[1]);
                if (template == null) {
                    player.sendMessage(CloudServer.TAG + "§cDas Template '" + args[1] + "' existiert nicht.");
                } else {
                    CloudServer.getTemplateManager().kill(template);
                    player.sendMessage(CloudServer.TAG + "§7Du hast alle §e" + template.getName() + "§7-Server gestoppt.");
                }
            } else {
                sendHelp(player);
            }
        } else if (args[0].equalsIgnoreCase("kill")) {
            if (args.length == 2) {
                SpigotServer server = CloudServer.getServerManager().getServer(args[1]);
                if (server == null) {
                    player.sendMessage(CloudServer.TAG + "§cDer Server '" + args[1] + "' wurde nicht gefunden.");
                } else {
                    if (server.getTemplate() == null) {
                        player.sendMessage(CloudServer.TAG + "§cDer Server ist kein Template-Server.");
                        return;
                    }

                    CloudServer.getTemplateManager().killAndDelete(server, true, true);
                    player.sendMessage(CloudServer.TAG + "§7Du hast den Server §e" + server.getName() + " §7gekillt.");
                }
            } else {
                sendHelp(player);
            }
        } else {
            sendHelp(player);
        }
    }

    private void sendHelp(Player player) {
        player.sendMessage(CloudServer.TAG + "§7Nutze §e/template <list|info|start|stop|kill> [Name|All|Servername] [Anzahl]");
    }
}

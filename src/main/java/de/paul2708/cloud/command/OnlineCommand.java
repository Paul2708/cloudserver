package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

import java.util.stream.Collectors;

/**
 * Created by Paul on 31.08.2016.
 */
public class OnlineCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Get players
        if(CloudServer.getPlayerManager().getOnlinePlayers().size() == 1) {
            player.sendMessage(CloudServer.TAG + "§7Momentan ist §e" + CloudServer.getPlayerManager().getOnlinePlayers().size() + " §7Spieler auf dem Netzwerk." );
        } else {
            player.sendMessage(CloudServer.TAG + "§7Momentan sind §e" +
                    CloudServer.getPlayerManager().getOnlinePlayers().size() + " §7Spieler auf dem Netzwerk.");
        }

        if (player.getRank().getPermissionLevel() >= 110) {
            String names = CloudServer.getPlayerManager().getOnlinePlayers().stream().map(Player::getName).collect(Collectors.joining("§7, §e"));
            player.sendMessage(CloudServer.TAG + "§7Spieler: §e" + names);
        }
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.server.MaxPlayerPacket;

/**
 * Created by Paul on 05.02.2017.
 */
public class SetMaxPlayersCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            CloudServer.getConfigFile().reload();
            int count = CloudServer.getConfigFile().getMaxPlayers();

            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new MaxPlayerPacket(count));
            player.sendMessage(CloudServer.TAG + "§eDu hast die maximale Spieleranzahl neu geladen.");

            CloudLogger.info("Count: max. " + count + " Spieler");
        } else if (args.length == 1) {
            int count;

            try {
                count = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                player.sendMessage(CloudServer.TAG + "§cDeine Angabe war ungültig.");
                return;
            }

            count = Math.abs(count);

            CloudServer.getConfigFile().set("max_players", count);
            CloudServer.getConfigFile().save();

            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new MaxPlayerPacket(count));
            player.sendMessage(CloudServer.TAG + "§eDu hast die maximale Spieleranzahl auf " + count + " gesetzt.");

            CloudLogger.info("Count: max. " + count + " Spieler");
        } else {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/setmaxplayers [Spieleranzahl]");
        }
    }
}

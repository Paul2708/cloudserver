package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 31.08.2016.
 */
public class CloudInfoCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 110) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Send stats
        List<String> message = new ArrayList<>();
        message.add(CloudServer.TAG + "§eInformationen §7über die §3OG-Cloud§7: ");
        message.add(CloudServer.TAG + "§7Start-Zeitpunkt: §e" + Util.getTime(CloudServer.getStartTime()));
        message.add(CloudServer.TAG + "§7Läuft seit: §e" + getHours() + " Stunden");
        message.add(CloudServer.TAG + "§7Registrierte Spieler: §e" + CloudServer.getPlayerManager().getPlayers().size());
        message.add(CloudServer.TAG + "§7Reports: §e" + CloudServer.getReportManager().getReports().size());
        message.add(CloudServer.TAG + "§7Chatlogs: §e" + CloudServer.getChatlogManager().getChatlogs().size());

        player.sendMessages(message);
    }

    private long getHours() {
        long diff = System.currentTimeMillis() - CloudServer.getStartTime();
        return TimeUnit.MILLISECONDS.toHours(diff);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.command.BroadcastPacket;

/**
 * Created by Paul on 16.08.2016.
 */
public class BroadcastCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/broadcast <Nachricht>");
            return;
        }

        String broadcast = "";

        for (String arg : args) {
            broadcast += arg + " ";
        }

        broadcast = broadcast.replaceAll("&", "§");

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new BroadcastPacket(broadcast));
        CloudLogger.info("Broadcast: " + broadcast);
    }
}

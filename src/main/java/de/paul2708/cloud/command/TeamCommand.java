package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Paul on 31.08.2016.
 */
public class TeamCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Get team
        List<Player> online = new ArrayList<>();
        List<Player> offline = new ArrayList<>();
        CloudServer.getPlayerManager().getPlayers().values().stream().filter(all -> all.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).forEach(all -> {
            if (all.isOnline()) {
                online.add(all);
            } else {
                offline.add(all);
            }
        });

        List<String> message = new ArrayList<>();
        message.add(CloudServer.TAG + "§7Folgende Teammitglieder sind §aonline§7:");
        message.addAll(Util.sort(online).stream().map(all -> "   §7- " + all.getRank().getPrefix() + all.getName()).collect(Collectors.toList()));
        message.add(CloudServer.TAG + "§7Folgende Teammitglieder sind §coffline§7:");
        message.addAll(Util.sort(offline).stream().map(all -> "   §7- " + all.getRank().getPrefix() + all.getName()).collect(Collectors.toList()));

        player.sendMessages(message);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 13.09.2016.
 */
public class BanCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 100) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0 || args.length == 1) {
            sendHelp(player);
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null) {
            player.sendMessages(CloudServer.TAG + "§cDer Spieler " + args[0] + " hat das Netzwerk noch nie betreten.",
                    CloudServer.TAG + "§7Falls du ihn dennoch sperren möchtest, nutze §e/blacklist");
            return;
        }
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst dich nicht selbst bannen.");
            return;
        }
        if (target.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                !(player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getAdminPermissionLevel())) {
            player.sendMessage(CloudServer.TAG + "§cDu darfst den Spieler " + target.getRank().getPrefix() + target.getName() +
                    " §cnicht bannen.");
            return;
        }

        if (args.length == 2) {
            if (args[1].equalsIgnoreCase("perma") || args[1].equalsIgnoreCase("permanent")) {
                CloudServer.getAbuseManager().ban(player, target, "Kein Grund angegeben", -1L);
            } else {
                sendHelp(player);
            }
        } else {
            if (args[1].equalsIgnoreCase("perma") || args[1].equalsIgnoreCase("permanent")) {
                String reason = "";
                for (int i = 2; i < args.length; i++) {
                    reason += args[i] + " ";
                }
                reason = reason.replaceAll("&", "§");

                CloudServer.getAbuseManager().ban(player, target, reason, -1L);
            } else {
                long duration;
                try {
                    duration = Integer.valueOf(args[1]);
                } catch (NumberFormatException e) {
                    player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[1] + "' ist nicht korrekt.");
                    return;
                }

                switch (args[2]) {
                    case "m": case "M":
                        duration = duration * 60;
                        break;
                    case "h": case "H":
                        duration = duration * 60 * 60;
                        break;
                    case "d": case "D":
                        duration = duration * 60 * 60 * 24;
                        break;
                    case "w": case "W":
                        duration = duration * 60 * 60 * 24 * 7;
                        break;
                    default:
                        player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[2] + "' ist nicht korrekt.");
                        return;
                }

                duration = duration * 1000;

                String reason = "";
                if (args.length > 3) {
                    for (int i = 3; i < args.length; i++) {
                        reason += args[i] + " ";
                    }
                    reason = reason.replaceAll("&", "§");
                } else {
                    reason = "-/-";
                }

                CloudServer.getAbuseManager().ban(player, target, reason, System.currentTimeMillis() + duration);
            }
        }
    }

    private void sendHelp(Player player) {
        player.sendMessages(CloudServer.TAG + "§7Nutze §e/ban <Spieler> perma [Grund]",
                CloudServer.TAG + "§7§nOder:§r §e/ban <Spieler> <Dauer> <m|h|d|w> [Grund]");
    }
}

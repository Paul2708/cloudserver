package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.abuse.AbuseAction;

import java.util.UUID;

/**
 * Created by Paul on 03.10.2016.
 */
public class UnMuteCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 110) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/unmute <Spieler>");
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null || CloudServer.getAbuseManager().isMuted(target) == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist nicht gemutet.");
            return;
        }

        // Get server info
        Abuse abuse = new Abuse(AbuseAction.UNMUTE, player.getName(), target.getName(), "-/-", System.currentTimeMillis(),
                -1L, UUID.fromString(player.getUuid()), UUID.fromString(target.getUuid()));
        CloudServer.getAbuseManager().addAbuse(abuse);

        target.sendMessage(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + " §ehat dich entmutet.");

        CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(online -> online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).filter(online -> online.hasNotification()).forEach(online -> {
            online.sendMessage(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + " §7hat " +
                    target.getRank().getPrefix() + target.getName() + " §7entmutet.");
        });
    }
}

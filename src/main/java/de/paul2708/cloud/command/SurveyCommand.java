package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.survey.Survey;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.survey.SurveyOpenPacket;
import de.paul2708.common.survey.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Paul on 31.08.2016.
 */
public class SurveyCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check player permission
        if (player.getRank().getPermissionLevel() < 100) {
            if (args.length != 0) {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/survey");
                return;
            }
            if (!isActive()) {
                player.sendMessage(CloudServer.TAG + "§cZur Zeit gibt es keine offene Umfrage.");
                return;
            }
            if (CloudServer.getSurveyManager().hasVoted(player)) {
                player.sendMessage(CloudServer.TAG + "§cDu hast schon bei der Umfrage teilgenommen.");
                return;
            }
            if (!CloudServer.getServerManager().isLobbyServer(player.getServer())) {
                player.sendMessage(CloudServer.TAG + "§cDu kannst nur in der Lobby teilnehmen.");
                return;
            }

            sendPacket(player);
            player.sendMessage(CloudServer.TAG + "§7Thema: §e" + CloudServer.getSurveyManager().getSurvey().getTopic() +
                    " §7(Belohnung: §e+" + CloudServer.getSurveyManager().getSurvey().getReward() + " Tokens§7)");
        } else {
            // Admin rank
            if (args.length == 0) {
                if (!isActive()) {
                    player.sendMessage(CloudServer.TAG + "§cZur Zeit gibt es keine offene Umfrage.");
                    return;
                }
                if (CloudServer.getSurveyManager().hasVoted(player)) {
                    player.sendMessage(CloudServer.TAG + "§cDu hast schon bei der Umfrage teilgenommen.");
                    return;
                }
                if (!CloudServer.getServerManager().isLobbyServer(player.getServer())) {
                    player.sendMessage(CloudServer.TAG + "§cDu kannst nur in der Lobby teilnehmen.");
                    return;
                }

                sendPacket(player);
                player.sendMessage(CloudServer.TAG + "§7Thema: §e" + CloudServer.getSurveyManager().getSurvey().getTopic() +
                        " §7(Belohnung: §e+" + CloudServer.getSurveyManager().getSurvey().getReward() + " Tokens§7)");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("result")) {
                    List<String> message = new ArrayList<>();
                    Survey survey = CloudServer.getSurveyManager().getSurvey();
                    double totalVotes = survey.getVoters().size() * 1.0;

                    if (totalVotes == 0) {
                        player.sendMessage(CloudServer.TAG + "§cEs hat noch niemand an der Umfrage teilgenommen.");
                        return;
                    }

                    message.add(CloudServer.TAG + "§7Ergebnis der Umfrage: §e" + survey.getTopic());

                    for (int i = 0; i < survey.getQuestions().length; i++) {
                        Question question = survey.getQuestions()[i];
                        Map<String, Integer> result = question.getAnswers();
                        List<String> answers = new ArrayList<>(result.keySet());

                        message.add(CloudServer.TAG + "§7" + (i + 1) + ". Frage: §e" + question.getQuestion());

                        for (int j = 0; j < answers.size(); j++) {
                            double votes = question.getVotes(answers.get(j)) * 1.0;
                            double percentage = (votes / totalVotes) * 100;

                            message.add(CloudServer.TAG + "   §7" + String.valueOf((char) (j + 97)) + ") §e" + answers.get(j) + "  §2(" + Util.round(percentage) + "%)");
                        }
                    }

                    message.add(CloudServer.TAG + "§7An der Umfrage haben §e" + ((int) totalVotes) + " §7Spieler teilgenommen.");

                    player.sendMessages(message);
                } else if (args[0].equalsIgnoreCase("load")) {
                    CloudServer.getSurveyManager().reload();

                    player.sendMessage(CloudServer.TAG + "§7Die Umfragen wurden neu geladen.");
                } else {
                    player.sendMessage(CloudServer.TAG + "§7Nutze §e/survey [result|load]");
                }
            } else {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/survey [result|load]");
            }
        }
    }

    private void sendPacket(Player player) {
        SpigotServer server = player.getServer();

        if (CloudServer.getServerManager().isLobbyServer(server)) {
            Question[] questions = CloudServer.getSurveyManager().getSurvey().getQuestions().clone();

            List<Question> list = new ArrayList<>();

            for (int i = 0; i < questions.length; i++) {
                list.add(questions[i]);
            }

            SurveyOpenPacket packet = new SurveyOpenPacket(UUID.fromString(player.getUuid()), list);
            server.sendPacket(packet);
        } else {
            player.sendMessage(CloudServer.TAG + "§cDu kannst Umfragen nur in der Lobby bearbeiten.");
        }
    }

    private boolean isActive() {
        return CloudServer.getSurveyManager().isActive();
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 31.08.2016.
 */
public class NickCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/nick");
            return;
        }

        // Random nick
        player.sendMessage(CloudServer.TAG + "§cDas Nick-System ist noch nicht verfügbar.");

        /*Nick nick = CloudServer.getNickManager().getFreeNick();

        if (nick == null) {
            player.sendMessage(CloudServer.TAG + "§cAlle Nicks sind bereits in Verwendung.");
        } else {
            CloudServer.getNickManager().nick(player, nick);

            player.sendMessage(CloudServer.TAG + "§7Dein Nickname ist: §e" + nick.getName());
        }*/
    }

}

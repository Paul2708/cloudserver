package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.template.Template;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Paul on 31.08.2016.
 */
public class LogCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/log <Cloud|Template-Server>");
            return;
        }

        // Check valid arguments
        Path path;
        if (args[0].equalsIgnoreCase("cloud")) {
            path = Paths.get(CloudServer.getLogFile().getPath());

            player.sendMessage(CloudServer.TAG + "§7Der Log für den §eCloud-Server §7wird hochgeladen...");
        } else {
            Template template = CloudServer.getTemplateManager().getTemplate(args[0]);
            if (template == null) {
                player.sendMessage(CloudServer.TAG + "§cDas Template " + args[0] + " existiert nicht.");
                return;
            }

            path = Paths.get("serverlogs/" + template.getName() + "/latest.log");
            if (path.toFile() == null || !path.toFile().exists()) {
                player.sendMessage(CloudServer.TAG + "§cEs konnte kein aktueller Log gefunden werden.");
                return;
            }

            player.sendMessage(CloudServer.TAG + "§7Der Log für §e" + template.getName() + " §7wird hochgeladen...");
        }

        String content = "";
        try {
            for (String line : Files.readAllLines(path)) {
                line = line.trim();
                content += line + "<br>";
            }
        } catch (Exception e) {
            CloudLogger.error("File: Log von " + path.toString() + " konnte nicht ausgelesen werden", e);
        }

        /*if (content.length() > 40000) {
            player.sendMessage(CloudServer.TAG + "Da der Log §e" + content.length() + " Zeichen §7hat, wird er auf 40k gekürzt.");
            content = content.substring(content.length() - 40000);
        }*/

        String id = CloudServer.getWebServer().addToCache(content);

        // player.sendClickableMessage(CloudServer.TAG + "§7Du kannst den Log unter §ehttp://192.168.2.115:1337/?log=" + id + " §7einsehen.", "§7Kopiere den Link", "", "http://192.168.2.115:1337/?log=" + id);
        player.sendClickableMessage(CloudServer.TAG + "§7Du kannst den Log unter §e37.228.148.3:1337/?log=" + id + " §7einsehen.", "§7Kopiere den Link", "", "37.228.148.3:1337/?log=" + id);
    }
}

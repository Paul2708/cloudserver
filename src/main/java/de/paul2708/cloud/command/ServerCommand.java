package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Paul on 03.11.2016.
 */
public class ServerCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 110) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            String server = CloudServer.getServerManager().getServers().stream().map(SpigotServer::getName).collect(Collectors.joining("§7, §e"));
            player.sendMessage(CloudServer.TAG + "§7Folgende Server sind registriert: §e" + server);
        } else {
            // Check valid arguemnts
            SpigotServer server = CloudServer.getServerManager().getServer(args[0]);

            if (server == null) {
                player.sendMessage(CloudServer.TAG + "§cDer Server " + args[0] + " wurde nicht gefunden.");
                return;
            }

            List<String> message = new ArrayList<>();

            message.add(CloudServer.TAG + "§eInformationen §7zu §e" + server.getName() + "§7:");
            message.add(CloudServer.TAG + "§7Name: §e" + server.getName());
            message.add(CloudServer.TAG + "§7Port: §e" + server.getPort());
            message.add(CloudServer.TAG + "§7Spieler: §e" + server.getOnlinePlayers() + "/" + server.getMaxPlayers());
            message.addAll(server.getPlayers().stream().map(online -> CloudServer.TAG + "   §7- " + online.getRank().getPrefix() + online.getName()).collect(Collectors.toList()));
            message.add(CloudServer.TAG + "§7Motd: §e" + server.getMotd());
            message.add(CloudServer.TAG + "§7State: §e" + (server.getGameState() == null ? "-/-" : server.getGameState() + "") + "");

            message.add(CloudServer.TAG + "§7Template: §e" + (server.getTemplate() == null ? "-/-" : server.getTemplate().getName()));
            int pid = CloudServer.getTemplateManager().getPID(server.getName());
            message.add(CloudServer.TAG + "§7PID: §e" + (pid == -1 ? "kein Prozess ID bekannt" : pid));

            player.sendMessages(message);
        }
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.stats.StatsCommandPacket;
import de.paul2708.common.stats.GameStatistic;
import de.paul2708.common.stats.StatisticData;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
public class StatsCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 0 && args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/stats [Spieler]");
            return;
        }

        // Check valid arguments
        if (CloudServer.getServerManager().isLobbyServer(player.getServer())) {
            player.sendMessage(CloudServer.TAG + "§cAuf der Lobby sind keine Statistiken verfügbar.");
            return;
        }
        if (!CloudServer.getStatsManager().isGameServer(player.getServer().getName())) {
            player.sendMessage(CloudServer.TAG + "§cAuf diesem Server sind keine Statistiken verfügbar.");
            return;
        }

        GameStatistic game = CloudServer.getStatsManager().getGameStatistic(player.getServer().getName());
        if (args.length == 0) {
            // Own stats
            player.sendMessage(CloudServer.TAG + "§7Deine §eStatistiken§7:");
            StatisticData data = CloudServer.getStatsManager().getStatisticData(UUID.fromString(player.getUuid()), game.getName());

            /*for (Map.Entry<Category, Object> entry : data.getStats().entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }*/
            
            CloudServer.getPacketServer().sendToAPI(game.getName(),
                    new StatsCommandPacket(UUID.fromString(player.getUuid()), data, new ArrayList<>()));
        } else if (args.length == 1) {
            // Other stats
            Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
            if (target == null) {
                player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " hat das Netzwerk noch nicht betreten.");
                return;
            }

            StatisticData data = CloudServer.getStatsManager().getStatisticData(UUID.fromString(target.getUuid()), game.getName());
            if (data == null) {
                data = CloudServer.getStatsManager().resolve(UUID.fromString(target.getUuid()), game.getName());
            }

            player.sendMessage(CloudServer.TAG + "§7" + target.getRank().getPrefix() + target.getName() + "'s §eStatistiken§7:");
            CloudServer.getPacketServer().sendToAPI(game.getName(),
                    new StatsCommandPacket(UUID.fromString(player.getUuid()), data, new ArrayList<>()));
        }
    }
}

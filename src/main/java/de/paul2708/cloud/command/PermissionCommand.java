package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.rank.PermissionUpdatePacket;
import de.paul2708.common.rank.Rank;

/**
 * Created by Paul on 14.08.2016.
 */
public class PermissionCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 3) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/permission <add|remove> <Rang> <Permission>");
            return;
        }

        String option = args[0];
        Rank rank = CloudServer.getRankManager().getRank(args[1]);
        String permission = args[2];

        // Check valid arguments
        if (!option.equalsIgnoreCase("add") && !option.equalsIgnoreCase("remove")) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/permission <add|remove> <Rang> <Permission>");
            return;
        }
        if (rank == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Rang " + args[1] + " existiert nicht.");
            return;
        }

        // Add permission
        if (option.equalsIgnoreCase("add")) {
            if (rank.getPermissionList().contains(permission)) {
                player.sendMessage(CloudServer.TAG + "§cDie Permission " + permission +
                        " für den Rang " + rank.getPrefix() + rank.getName() + " §cist bereits vorhanden.");
                return;
            }

            rank.addPermission(permission);
            DatabaseManager.updatePermissions(rank.getName());

            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                    new PermissionUpdatePacket(rank));

            player.sendMessage(CloudServer.TAG + "§7Die Permission §e" + permission + " §7wurde dem Rang " +
                    rank.getPrefix() + rank.getName() + " §7hinzugefügt.");

            CloudLogger.info("Permission: +" + permission + " für " + rank.getName());
        } else {
            if (!rank.getPermissionList().contains(permission)) {
                player.sendMessage(CloudServer.TAG + "§cDie Permission " + permission +
                        " für den Rang " + rank.getPrefix() + rank.getName() + " §cist nicht vorhanden.");
                return;
            }

            rank.removePermission(permission);
            DatabaseManager.updatePermissions(rank.getName());

            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                    new PermissionUpdatePacket(rank));

            player.sendMessage(CloudServer.TAG + "§7Die Permission §e" + permission + " §7wurde dem Rang " +
                    rank.getPrefix() + rank.getName() + " §7entfernt.");

            CloudLogger.info("Permission: -" + permission + " für " + rank.getName());
        }
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 04.09.2016.
 */
public class UnlinkCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check valid arguments
        if (player.getTeamSpeakUID().equalsIgnoreCase("-1")) {
            player.sendMessage(CloudServer.TAG + "§cDu bist nicht verlinkt.");
            return;
        }
        if (CloudServer.getTeamspeakBot() == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Bot ist nicht aktiv.");
            return;
        }

        // Unlink
        CloudServer.getTeamspeakBot().changeDescription(
                CloudServer.getTeamspeakBot().getApi().getClientByUId(player.getTeamSpeakUID()),
                "nicht verlinkt");
        CloudServer.getTeamspeakBot().setServerGroup(CloudServer.getTeamspeakBot().getApi().getDatabaseClientByUId(player.getTeamSpeakUID()),
                CloudServer.getTeamspeakFile().getDefaultGroupId());
        player.setTeamSpeakUID("-1");

        player.sendMessage(CloudServer.TAG + "§eDein Account ist nicht mehr mit dem TS3 verlinkt.");
    }
}

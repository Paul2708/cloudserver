package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.rank.RankManager;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import de.paul2708.common.rank.Rank;

/**
 * Created by Paul on 08.08.2016.
 */
public class RankCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/rank <create|delete>");
            return;
        }
        // Create
        if (args[0].equalsIgnoreCase("create")) {
            if (args.length != 6) {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/rank create <Name> <Tag> <ShortTag> <Level> <TeamspeakID>");
            } else {
                String name = args[1];
                String tag = args[2];
                String shortTag = args[3];
                int level;
                int groupId;

                // Check valid arguments
                if (CloudServer.getRankManager().getRank(name) != null) {
                    player.sendMessage(CloudServer.TAG + "§cDer Rang existiert bereits.");
                    return;
                }

                try {
                    level = Integer.valueOf(args[4]);
                } catch (NumberFormatException e) {
                    player.sendMessage(CloudServer.TAG + "§cDas Permission-Level ist ungültig. (" + args[4] + ")");
                    return;
                }
                try {
                    groupId = Integer.valueOf(args[5]);
                } catch (NumberFormatException e) {
                    player.sendMessage(CloudServer.TAG + "§cDie TS3-Gruppen ID ist ungültig. (" + args[5] + ")");
                    return;
                }

                // Rank adden
                DatabaseManager.addRank(name, tag, shortTag, level, groupId);
                CloudServer.getRankManager().getRanks().add(new Rank(name, tag, shortTag, level, "", groupId));

                player.sendMessage("§7Der Rang §e" + name + " §7wurde hinzugefügt.");

                CloudLogger.info("Rank: " + name + " wurde hinzugefügt");
            }
            // Delete
        } else if (args[0].equalsIgnoreCase("delete")) {
            if (args.length != 2) {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/rank delete <Name>");
            } else {
                String name = args[1];

                // Check valid arguments
                if (CloudServer.getRankManager().getRank(name) == null) {
                    player.sendMessage(CloudServer.TAG + "§cDer Rang existiert nicht.");
                    return;
                }

                // Rank löschen
                DatabaseManager.removeRank(name);
                Rank rank = CloudServer.getRankManager().getRank(name);
                CloudServer.getRankManager().getRanks().remove(rank);

                player.sendMessage("§7Der Rang §e" + name + " §7wurde gelöscht.");

                CloudLogger.info("Rank: " + name + " wurde entfernt");

                int temp = 0;
                for (Player all : CloudServer.getPlayerManager().getPlayers().values()) {
                    if (all.getRank().getName().equalsIgnoreCase(name)) {
                        all.setRankEnd(-1L);
                        all.setRank(CloudServer.getRankManager().getRank(RankManager.DEFAULT_RANK));
                        if (all.isOnline()) {
                            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                                    new RankUpdatePacket(all.getUuid(), rank, CloudServer.getRankManager().getRank(RankManager.DEFAULT_RANK), -1L));
                            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                                    new RankUpdatePacket(all.getUuid(), rank, CloudServer.getRankManager().getRank(RankManager.DEFAULT_RANK), -1L));
                        }

                        temp++;
                    }
                }

                CloudLogger.info("Rank: " + RankManager.DEFAULT_RANK + " für " + temp + " Spieler gesetzt");
            }
        }
    }
}

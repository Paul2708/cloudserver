package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 31.08.2016.
 */
public class WhereAmICommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        player.sendMessage(CloudServer.TAG + "§7Du befindest dich auf Server §e" + player.getServer().getName() + "§7.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.key.Key;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 31.12.2016.
 */
public class KeyCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length > 2 || args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/key <create|list> [1-10]");
            return;
        }

        if (args[0].equalsIgnoreCase("list")) {
            List<Key> keys = CloudServer.getKeyManager().getKeys();
            player.sendMessage(CloudServer.TAG + "§e" + keys.size() + " §7Keys wurden erstellt.");

            List<String> messages = new ArrayList<>();
            for (Key key : keys) {
                if (key.isRedeemed()) {
                    messages.add(CloudServer.TAG + "§e" + key.getKey() + "§7: §a" + key.getName() + " §7(§e" + Util.getTime(key.getTime().getTime()) + "§7)");
                } else {
                    messages.add(CloudServer.TAG + "§e" + key.getKey() + "§7: §cnicht eingelöst");
                }
            }


            player.sendMessages(messages);
        } else if (args[0].equalsIgnoreCase("create")) {
            if (args.length == 1) {
                player.sendMessage(CloudServer.TAG + "§7Ein §eBeta-Key §7wird erstellt...");
                String key = CloudServer.getKeyManager().generateKey();
                player.sendClickableMessage(CloudServer.TAG + "§7Beta-Key: §e" + key, "§eKopieren", "", key);
                player.playSound("LEVEL_UP");

                CloudServer.getKeyManager().addKey(key);
            } else {
                try {
                    int amount = Integer.valueOf(args[1]);

                    if (amount < 1 || amount > 10) {
                        player.sendMessage(CloudServer.TAG + "§cDu kannst nur einen bis max. 10 Keys erstellen.");
                        return;
                    }

                    player.sendMessage(CloudServer.TAG + "§7Es werden §e" + amount + " Beta-Keys §7erstellt...");
                    for (int i = 0; i < amount; i++) {
                        String key = CloudServer.getKeyManager().generateKey();
                        player.sendClickableMessage(CloudServer.TAG + "§7Beta-Key: §e" + key, "§eKopieren", "", key);
                        CloudServer.getKeyManager().addKey(key);
                    }

                    player.playSound("LEVEL_UP");
                } catch (NumberFormatException e) {
                    player.sendMessage(CloudServer.TAG + "§cDu kannst nur einen bis max. 10 Keys erstellen.");
                    return;
                }
            }
        } else {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/key <create|list> [1-10]");
        }
    }
}

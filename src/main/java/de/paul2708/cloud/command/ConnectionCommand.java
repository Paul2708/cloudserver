package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.common.packet.player.PlayerSoundPacket;

import java.util.UUID;

/**
 * Created by Paul on 07.12.2016.
 */
public class ConnectionCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 100) {
            player.sendMessage(CloudServer.TAG + "§c§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/connection <Server>");
            return;
        }

        // Check valid arguments
        SpigotServer server = CloudServer.getServerManager().getServer(args[0]);
        if (server == null) {
            player.sendMessages(CloudServer.TAG + "§cDer Server " + args[0] + " ist nicht registriert.");
            return;
        }

        // Send test packet
        server.sendPacket(new PlayerSoundPacket(UUID.fromString(player.getUuid()), "LEVEL_UP"));
        player.sendMessages(CloudServer.TAG + "§eTest-Packet wurde gesendet.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;

/**
 * Created by Paul on 31.08.2016.
 */
public class SendAllCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/sendall <Server>");
            return;
        }

        // Check valid arguments
        SpigotServer server = CloudServer.getServerManager().getServer(args[0]);
        if (server == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Server " + args[0] + " wurde nicht gefunden.");
            return;
        }

        // Send to server
        for (Player online : player.getServer().getPlayers()) {
            online.send(server);
        }

        player.sendMessage(CloudServer.TAG + "§7Alle Spieler wurden auf den Server §e" + server.getName() + " §7verschoben.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.player.PlayerImageMessagePacket;

import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 08.09.2016.
 */
public class JoinCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 80) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check valid arguments
        if (Util.joinDelay.get(player) != null) {
            long timeStamp = Util.joinDelay.get(player);

            if (timeStamp + TimeUnit.MINUTES.toMillis(5) > System.currentTimeMillis()) {
                player.sendMessage(CloudServer.TAG + "§cDu kannst nur alle 5 Minuten nach Spielern fragen.");
                return;
            }
        }

        SpigotServer server = player.getServer();
        if (server.getName().startsWith(CloudServer.getConfigFile().getLobbyTag())) {
            player.sendMessage(CloudServer.TAG + "§cDu befindest dich auf einem Lobby-Server.");
            return;
        }

        // Send image
        Util.joinDelay.put(player, System.currentTimeMillis());

        CloudServer.getPacketServer().sendToLobby(new PlayerImageMessagePacket(player.getName(), player.getUuid(), player.getRank(), server.getName()));
        player.sendMessage(CloudServer.TAG + "§eDu hast in der Lobby nach Spielern gefragt.");
    }
}

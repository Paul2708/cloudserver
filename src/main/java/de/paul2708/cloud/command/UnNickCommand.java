package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 31.08.2016.
 */
public class UnNickCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/unnick");
            return;
        }

        // Check valid arguments
        if (!CloudServer.getNickManager().isNicked(player)) {
            player.sendMessage(CloudServer.TAG + "§cDu bist nicht genickt.");
            return;
        }

        // Unnick player
        CloudServer.getNickManager().unnick(player);

        player.sendMessage(CloudServer.TAG + "§7Du hast deinen Nick §7resettet.");
    }

}

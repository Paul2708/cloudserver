package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 02.09.2016.
 */
public class LoadMessagesCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Reload messages
        CloudServer.getAutoMessageFile().reload();
        CloudServer.getAutoMessageManager().stop();
        CloudServer.getAutoMessageManager().start();

        player.sendMessage(CloudServer.TAG + "§eDie Nachrichten wurden neu geladen.");
    }
}

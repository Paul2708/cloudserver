package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 16.10.2016.
 */
public class TokensCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Tokens
        player.sendMessage("§eTokens§8│§7" + "§7Du hast §e" + player.getTokens() + " §7Tokens.");
    }
}

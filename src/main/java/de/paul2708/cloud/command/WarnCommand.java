package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.abuse.WarnReason;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.abuse.AbuseAction;

import java.util.UUID;

/**
 * Created by Paul on 04.10.2016.
 */
public class WarnCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 2) {
            player.sendMessages(CloudServer.TAG + "§7Nutze §e/warn <Spieler> <Grund>",
                    CloudServer.TAG + "§7Mögliche Gründe: " + WarnReason.getReasons());
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " war noch nie auf dem Netzwerk.");
            return;
        }
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst dich nicht selbst warnen.");
            return;
        }
        if (target.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                !(player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getAdminPermissionLevel())) {
            player.sendMessage(CloudServer.TAG + "§cDu darfst den Spieler " + target.getRank().getPrefix() + target.getName() +
                    " §cnicht verwarnen.");
            return;
        }

        WarnReason reason = WarnReason.getByName(args[1]);
        if (reason == null) {
            player.sendMessages(CloudServer.TAG + "§cDer Grund wurde nicht gefunden.",
                    CloudServer.TAG + "§7Mögliche Gründe: " + WarnReason.getReasons());
            return;
        }

        if (reason.getMutePoints() != 0 && CloudServer.getAbuseManager().isMuted(target) != null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler ist bereits gemutet.");
            return;
        }
        if (reason.getBanPoints() != 0 && CloudServer.getAbuseManager().isBanned(target) != null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler ist bereits gebannt.");
            return;
        }

        // Warn player
        Abuse abuse = new Abuse(AbuseAction.WARN, player.getName(), target.getName(), reason.getName(),
                System.currentTimeMillis(), -1L, UUID.fromString(player.getUuid()),
                UUID.fromString(target.getUuid()));
        CloudServer.getAbuseManager().addAbuse(abuse);

        target.sendMessage(CloudServer.TAG + "§cDu wurdest wegen §e" + reason.getName() + " §cverwarnt.");

        player.sendMessage(CloudServer.TAG + "§7Du hast §e" + target.getName() + " §7wegen §e" + reason.getName() + " §7verwarnt.");
    }
}

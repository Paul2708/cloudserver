package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 17.08.2016.
 */
public class SendCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 110) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1 && args.length != 2) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/send [Spieler] <Server>");
            return;
        }

        if (args.length == 1) {
            // Check valid arguemnts
            if (CloudServer.getServerManager().getServer(args[0]) == null) {
                player.sendMessage(CloudServer.TAG + "§cDer Server " + args[0] + " wurde nicht gefunden.");
                return;
            }
            if (CloudServer.getServerManager().getServer(args[0]).getName().
                    equalsIgnoreCase(player.getServer().getName())) {
                player.sendMessage(CloudServer.TAG + "§cDu befindest dich bereits auf " + player.getServer().getName() + ".");
                return;
            }

            player.send(args[0]);
            player.sendMessage(CloudServer.TAG + "§7Du wirst auf §e" + args[0] + " §7verschoben.");
        } else {
            Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
            if (target == null || !target.isOnline()) {
                player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " konnte nicht gefunden werden.");
            } else {
                if (CloudServer.getServerManager().getServer(args[1]) == null) {
                    player.sendMessage(CloudServer.TAG + "§cDer Server " + args[1] + " wurde nicht gefunden.");
                    return;
                }
                if (target.getServer() != null && target.getServer().getName().equalsIgnoreCase(args[1])) {
                    player.sendMessage(CloudServer.TAG + "§cDer Spieler " + target.getName() + " ist bereits auf dem Server " + args[1] + ".");
                    return;
                }

                target.send(args[1]);
                player.sendMessage(CloudServer.TAG + target.getRank().getPrefix() + target.getName() + " §7wird auf §e" + args[1] + " §7verschoben.");
            }
        }
    }
}

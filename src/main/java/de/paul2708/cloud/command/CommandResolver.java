package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.command.CommandType;

import java.util.UUID;

/**
 * Created by Paul on 08.08.2016.
 */
public class CommandResolver {

    public static void resolve(UUID uuid, String command, String... args) {
        CommandType type = CommandType.getTypeByCommand(command);
        Player player = CloudServer.getPlayerManager().getPlayer(uuid);
        if (player == null) return;

        switch (type) {
            case RANK:
                new RankCommand().execute(player, args);
                break;
            case SET_RANK:
                new SetRankCommand().execute(player, args);
                break;
            case PERMISSION:
                new PermissionCommand().execute(player, args);
                break;
            case REPORT:
                new ReportCommand().execute(player, args);
                break;
            case REPORTS:
               new ReportsCommand().execute(player, args);
                break;
            case BROADCAST:
                new BroadcastCommand().execute(player, args);
                break;
            case SEND:
                new SendCommand().execute(player, args);
                break;
            case WHEREAMI:
                new WhereAmICommand().execute(player, args);
                break;
            case SENDALL:
                new SendAllCommand().execute(player, args);
                break;
            case GOTO:
                new GoToCommand().execute(player, args);
                break;
            case FIND:
                new FindCommand().execute(player, args);
                break;
            case TEAMCHAT:
                new TeamChatCommand().execute(player, args);
                break;
            case ADMINCHAT:
                new AdminChatCommand().execute(player, args);
                break;
            case ONLINE:
                new OnlineCommand().execute(player, args);
                break;
            case TEAM:
                new TeamCommand().execute(player, args);
                break;
            case LOADMESSAGES:
                new LoadMessagesCommand().execute(player, args);
                break;
            case SETMOTD:
                new SetMotdCommand().execute(player, args);
                break;
            case CHATLOG:
                new ChatlogCommand().execute(player, args);
                break;
            case UNLINK:
                new UnlinkCommand().execute(player, args);
                break;
            case TS:
                new TsCommand().execute(player, args);
                break;
            case BUILDEVENT:
                new BuildEventCommand().execute(player, args);
                break;
            case BUILDSERVER:
                new BuildServerCommand().execute(player, args);
                break;
            case BLACKLIST:
                new BlacklistCommand().execute(player, args);
                break;
            case MAINTENANCE:
                new MaintenanceCommand().execute(player, args);
                break;
            case INFO:
                new InfoCommand().execute(player, args);
                break;
            case JOIN:
                new JoinCommand().execute(player, args);
                break;
            case KICK:
                new KickCommand().execute(player, args);
                break;
            case BAN:
                new BanCommand().execute(player, args);
                break;
            case MUTE:
                new MuteCommand().execute(player, args);
                break;
            case UNBAN: case PARDON:
                new UnBanCommand().execute(player, args);
                break;
            case UNMUTE:
                new UnMuteCommand().execute(player, args);
                break;
            case WARN:
                new WarnCommand().execute(player, args);
                break;
            case PLAYTIME:
                new PlayTimeCommand().execute(player, args);
                break;
            case TOKENS:
                new TokensCommand().execute(player, args);
                break;
            case STATS:
                new StatsCommand().execute(player, args);
                break;
            case SERVER:
                new ServerCommand().execute(player, args);
                break;
            case LOBBY: case HUB: case LEAVE: case L:
                new LobbyCommand().execute(player, args);
                break;
            case NOTIFICATION:
                new NotificationCommand().execute(player, args);
                break;
            case TWEET:
                new TweetCommand().execute(player, args);
                break;
            case CLOUD:
                new CloudInfoCommand().execute(player, args);
                break;
            case TEMPLATE:
                new TemplateCommand().execute(player, args);
                break;
            case CONNECTION:
                new ConnectionCommand().execute(player, args);
                break;
            case KEY:
                new KeyCommand().execute(player, args);
                break;
            case BUG:
                new BugCommand().execute(player, args);
                break;
            case MSG:
                new MsgCommand().execute(player, args);
                break;
            case SETMAXPLAYERS:
                new SetMaxPlayersCommand().execute(player, args);
                break;
            case REPLY: case R:
                new ReplyCommand().execute(player, args);
                break;
            case LOG:
                new LogCommand().execute(player, args);
                break;
            case NICK:
                new NickCommand().execute(player, args);
                break;
            case UNNICK:
                new UnNickCommand().execute(player, args);
                break;
            case TEMPLATES:
                new TemplatesCommand().execute(player, args);
                break;
            case SURVEY:
                new SurveyCommand().execute(player, args);
                break;
        }
    }
}

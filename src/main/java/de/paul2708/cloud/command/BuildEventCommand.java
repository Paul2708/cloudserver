package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 05.09.2016.
 */
public class BuildEventCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check valid arguments
        if (CloudServer.getConfigFile().getBuildEventServer().equalsIgnoreCase("none") ||
                CloudServer.getServerManager().getServer(CloudServer.getConfigFile().getBuildEventServer()) == null) {
            player.sendMessage(CloudServer.TAG + "§cEs findet momentan kein Bauevent statt.");
            return;
        }

        player.send(CloudServer.getConfigFile().getBuildEventServer());
        player.sendMessage(CloudServer.TAG + "§7Du wirst auf das §eBauevent §7verschoben.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.server.MotdPacket;

/**
 * Created by Paul on 02.09.2016.
 */
public class SetMotdCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            CloudServer.getConfigFile().reload();
            String motd = CloudServer.getConfigFile().getMotd();
            motd = motd.replaceAll("&", "§");

            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new MotdPacket(motd));
            player.sendMessage(CloudServer.TAG + "§eDu hast die Motd neu geladen.");
            CloudLogger.info("Motd: " + motd);
            return;
        }

        // Build motd
        String motd = "";

        for (String arg : args) {
            motd += arg + " ";
        }

        // Save in file
        CloudServer.getConfigFile().set("motd", motd);
        CloudServer.getConfigFile().save();

        // Send
        motd = motd.replaceAll("&", "§");

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new MotdPacket(motd));

        player.sendMessage(CloudServer.TAG + "§7Du hast die Motd zu " + motd + " §7geändert.");
        CloudLogger.info("Motd: " + motd);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 05.09.2016.
 */
public class TsCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 95) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 2) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/ts support <open|close>");
            return;
        }

        // Check valid arguments
        if (!args[0].equalsIgnoreCase("support") ||
                (!args[1].equalsIgnoreCase("open") && !args[1].equalsIgnoreCase("close"))) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/ts support <open|close>");
            return;
        }
        if (CloudServer.getTeamspeakBot() == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Bot ist nicht aktiv.");
            return;
        }

        // Open/close support channel
        CloudServer.getTeamspeakBot().editSupportChannel(args[1].equalsIgnoreCase("open"));
        player.sendMessage(CloudServer.TAG + "§7Du hast den TS-Support Channel " +
                (args[1].equalsIgnoreCase("open") ? "§ageöffnet" : "§cgeschlossen") + "§7.");
    }
}

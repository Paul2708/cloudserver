package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;


/**
 * Created by Paul on 03.11.2016.
 */
public class LobbyCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check valid arguments
        if (CloudServer.getServerManager().isLobbyServer(player.getServer())) {
            player.sendMessage(CloudServer.TAG + "§cDu bist bereits auf der Lobby.");
            return;
        }

        // Send to lobby
        SpigotServer lobby = CloudServer.getServerManager().getLobby();
        if (lobby == null) {
            player.sendMessage(CloudServer.TAG + "§cEs konnte keine Lobby gefunden werden.");
            return;
        }

        player.send(lobby);
        player.sendMessage(CloudServer.TAG + "§7Du wirst auf die Lobby verschoben.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

import java.util.Map;

/**
 * Created by Paul on 20.01.2017.
 */
public class ReplyCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 20) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/reply [Nachricht]");
            return;
        }

        Map<Player, Player> data = Util.msgData;
        Player target = data.get(player);

        // Check valid arguments
        if (target == null) {
            player.sendMessage(CloudServer.TAG + "§cDu hast keine Unterhaltung offen.");
            return;
        }
        if (!target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + target.getRank() + target.getName() + " §cist offline.");
            return;
        }

        String message = "";
        for (int i = 0; i < args.length; i++) {
            message += args[i] + " ";
        }

        message = message.substring(0, message.length() - 1);

        // Send message
        player.sendMessage(CloudServer.TAG + "§eDu §8⇛ " + target.getRank().getPrefix() +
                target.getName() + "§8: §7" + message);
        target.sendMessage(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + "§e §8⇛ §eDir §8: §7" + message);
    }
}

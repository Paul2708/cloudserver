package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 16.10.2016.
 */
public class PlayTimeCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Playtime
        player.sendMessage(CloudServer.TAG + "§7Deine Spielzeit: §e" + player.getPlayTime() / 60 + " §7Stunden und" +
                " §e" + player.getPlayTime() % 60 + " §7Minuten.");
    }
}

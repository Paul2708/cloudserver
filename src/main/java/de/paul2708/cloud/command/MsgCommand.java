package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

/**
 * Created by Paul on 20.01.2017.
 */
public class MsgCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 20) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length < 2) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/msg [Spieler] [Nachricht]");
            return;
        }

        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);

        // Check valid arguments
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }
        if (target.equals(player)) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst nicht mit dir selbst chatten.");
            return;
        }

        String message = "";
        for (int i = 1; i < args.length; i++) {
            message += args[i] + " ";
        }

        message = message.substring(0, message.length() - 1);

        // Send message
        Util.msgData.put(player, target);
        Util.msgData.put(target, player);

        player.sendMessage(CloudServer.TAG + "§eDu §8⇛ " + target.getRank().getPrefix() +
                target.getName() + "§8: §7" + message);
        target.sendMessage(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + "§e §8⇛ §eDir §8: §7" + message);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 31.08.2016.
 */
public class GoToCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/goto <Spieler>");
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }
        if (target.getServer().getName().equalsIgnoreCase(player.getServer().getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu befindest dich bereits auf dem gleichen Server.");
            return;
        }

        // Send to server
        player.send(target.getServer().getName());
        player.sendMessage(CloudServer.TAG + "§7Du hast den Server von " + target.getRank().getPrefix() + target.getName() + " §7betreten.");
    }
}

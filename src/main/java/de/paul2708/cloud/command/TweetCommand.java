package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 12.11.2016.
 */
public class TweetCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 110) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/tweet <Nachricht>");
            return;
        }

        // Check valid arguments
        String message = "";
        for (int i = 0; i < args.length; i++) {
            message += args[i] + " ";
        }

        player.sendMessage(CloudServer.TAG + "§cMomentan ist /tweet noch deaktiviert.");
        return;

        // Post tweet
        //player.sendMessage(CloudServer.TAG + "§7Der Tweet wird vorbereitet...");
        //CloudServer.getTwitterManager().post(player, message);
    }
}

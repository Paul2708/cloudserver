package de.paul2708.cloud.command;

import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 08.08.2016.
 */
public abstract class CloudCommand {

    public abstract void execute(Player player, String[] args);
}

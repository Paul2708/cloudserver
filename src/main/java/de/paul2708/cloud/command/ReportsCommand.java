package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.report.ListReportPacket;

/**
 * Created by Paul on 18.08.2016.
 */
public class ReportsCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 95) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/reports");
            return;
        }
        // Check valid args
        if (CloudServer.getReportManager().getOpenReports().size() == 0) {
            player.sendMessage(CloudServer.TAG + "§cEs gibt keine offene Reports.");
            return;
        }

        // Open report inventory
        player.getServer().sendPacket(new ListReportPacket(player.getName(), CloudServer.getReportManager().getOpenReports()));
    }
}

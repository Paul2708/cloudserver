package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.player.PlayerMessagePacket;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import de.paul2708.common.rank.Rank;

/**
 * Created by Paul on 08.08.2016.
 */
public class SetRankCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 2 && args.length != 4) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/setrank <Spieler> <Rang> [Dauer] [h|d|w|m]");
            String ranks = "";

            for (Rank rank : CloudServer.getRankManager().getRanks()) {
                ranks += rank.getPrefix() + rank.getName() + "§7, ";
            }

            ranks = ranks.substring(0, ranks.length() - 2);
            player.sendMessage(CloudServer.TAG + "§eMögliche Ränge§7: " + ranks);
            return;
        }

        // Check valid arguments
        if (CloudServer.getPlayerManager().getPlayer(args[0]) == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " war noch nie auf dem Netzwerk.");
            return;
        }
        if (CloudServer.getRankManager().getRank(args[1]) == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Rang " + args[1] + " existiert nicht.");
            return;
        }

        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        final Rank oldRank = target.getRank();
        Rank newRank = CloudServer.getRankManager().getRank(args[1]);
        long duration = -1L;

        if (args.length == 4) {
            try {
                duration = Integer.valueOf(args[2]);
            } catch (NumberFormatException e) {
                player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[3] + "' ist nicht korrekt.");
                return;
            }

            if (args[3].equalsIgnoreCase("h")) {
                duration = duration * 60 * 60;
            } else if (args[3].equalsIgnoreCase("d")) {
                duration = duration * 60 * 60 * 24;
            } else if (args[3].equalsIgnoreCase("w")) {
                duration = duration * 60 * 60 * 24 * 7;
            } else if (args[3].equalsIgnoreCase("m")) {
                duration = duration * 60 * 60 * 24  * 30;
            } else {
                player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[3] + "' ist nicht korrekt.");
                return;
            }

            duration = duration * 1000;
        }

        target.setRankEnd(duration == -1 ? -1 : duration + System.currentTimeMillis());
        target.setRank(newRank);
        CloudServer.getPacketServer().send(ClientType.SPIGOT,
                new RankUpdatePacket(target.getUuid(), oldRank, newRank, duration));
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new RankUpdatePacket(target.getUuid(), oldRank, newRank, duration));

        String message;

        // Permanent or experince
        if (duration == -1) {
            message = CloudServer.TAG + "§7Du hast nun §cpermanent §7den Rang " + newRank.getPrefix() + newRank.getName() + " §7erhalten.";

            player.sendMessage(CloudServer.TAG + "§7Der Spieler §e" + target.getName() + " §7hat nun permanent den Rang " +
                    newRank.getPrefix() + newRank.getName() + "§7.");
        } else {
            long time = System.currentTimeMillis() + duration;
            message = CloudServer.TAG + "§7Du hast nun den Rang " + newRank.getPrefix() + newRank.getName() + " §7erhalten. ";
            message += "§7(Ende: §e" + Util.getTime(time) + "§7)";

            player.sendMessage(CloudServer.TAG + "§7Der Spieler §e" + target.getName() + " §7hat nun den Rang " + newRank.getPrefix() + newRank.getName() + "§7.");
            player.sendMessage(CloudServer.TAG + "§7Ende: §e" + Util.getTime(time));
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerMessagePacket(target.getName(), message));
        CloudLogger.info("Rank: " + oldRank.getName() + " -> " + newRank.getName() + " (" + args[0] + ")");
    }
}

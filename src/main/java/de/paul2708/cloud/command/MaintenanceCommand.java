package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 06.09.2016.
 */
public class MaintenanceCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            sendHelp(player);
            return;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("on")) {
                if (CloudServer.getMaintenanceFile().isEnabled()) {
                    player.sendMessage(CloudServer.TAG + "§cDie Wartungen sind bereits aktiv.");
                } else {
                    if (CloudServer.getMaintenanceManager().isStarting()) {
                        player.sendMessage(CloudServer.TAG + "§cDie Wartungen starten bereits.");
                        return;
                    }
                    CloudServer.getMaintenanceManager().activate("none");

                    player.sendMessage(CloudServer.TAG + "§eDu hast die Wartungen aktiviert.");
                    player.sendMessage(CloudServer.TAG + "§7Nur noch Teammitglieder können den Server betreten.");
                }
            } else if (args[0].equalsIgnoreCase("off")) {
                if (CloudServer.getMaintenanceManager().isStarting()) {
                    player.sendMessage(CloudServer.TAG + "§cDie Wartungen starten bereits.");
                    return;
                }
                if (!CloudServer.getMaintenanceFile().isEnabled()) {
                    player.sendMessage(CloudServer.TAG + "§cDie Wartungen sind nicht aktiv.");
                } else {
                    CloudServer.getMaintenanceManager().disable();

                    player.sendMessage(CloudServer.TAG + "§eDu hast die Wartungen beendet.");
                    player.sendMessage(CloudServer.TAG + "§7Jeder kann den Server wieder betreten.");
                }
            } else if (args[0].equalsIgnoreCase("kickall")) {
                if (CloudServer.getMaintenanceManager().isStarting()) {
                    player.sendMessage(CloudServer.TAG + "§cDie Wartungen starten bereits.");
                    return;
                }
                if (CloudServer.getMaintenanceFile().isEnabled()) {
                    player.sendMessage(CloudServer.TAG + "§cDie Wartungen sind bereits aktiv.");
                } else {
                    CloudServer.getMaintenanceManager().activate("none");
                    CloudServer.getPlayerManager().kickAll("§cEs finden Wartungen statt. Mehr Infos auf unserem TS3.");

                    player.sendMessage(CloudServer.TAG + "§eDu hast die Wartungen aktiviert.");
                    player.sendMessage(CloudServer.TAG + "§7Alle Spieler wurden gekickt.");
                }
            } else {
                sendHelp(player);
            }
        } else if (args.length <= 3) {
            sendHelp(player);
        } else {
            if (CloudServer.getMaintenanceManager().isStarting()) {
                player.sendMessage(CloudServer.TAG + "§cDie Wartungen starten bereits.");
                return;
            }
            if (CloudServer.getMaintenanceFile().isEnabled()) {
                player.sendMessage(CloudServer.TAG + "§cDie Wartungen sind bereits aktiv.");
                return;
            }
            if (!args[0].equalsIgnoreCase("force")) {
                sendHelp(player);
            } else {

                long duration;

                try {
                    duration = Integer.valueOf(args[1]);
                } catch (NumberFormatException e) {
                    player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[1] + "' ist nicht korrekt.");
                    return;
                }

                switch (args[2]) {
                    case "m": case "M":
                        duration = duration * 60;
                        break;
                    default:
                        player.sendMessage(CloudServer.TAG + "§cDie Angabe '" + args[2] + "' ist nicht korrekt.");
                        return;
                }

                duration = duration * 1000;

                String reason = "";

                for (int i = 3; i < args.length; i++) {
                    reason += args[i] + " ";
                }

                CloudServer.getMaintenanceManager().start(duration, reason);

                player.sendMessage(CloudServer.TAG + "§7Die Wartungen beginnen in §e" +
                        (duration/60/1000) + " Minuten§7. Grund: §e" + reason);
            }
        }
    }

    private void sendHelp(Player player) {
        player.sendMessage(CloudServer.TAG + "§7Nutze §e/maintenance <on|off|kickall|force> [Dauer] [m] [Grund]");
    }
}

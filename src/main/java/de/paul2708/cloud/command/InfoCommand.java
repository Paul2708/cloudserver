package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.abuse.Abuse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 07.09.2016.
 */
public class InfoCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/info <Spieler>");
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " war noch nie auf dem Netzwerk.");
            return;
        }


        // Send info
        List<String> list = new ArrayList<>();
        list.add(CloudServer.TAG + "§7Folgende Informationen gibt es zu §e" + target.getName() + "§7:");
        list.add(CloudServer.TAG + "§7Name: §e" + target.getName());
        list.add(CloudServer.TAG + "§7UUID: §e" + target.getUuid());
        list.add(CloudServer.TAG + "§7Rang: " + target.getRank().getPrefix() + target.getRank().getName() + " §e(" + target.getRank().getPermissionLevel() + ")");
        list.add(CloudServer.TAG + "§7Rang-Ende: " + (target.getRankEnd() == -1L ? "§cpermanent" :
                "§e" + Util.getTime(target.getRankEnd())));
        list.add(CloudServer.TAG + "§7Status: " + (target.isOnline() ? "§aOnline" : "§cOffline"));
        list.add(CloudServer.TAG + "§7Aktueller Server: §e" + (target.isOnline() ? target.getServer().getName() : "-/-"));
        list.add(CloudServer.TAG + "§7Online Zeit: §e" + target.getPlayTime() / 60 + " §7Stunden und" + " §e" + target.getPlayTime() % 60 + " §7Minuten");
        list.add(CloudServer.TAG + "§7Benachrichtigungen: " + (target.hasNotification() ? "§aaktiviert" : "§cdeaktiviert"));
        list.add(CloudServer.TAG + "§7Tokens: §e" + target.getTokens());
        list.add(CloudServer.TAG + "§7Erster Login: §e" + Util.getTime(target.getFirstLogin()));
        list.add(CloudServer.TAG + "§7Letzter Login: §e" + Util.getTime(target.getLastLogin()));
        list.add(CloudServer.TAG + "§7IP-Adresse: " + (player.getRank().getPermissionLevel() >= 120 ? "§e" + target.getIp() : "§c*admin only*"));

        // Teamspeak
        if (CloudServer.getTeamspeakBot() == null) {
            list.add(CloudServer.TAG + "§7Teamspeak-Client: §eBot nicht aktiv");
        } else {
            list.add(CloudServer.TAG + "§7Teamspeak-Client: §e" + CloudServer.getTeamspeakBot().getName(target.getTeamSpeakUID()));
        }

        list.add(CloudServer.TAG + "§7Teamspeak-UID: §e" + (player.getRank().getPermissionLevel() >= 120 ? (target.getTeamSpeakUID().equalsIgnoreCase("-1") ? "-/-" : target.getTeamSpeakUID()) : "§c*admin only*"));
        list.add(CloudServer.TAG + "§7Ban-Punkte: §e" + target.getBanPoints());
        list.add(CloudServer.TAG + "§7Mute-Punkte: §e" + target.getMutePoints());

        // Mute and ban
        Abuse mute = CloudServer.getAbuseManager().isMuted(player);
        if (mute == null) {
            list.add(CloudServer.TAG + "§aDer Spieler ist nicht gemutet.");
        } else {
            list.add("§cDer Spieler ist gemutet. Grund: " + mute.getReason() + " (" + Util.getTime(mute.getLastTo()) + ")");
        }
        Abuse ban = CloudServer.getAbuseManager().isBanned(player);
        if (ban == null) {
            list.add(CloudServer.TAG + "§aDer Spieler ist nicht gebannt.");
        } else {
            list.add("§cDer Spieler ist gebannt. Grund: " + ban.getReason() + " (" + Util.getTime(ban.getLastTo()) + ")");
        }

        player.sendMessages(list);
    }
}

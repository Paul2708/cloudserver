package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 06.11.2016.
 */
public class NotificationCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 95) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Change notification
        player.notification(!player.hasNotification());
        if (player.hasNotification()) {
            player.sendMessage(CloudServer.TAG + "§7Du hast die Benachrichtigungen §aaktiviert§7.");
        } else {
            player.sendMessage(CloudServer.TAG + "§7Du hast die Benachrichtigungen §cdeaktiviert§7.");
        }
    }
}

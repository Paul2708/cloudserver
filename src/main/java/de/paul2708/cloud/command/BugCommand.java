package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;

import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 16.08.2016.
 */
public class BugCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/bug <Beschreibung>");
            return;
        }

        // Check valid arguments
        if (Util.bugDelay.get(player) != null) {
            long timeStamp = Util.bugDelay.get(player);

            if (timeStamp + TimeUnit.SECONDS.toMillis(30) > System.currentTimeMillis()) {
                player.sendMessage(CloudServer.TAG + "§cDu kannst nur alle 30 Sekunden einen Bug melden.");
                return;
            }
        }

        String message = "\"";

        for (String arg : args) {
            message += arg + " ";
        }

        message = message.substring(0, message.length() - 1);
        message += "\" - " + player.getName() + " auf " + player.getServer().getName() + " | " + Util.getTime(System.currentTimeMillis());

        // Send bug
        Util.bugDelay.put(player, System.currentTimeMillis());

        CloudServer.getGroupMeManager().sendMessage(player, message);
    }
}

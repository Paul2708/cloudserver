package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.template.Template;

import java.util.stream.Collectors;

/**
 * Created by Paul on 28.11.2016.
 */
public class TemplatesCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/templates");
            return;
        }

        // Send templates
        String templates = CloudServer.getTemplateManager().getTemplates().stream()
                .map(Template::getName).collect(Collectors.joining("§7, §e"));
        player.sendMessage(CloudServer.TAG + "§7Geladene Templates: §e" + templates);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.rank.Rank;

/**
 * Created by Paul on 31.08.2016.
 */
public class TeamChatCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/tc <Nachricht>");
            return;
        }

        String message = "";

        for (String arg : args) {
            message += arg + " ";
        }

        message = message.replaceAll("&", "§");

        for (Player online : CloudServer.getPlayerManager().getOnlinePlayers()) {
            if (online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
                Rank rank = player.getRank();
                online.sendMessage("§7[§eTeamChat§7] " + rank.getPrefix() + player.getName() + " §8» §7" + message);
            }
        }

        CloudLogger.info("TeamChat: "+ player.getName() + " >> " + message);
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.report.RequestReportPacket;

/**
 * Created by Paul on 16.08.2016.
 */
public class ReportCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 10) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/report <Spieler>");
            return;
        }

        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);

        // Check valid arguments
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }
        if (!CloudServer.getReportManager().canReport(player)) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst nur jede Minute Spieler reporten.");
            return;
        }
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.sendMessage(CloudServer.TAG + "§cDu kannst dich selbst nicht reporten.");
            return;
        }
        if (target.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
            player.sendMessage(CloudServer.TAG + "§cDu darfst den Spieler " + target.getRank().getPrefix() + target.getName() +
                    " §cnicht reporten.");
            return;
        }

        // Report request
        player.getServer().sendPacket(new RequestReportPacket(player.getName(), target.getName(), target.getServer().getName()));
    }
}

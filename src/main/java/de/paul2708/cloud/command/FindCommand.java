package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 31.08.2016.
 */
public class FindCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length != 1) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/find <Spieler>");
            return;
        }

        // Check valid arguments
        Player target = CloudServer.getPlayerManager().getPlayer(args[0]);
        if (target == null || !target.isOnline()) {
            player.sendMessage(CloudServer.TAG + "§cDer Spieler " + args[0] + " ist offline.");
            return;
        }

        // Get server info
        player.sendMessage(CloudServer.TAG + target.getRank().getPrefix() + target.getName() + " §7befindet sich auf Server §e" + target.getServer().getName() + "§7.");
    }
}

package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;

/**
 * Created by Paul on 05.09.2016.
 */
public class BlacklistCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 120) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        if (args.length > 2 || args.length == 0) {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/blacklist <add|remove|list> [Name]");
            return;
        }

        if (args[0].equalsIgnoreCase("list")) {
            String message = "";
            for (String name : CloudServer.getBlacklistManager().getNames()) {
                message += "§e" + name + "§7, ";
            }

            if (message.equalsIgnoreCase("")) {
                player.sendMessage(CloudServer.TAG + "§7Aktuelle Blacklist: §e-/-");
            } else {
                message = message.substring(0, message.length() - 4);
                player.sendMessage(CloudServer.TAG + "§7Aktuelle Blacklist: " + message);
            }
        } else if (args[0].equalsIgnoreCase("add")) {
            if (args.length == 1) {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/blacklist <add|remove|list> [Name]");
                return;
            }
            String correct = CloudServer.getBlacklistManager().correct(args[1]);
            if (CloudServer.getBlacklistManager().getNames().contains(correct)) {
                player.sendMessage(CloudServer.TAG + "§cDer Name " + correct + " steht schon auf der Blacklist.");
                return;
            }

            CloudServer.getBlacklistManager().addName(correct);
            player.sendMessage(CloudServer.TAG + "§7Du hast den Namen §e" + correct + " §7auf die Blacklist gesetzt.");
        } else if (args[0].equalsIgnoreCase("remove")) {
            if (args.length == 1) {
                player.sendMessage(CloudServer.TAG + "§7Nutze §e/blacklist <add|remove|list> [Name]");
                return;
            }
            String correct = CloudServer.getBlacklistManager().correct(args[1]);
            if (!CloudServer.getBlacklistManager().getNames().contains(correct)) {
                player.sendMessage(CloudServer.TAG + "§cDer Name " + args[1] + " steht nicht auf der Blacklist.");
                return;
            }

            CloudServer.getBlacklistManager().removeName(correct);
            player.sendMessage(CloudServer.TAG + "§7Du hast den Namen §e" + correct + " §7von der Blacklist genommen.");
        } else {
            player.sendMessage(CloudServer.TAG + "§7Nutze §e/blacklist <add|remove|list> [Name]");
        }
    }
}

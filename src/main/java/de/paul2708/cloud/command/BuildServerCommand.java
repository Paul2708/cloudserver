package de.paul2708.cloud.command;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.player.PlayerChangeServerPacket;

/**
 * Created by Paul on 05.09.2016.
 */
public class BuildServerCommand extends CloudCommand {

    @Override
    public void execute(Player player, String[] args) {
        if (player.getRank().getPermissionLevel() < 100 && player.getRank().getPermissionLevel() != 90) {
            player.sendMessage(CloudServer.TAG + "§cDafür hast du keine Rechte!");
            return;
        }

        // Check valid arguments
        if (CloudServer.getConfigFile().getBuildServer().equalsIgnoreCase("none")) {
            player.sendMessage(CloudServer.TAG + "§cEs gibt momentan kein Bauserver.");
            return;
        }
        if (CloudServer.getServerManager().getServer(CloudServer.getConfigFile().getBuildServer()) == null) {
            player.sendMessage(CloudServer.TAG + "§cEs besteht keine Verbindung zum Bauserver.");
            return;
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerChangeServerPacket(player.getUuid(), CloudServer.getConfigFile().getBuildServer()));
        player.sendMessage(CloudServer.TAG + "§7Du wirst auf den §eBauserver §7verschoben.");
    }
}

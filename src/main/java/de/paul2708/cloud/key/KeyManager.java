package de.paul2708.cloud.key;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.key.KeyRedeemedPacket;
import lombok.Getter;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 31.12.2016.
 */
public class KeyManager {

    @Getter
    private List<Key> keys = new CopyOnWriteArrayList<>();

    public void resolveKeys() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `beta_keys`");

        try {
            while (rs.next()) {
                String key = rs.getString("code");
                String name = rs.getString("name");
                String uuid = rs.getString("uuid");
                Date date = rs.getDate("timestamp");

                Key betaKey;

                if (name == null) {
                    betaKey = new Key(key, "-/-", UUID.randomUUID(), new Date(0L));
                } else {
                    betaKey = new Key(key, name, UUID.fromString(uuid), date);
                }

                keys.add(betaKey);
            }
        } catch (Exception e) {
            CloudLogger.error("Keys: Fehler beim Bekommen", e);
        }
    }

    public void startKeyObserver() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `beta_keys`");

                try {
                    while (rs.next()) {
                        String key = rs.getString("code");
                        String name = rs.getString("name");
                        String uuid = rs.getString("uuid");
                        Date date = rs.getDate("timestamp");

                        if (name != null) {
                            Key betaKey = getKeyByCode(key);

                            if (betaKey.isRedeemed()) {
                                continue;
                            }

                            betaKey.setName(name);
                            betaKey.setUuid(UUID.fromString(uuid));
                            betaKey.setDate(date);
                            betaKey.redeemed();

                            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                                    new KeyRedeemedPacket(betaKey.getUuid()));

                            CloudLogger.info("Keys: Key an Bungee geschickt");
                        }
                    }
                } catch (Exception e) {
                    CloudLogger.error("Keys: Fehler beim Bekommen", e);
                }
            }
        }, TimeUnit.SECONDS.toMillis(30), TimeUnit.MINUTES.toMillis(1));
    }

    public void addKey(String key) {
        Key betaKey = new Key(key, "-/-", UUID.randomUUID(), new Date(0L));
        keys.add(betaKey);
        DatabaseManager.addKey(key);

        CloudLogger.info("Key: " + key + " wurde erstellt");
    }

    public String generateKey() {
        Random random = new Random();
        String[] array = new String[10];
        int index;

        for (int i = 0; i < 3; i++) {
            index = random.nextInt(10);

            if (array[index] == null) {
                array[index] = "" + random.nextInt(10);
            } else {
                while (array[index] != null) {
                    index = random.nextInt(10);

                    if (array[index] == null) {
                        break;
                    }
                }

                array[index] = "" + random.nextInt(10);
            }
        }
        for (int i = 0; i < 10; i++) {
            if (array[i] != null) {
                continue;
            }
            String letter = Util.ALPHABET[random.nextInt(Util.ALPHABET.length)];
            if (random.nextBoolean()) {
                letter = letter.toUpperCase();
            }

            array[i] = letter;
        }

        String key = "";
        for (int i = 0; i < array.length; i++) {
            key += array[i];
        }

        for (Key all : keys) {
            if (all.getKey().equalsIgnoreCase(key)) {
                return generateKey();
            }
        }

        return key;
    }

    public Key getKeyByCode(String key) {
        for (Key allKeys : keys) {
            if (allKeys.getKey().equals(key)) {
                return allKeys;
            }
        }

        return null;
    }
}

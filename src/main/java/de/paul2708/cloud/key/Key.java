package de.paul2708.cloud.key;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Paul on 31.12.2016.
 */
public class Key {

    private String key;
    private String name;
    private UUID uuid;
    private Date date;

    private boolean redeemed;

    public Key(String key, String name, UUID uuid, Date date) {
        this.key = key;
        this.name = name;
        this.uuid = uuid;
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Date getTime() {
        return date;
    }

    public void redeemed() {
        this.redeemed = true;
    }

    public boolean isRedeemed() {
        return redeemed;
    }
}

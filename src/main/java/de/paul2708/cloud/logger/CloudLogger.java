package de.paul2708.cloud.logger;

import de.paul2708.cloud.util.Util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Paul on 05.07.2017.
 */
public class CloudLogger {

    private static CloudPrinter printer;

    public static void initialize() {
        CloudLogger.printer = new CloudPrinter(System.out);

        System.setOut(printer);
        System.setErr(printer);
    }

    public static void info(String message) {
        printer.print("[" + Util.getTime(System.currentTimeMillis()) + "] " + message + "\n");
    }

    public static void error(String message, Throwable exception) {
        printer.print("[" + Util.getTime(System.currentTimeMillis()) + "] " + message + "\n");
        printer.print(getStackTrace(exception) + "\n");
    }

    private static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);

        return sw.getBuffer().toString();
    }
}

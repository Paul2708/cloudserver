package de.paul2708.cloud.logger;

import de.paul2708.cloud.CloudServer;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Paul on 05.07.2017.
 */
public class CloudPrinter extends PrintStream {

    public CloudPrinter(OutputStream out) {
        super(out);
    }

    @Override
    public void print(String input) {
        super.print(input);

        CloudServer.getLogFile().log(input);
    }
}

package de.paul2708.cloud.abuse;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.abuse.AbuseAction;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.other.AbusePacket;
import lombok.Getter;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import static de.paul2708.common.abuse.AbuseAction.*;

/**
 * Created by Paul on 11.09.2016.
 */
public class AbuseManager {

    @Getter
    private List<Abuse> abuses = new CopyOnWriteArrayList<>();

    private Map<Player, Abuse> bans = new ConcurrentHashMap<>();
    private Map<Player, Abuse> mutes = new ConcurrentHashMap<>();

    public void resolveAbuses() {
        Map<Player, List<Abuse>> tempBans = new ConcurrentHashMap<>();
        Map<Player, List<Abuse>> tempMutes = new ConcurrentHashMap<>();

        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `abuses`");

        try {
            while (rs.next()) {
                String action = rs.getString("action");
                String staff = rs.getString("staff");
                String abuser = rs.getString("abuser");
                String reason = rs.getString("reason");
                String timeStamp = rs.getString("timestamp");
                String lastTo = rs.getString("last_to");
                String staffUuid = rs.getString("staff_uuid");
                String abuserUuid = rs.getString("abuser_uuid");

                Abuse abuse = new Abuse(AbuseAction.getActionByName(action), staff, abuser, reason,
                        Long.valueOf(timeStamp), Long.valueOf(lastTo), UUID.fromString(staffUuid), UUID.fromString(abuserUuid));
                abuses.add(abuse);

                Player player = CloudServer.getPlayerManager().getPlayer(abuse.getAbuserUuid());

                // Banned
                if (abuse.getAction() == BAN) {
                    if (abuse.getLastTo() == -1 || abuse.getLastTo() > System.currentTimeMillis()) {
                        tempBans.putIfAbsent(player, new CopyOnWriteArrayList<>());
                        List<Abuse> list = tempBans.get(player);
                        list.add(abuse);
                        tempBans.put(player, list);
                    }
                // Muted
                } else if (abuse.getAction() == MUTE) {
                    if (abuse.getLastTo() == -1 || abuse.getLastTo() > System.currentTimeMillis()) {
                        tempMutes.putIfAbsent(player, new CopyOnWriteArrayList<>());
                        List<Abuse> list = tempMutes.get(player);
                        list.add(abuse);
                        tempMutes.put(player, list);
                    }
                // Pardon
                } else if (abuse.getAction() == UNBAN) {
                    tempBans.putIfAbsent(player, new CopyOnWriteArrayList<>());
                    List<Abuse> list = tempBans.get(player);
                    list.add(abuse);
                    tempBans.put(player, list);
                } else if (abuse.getAction() == UNMUTE) {
                    tempMutes.putIfAbsent(player, new CopyOnWriteArrayList<>());
                    List<Abuse> list = tempMutes.get(player);
                    list.add(abuse);
                    tempMutes.put(player, list);
                }

            }

            clean(tempBans, tempMutes);
        } catch (Exception e) {
            CloudLogger.error("Fehler beim Bekommen der Verstöße", e);
        }
    }

    public void startAbuseObserver() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                // Bans
                for (Player player : bans.keySet()) {
                    Abuse abuse = bans.get(player);

                    if (abuse == null) continue;
                    if (abuse.getLastTo() < System.currentTimeMillis() && abuse.getLastTo() != -1L) {
                        Abuse pardon = new Abuse(UNBAN, "Automatic", abuse.getAbuser(),
                                "time is over", System.currentTimeMillis(), -1L, UUID.randomUUID(), abuse.getAbuserUuid());
                        addAbuse(pardon);
                    }
                }

                // Mutes
                for (Player player : mutes.keySet()) {
                    Abuse abuse = mutes.get(player);

                    if (abuse == null) continue;
                    if (abuse.getLastTo() < System.currentTimeMillis() && abuse.getLastTo() != -1L) {
                        Abuse pardon = new Abuse(UNMUTE, "Automatic", abuse.getAbuser(),
                                "time is over", System.currentTimeMillis(), -1L, UUID.randomUUID(), abuse.getAbuserUuid());
                        addAbuse(pardon);
                    }
                }
            }
        }, TimeUnit.MINUTES.toMillis(1), TimeUnit.MINUTES.toMillis(1));
    }

    public Abuse isBanned(Player player) {
        if (bans.containsKey(player)) {
            return bans.get(player);
        }

        return null;
    }

    public Abuse isMuted(Player player) {
        if (mutes.containsKey(player)) {
            return mutes.get(player);
        }

        return null;
    }

    public void clean(Map<Player, List<Abuse>> tempBans, Map<Player, List<Abuse>> tempMutes) {
        // Banned
        for (Player player : tempBans.keySet()) {
            List<Abuse> list = tempBans.get(player);
            if (getAbuses(BAN, player, tempBans, tempMutes) <=
                    getAbuses(UNBAN, player, tempBans, tempMutes)) {
                list.clear();
            } else {
                Abuse last = tempBans.get(player).get(tempBans.get(player).size() - 1);
                list.clear();
                list.add(last);
                tempBans.put(player, list);
            }

            if (list.size() == 1) {
                bans.put(player, list.get(0));
            }
        }

        // Muted
        for (Player player : tempMutes.keySet()) {
            List<Abuse> list = tempMutes.get(player);
            if (getAbuses(MUTE, player, tempBans, tempMutes) <=
                    getAbuses(UNMUTE, player, tempBans, tempMutes)) {
                list.clear();
            } else {
                Abuse last = tempMutes.get(player).get(tempMutes.get(player).size() - 1);
                list.clear();
                list.add(last);
                tempMutes.put(player, list);
            }

            if (list.size() == 1) {
                mutes.put(player, list.get(0));
            }
        }
    }

    public void addAbuse(Abuse abuse) {
        abuses.add(abuse);
        DatabaseManager.addAbuse(abuse);

        Player abuser = CloudServer.getPlayerManager().getPlayer(abuse.getAbuserUuid());

        if (abuse.getAction() == BAN) {
            bans.put(abuser, abuse);

            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new AbusePacket("add", abuse));
        } else if (abuse.getAction() == MUTE) {
            mutes.put(abuser, abuse);

            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new AbusePacket("add", abuse));
        } else if (abuse.getAction() == UNBAN) {
            Abuse last = bans.get(abuser);
            bans.remove(abuser);

            abuser.setBanReason("");
            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new AbusePacket("remove", last));
        } else if (abuse.getAction() == UNMUTE) {
            Abuse last = mutes.get(abuser);
            mutes.remove(abuser);

            abuser.setMuteReason("");
            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new AbusePacket("remove", last));
        } else if (abuse.getAction() == WARN) {
            WarnReason reason = WarnReason.getByName(abuse.getReason());

            int banPoints = WarnReason.getByName(abuse.getReason()).getBanPoints();
            abuser.setBanPoints(abuser.getBanPoints() + banPoints);
            int mutePoints = WarnReason.getByName(abuse.getReason()).getMutePoints();
            abuser.setMutePoints(abuser.getMutePoints() + mutePoints);

            // Send message to reporter
            for (Map.Entry<Player, Player> entry : CloudServer.getReportManager().getReportMessage().entrySet()) {
                Player badPerson = entry.getValue();
                Player reporter = entry.getKey();

                if (badPerson.equals(abuser)) {
                    if (reporter != null) {
                        reporter.sendMessage(CloudServer.TAG + "§eDer Spieler, den du reportet hast, wurde bestraft.");
                        reporter.sendMessage(CloudServer.TAG + "§e +200 Tokens");
                        reporter.updateTokens(200);
                        CloudServer.getReportManager().getReportMessage().remove(reporter);
                    }
                }
            }

            // Muten
            if (reason.getMutePoints() != 0 && WarnReason.getMuteLength(abuser.getMutePoints()) != -1L) {
                mute(CloudServer.getPlayerManager().getPlayer(abuse.getStaff()),
                        abuser, abuse.getReason(), System.currentTimeMillis() + WarnReason.getMuteLength(abuser.getMutePoints()));
            }
            // Bannen
            if (reason.getBanPoints() != 0 && WarnReason.getBanLength(abuser.getBanPoints()) != -1L) {
                if (WarnReason.getBanLength(abuser.getBanPoints()) == 0L) {
                    ban(CloudServer.getPlayerManager().getPlayer(abuse.getStaff()),
                            abuser, abuse.getReason(), -1L);
                    return;
                }

                ban(CloudServer.getPlayerManager().getPlayer(abuse.getStaff()),
                        abuser, abuse.getReason(), System.currentTimeMillis() + WarnReason.getBanLength(abuser.getBanPoints()));
            }
        }

        CloudLogger.info("AbuseUpdate: " + abuse.getAction() + " " + abuse.getStaff() + " -> " + abuse.getAbuser() + " Grund: " + abuse.getReason());
    }

    public void mute(Player player, Player target, String reason, long end) {
        if (isMuted(target) != null) {
            player.sendMessage(CloudServer.TAG + "Der Spieler ist bereits gemutet.");
            return;
        }

        target.setMuteReason(reason);
        Abuse abuse = new Abuse(MUTE, player.getName(), target.getName(), reason,
                System.currentTimeMillis(), end, UUID.fromString(player.getUuid()), UUID.fromString(target.getUuid()));
        CloudServer.getAbuseManager().addAbuse(abuse);

        target.sendMessages(CloudServer.TAG + "§cDu wurdest gemutet.", CloudServer.TAG + "§7Ende: §e" + (end == -1 ? "permanent" :
                Util.getTime(end)), CloudServer.TAG + "§7Grund: §e" + reason);

        CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(online -> online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).filter(Player::hasNotification).forEach(online -> online.sendMessages(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + " §7hat " +
                        target.getRank().getPrefix() + target.getName() + " §7gemutet.",
                CloudServer.TAG + "§7Grund: §e" + reason +
                        " §7(§e" + (end == -1 ? "permanent§7)" : Util.getTime(end) + "§7)")));
    }

    public void ban(Player player, Player target, String reason, long end) {
        if (isBanned(target) != null) {
            player.sendMessage(CloudServer.TAG + "Der Spieler ist bereits gebannt.");
            return;
        }

        target.setBanReason(reason);
        Abuse abuse = new Abuse(BAN, player.getName(), target.getName(), reason,
                System.currentTimeMillis(), end, UUID.fromString(player.getUuid()), UUID.fromString(target.getUuid()));
        CloudServer.getAbuseManager().addAbuse(abuse);

        target.kick("§cDu wurdest gebannt.\n§7Ende: §c" + (end == -1 ? "permanent" :
                Util.getTime(end)) + "\n§7Grund: §e" + reason);

        CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(online -> online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).filter(Player::hasNotification).forEach(online -> online.sendMessages(CloudServer.TAG + player.getRank().getPrefix() + player.getName() + " §7hat " +
                        target.getRank().getPrefix() + target.getName() + " §7vom Netzwerk gebannt.",
                CloudServer.TAG + "§7Grund: §e" + reason +
                        " §7(§e" + (end == -1 ? "permanent§7)" : Util.getTime(end) + "§7)")));
    }

    public ArrayList<Abuse> getActiveAbuses() {
        ArrayList<Abuse> list = new ArrayList<>();

        bans.values().forEach(list::add);
        bans.values().forEach(list::add);

        return list;
    }

    private int getAbuses(AbuseAction action, Player abuser, Map<Player,
            List<Abuse>> tempBans, Map<Player, List<Abuse>> tempMutes) {
        int size = 0;
        if (action == BAN || action == UNBAN) {
            for (Abuse list : tempBans.get(abuser)) {
                if (list.getAction() == action) {
                    size++;
                }
            }
        } else if (action == MUTE || action == UNMUTE) {
            for (Abuse list : tempMutes.get(abuser)) {
                if (list.getAction() == action) {
                    size++;
                }
            }
        }

        return size;
    }
}

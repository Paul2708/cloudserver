package de.paul2708.cloud.abuse;

import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 04.10.2016.
 */
public enum WarnReason {

    INSULT("Beleidigung", 0, 2),
    SPAM("Spam", 0, 1),
    PROVOCATION("Provokation", 0, 2),
    THREAT("Drohung", 0, 6),
    SERVER_INSULT("Server-Beleidigung", 0, 2),
    SEXUAL_HARASSMENT("Sexuelle-Belästigung", 0, 6),
    RACISM("Rassismus", 12, 0),
    SCAM("Betrug", 30, 0),
    ADVERTISMENT("Werbung", 0, 30),
    TROLLING("Trolling", 3, 0),
    HACKING("Hacking", 12, 0),
    BUGUSING("Bugusing", 12, 0),
    NAME("Username", 30, 0),
    SKIN("Skin", 15, 0),
    TEAMING("Teaming", 3, 0),
    BYPASS("Banumgehung", 30, 0),
    DISREGARD("Missachtung", 10, 0),
    ;

    private String name;
    private int banPoints;
    private int mutePoints;

    WarnReason(String name, int banPoints, int mutePoints) {
        this.name = name;
        this.banPoints = banPoints;
        this.mutePoints = mutePoints;
    }

    public String getName() {
        return name;
    }

    public int getBanPoints() {
        return banPoints;
    }

    public int getMutePoints() {
        return mutePoints;
    }

    public static long getBanLength(int points) {
        if (points == 3) {
            return TimeUnit.DAYS.toMillis(1);
        } else if (between(4, 6, points)) {
            return TimeUnit.DAYS.toMillis(7);
        } else if (between(7, 12, points)) {
            return TimeUnit.DAYS.toMillis(14);
        } else if (between(13, 29, points)) {
            return TimeUnit.DAYS.toMillis(30);
        } else if (points >= 30) {
            return 0L;
        }

        return -1L;
    }

    public static long getMuteLength(int points) {
        if (points == 1) {
            return TimeUnit.MINUTES.toMillis(5);
        } else if (between(2, 3, points)) {
            return TimeUnit.MINUTES.toMillis(10);
        } else if (between(4, 6, points)) {
            return TimeUnit.MINUTES.toMillis(30);
        } else if (between(7, 12, points)) {
            return TimeUnit.MINUTES.toMillis(60);
        } else if (points >= 13) {
            return TimeUnit.MINUTES.toMillis(120);
        }

        return -1L;
    }

    public static WarnReason getByName(String name) {
        for (WarnReason reason : values()) {
            if (reason.getName().equalsIgnoreCase(name)) {
                return reason;
            }
        }

        return null;
    }

    public static String getReasons() {
        String message = "";
        for (WarnReason reason : values()) {
            message += "§e" + reason.getName() + "§7, ";
        }

        message = message.substring(0, message.length() - 4);

        return message;
    }

    private static boolean between(int a, int b, int input) {
        return input >= a && input <= b;
    }
}

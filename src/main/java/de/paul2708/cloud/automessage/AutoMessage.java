package de.paul2708.cloud.automessage;

import de.paul2708.cloud.CloudServer;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.command.BroadcastPacket;

/**
 * Created by Paul on 02.09.2016.
 */
public class AutoMessage {

    private String message;

    public AutoMessage(String message) {
        this.message = message.replaceAll("&", "§");
    }

    public void broadcast() {
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new BroadcastPacket(message));
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AutoMessage) {
            AutoMessage message = (AutoMessage) obj;

            if (message.getMessage().equalsIgnoreCase(getMessage())) {
                return true;
            }
        }

        return false;
    }
}

package de.paul2708.cloud.automessage;

import de.paul2708.cloud.CloudServer;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 02.09.2016.
 */
public class AutoMessageManager {

    // TODO: Clickable

    private Timer timer;

    private List<AutoMessage> messages = new CopyOnWriteArrayList<>();
    private int delay;
    private AutoMessage lastMessage;
    private Random random;

    public void start() {
        if (!CloudServer.getAutoMessageFile().isEnabled()) {
            return;
        }

        loadMessages();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                getRandomMessage().broadcast();
            }
        }, TimeUnit.MINUTES.toMillis(delay), TimeUnit.MINUTES.toMillis(delay));
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }

        messages.clear();
    }

    private void loadMessages() {
        messages.clear();

        for (String message : CloudServer.getAutoMessageFile().getMessages()) {
            AutoMessage autoMessage = new AutoMessage(message);
            messages.add(autoMessage);
        }

        this.delay = CloudServer.getAutoMessageFile().getDelay();
        this.random = new Random();
    }

    private AutoMessage getRandomMessage() {
        if (messages.size() == 0) {
            return new AutoMessage("Es gibt keine Nachrichten, aber auto-message ist true.");
        } else if (messages.size() == 1) {
            return messages.get(0);
        } else {
            if (lastMessage == null) {
                int index = random.nextInt(messages.size());
                this.lastMessage = messages.get(index);
                return lastMessage;
            } else {
                AutoMessage randomMessage = messages.get(random.nextInt(messages.size()));
                while (lastMessage.equals(randomMessage)) {
                    randomMessage = messages.get(random.nextInt(messages.size()));
                }

                lastMessage = randomMessage;
                return lastMessage;
            }
        }
    }
}

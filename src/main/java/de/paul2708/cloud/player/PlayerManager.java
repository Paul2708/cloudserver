package de.paul2708.cloud.player;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.rank.RankManager;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.packet.player.PlayerLoginPacket;
import de.paul2708.common.packet.player.PlayerLogoutPacket;
import lombok.Getter;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Paul on 07.08.2016.
 */
public class PlayerManager {

    @Getter
    private Map<String, Player> players = new ConcurrentHashMap<>();

    @Getter
    private List<Player> onlinePlayers = new CopyOnWriteArrayList<>();

    public void resolvePlayers() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `players`");

        try {
            while (rs.next()) {
                String name = rs.getString("name");
                String uuid = rs.getString("uuid");
                String rank = rs.getString("rank");
                String rankEnd = rs.getString("rank_end");
                String ip = rs.getString("ip");
                String teamSpeakUID = rs.getString("teamspeak_uid");
                String firstLogin = rs.getString("first_login");
                String lastLogin = rs.getString("last_login");
                String playTime = rs.getString("play_time");
                String banReason = rs.getString("ban_reason");
                String muteReason = rs.getString("mute_reason");
                int banPoints = rs.getInt("ban_points");
                int mutePoints = rs.getInt("mute_points");
                int tokens = rs.getInt("tokens");
                String notification = rs.getString("notification");

                if (players.containsKey(uuid)) {
                    CloudLogger.info("Player: doppelter Datenbankeintrag für " + name);
                    continue;
                }

                Player player = new Player(name, uuid, rank, rankEnd, ip, teamSpeakUID, firstLogin, lastLogin, playTime,
                        banReason, muteReason, banPoints, mutePoints, tokens, notification);
                players.put(uuid, player);
            }

            CloudLogger.info("Player: " + players.size() + " Spieler geladen");
        } catch (Exception e) {
            CloudLogger.error("Player: Fehler beim Bekommen der Spieler", e);
        }
    }

    public void startPlayTimeObserver() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                for (Player player : getOnlinePlayers()) {
                    int playTime = player.getPlayTime();
                    player.setPlayTime(playTime + 1);
                }
            }
        }, TimeUnit.MINUTES.toMillis(1), TimeUnit.MINUTES.toMillis(1));
    }

    public void loginPlayer(PlayerLoginPacket packet) {
        Player player;
        if (!players.containsKey(packet.getUuid())) {
            player = new Player(packet.getName(), packet.getUuid(), RankManager.DEFAULT_RANK, "-1", packet.getIp(), "-1", "" + System.currentTimeMillis(),
                    "" + System.currentTimeMillis(), "0", "", "", 0, 0, 0, "true");

            player.setOnline(true);
            player.setRankEnd(-1L);
            player.setRank(CloudServer.getRankManager().getRank(RankManager.DEFAULT_RANK));
            player.notification(true);

            players.put(packet.getUuid(), player);
            player.save(false);

            onlinePlayers.add(player);
            CloudLogger.info("Player: " + packet.getName() + " hat das Netzwerk zum erste Mal betreten");
        } else {
            player = players.get(packet.getUuid());

            player.setOnline(true);
            player.setName(packet.getName());
            player.setIp(packet.getIp());
            player.setLastLogin(System.currentTimeMillis());

            player.save(true);

            onlinePlayers.add(player);
            CloudLogger.info("Player: " + packet.getName() + " hat das Netzwerk betreten");
        }

        getOnlinePlayers().stream().filter(online -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() && online.hasNotification()).filter(online -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).forEach(online -> online.sendMessage(CloudServer.TAG +
                player.getRank().getPrefix() + player.getName() + " §7hat das Netzwerk betreten."));

        for (Abuse abuse : CloudServer.getAbuseManager().getActiveAbuses()) {
            Player target = CloudServer.getPlayerManager().getPlayer(abuse.getAbuserUuid());

            if (target.getIp().equalsIgnoreCase(packet.getIp())) {
                CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(online -> online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).forEach(online -> {
                    if (online.hasNotification()) {
                        online.sendMessage(CloudServer.TAG + "§7Der Spieler §e" + packet.getName() + " §7hat mit einer gebannten IP das Netzwerk betreten. (§e" + abuse.getReason() + "§7)");
                    }
                });

                break;
            }
        }
    }

    public void logoutPlayer(PlayerLogoutPacket packet) {
        Player player = players.get(packet.getUuid());

        player.setOnline(false);
        player.save(true);

        onlinePlayers.remove(player);
        CloudLogger.info("Player: " + player.getName() + " hat sich ausgeloggt.");

        getOnlinePlayers().stream().filter(online -> online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() && online.hasNotification()).filter(online -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).forEach(online -> online.sendMessage(CloudServer.TAG +
                player.getRank().getPrefix() + player.getName() + " §7hat das Netzwerk verlassen."));
    }

    public void kickAll(String reason) {
        for (Player player : getOnlinePlayers()) {
            if (player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()) {
                continue;
            }

            player.kick(reason);
        }
    }

    public void sendToTeam(String message) {
        getOnlinePlayers().stream().filter(online ->
                online.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                        online.hasNotification()).forEach(online -> online.sendMessage(message));
    }

    public Player getPlayer(String name) {
        for (Player player : players.values()) {
            if (player.getName().equalsIgnoreCase(name)) {
                return player;
            }
        }

        return null;
    }

    public Player getPlayer(Client client) {
        for (Player player : players.values()) {
            if (player.getTeamSpeakUID().equalsIgnoreCase(client.getUniqueIdentifier())) {
                return player;
            }
        }

        return null;
    }

    public Player getPlayer(UUID uuid) {
        for (Player player : players.values()) {
            if (player.getUuid().equalsIgnoreCase(uuid.toString())) {
                return player;
            }
        }

        return null;
    }

    public ArrayList<UUID> getTeamMember() {
        ArrayList<UUID> list = getPlayers().values().stream().filter(player -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).map(player -> UUID.fromString(player.getUuid())).collect(Collectors.toCollection(ArrayList::new));
        return list;
    }

    public ArrayList<UUID> getPremiumMember() {
        ArrayList<UUID> list = getPlayers().values().stream().filter(player -> player.getRank().getPermissionLevel() >= 20).map(player -> UUID.fromString(player.getUuid())).collect(Collectors.toCollection(ArrayList::new));
        return list;
    }
}

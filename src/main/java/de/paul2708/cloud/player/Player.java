package de.paul2708.cloud.player;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.command.CommandResolver;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.command.CommandType;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.other.ClickMessagePacket;
import de.paul2708.common.packet.player.PlayerChangeServerPacket;
import de.paul2708.common.packet.player.PlayerKickPacket;
import de.paul2708.common.packet.player.PlayerMessagePacket;
import de.paul2708.common.packet.player.PlayerSoundPacket;
import de.paul2708.common.rank.Rank;
import lombok.Getter;
import lombok.Setter;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class Player {

    // Ids
    @Getter
    @Setter
    private String name, uuid, ip, teamSpeakUID;

    // Rank
    @Getter
    private Rank rank;

    // Time
    @Getter
    @Setter
    private long firstLogin, lastLogin, rankEnd;

    @Getter
    @Setter
    private int playTime;

    // Ban, Mute Data
    @Getter
    @Setter
    private String banReason, muteReason;

    @Getter
    @Setter
    private int banPoints, mutePoints;

    // Tokens
    @Getter
    private int tokens;

    // Notification
    private boolean notification;

    // Online
    @Getter
    @Setter
    private boolean online;

    @Getter
    @Setter
    private SpigotServer server;

    public Player(String name, String uuid, String rank, String rankEnd, String ip, String teamSpeakUID, String firstLogin, String lastLogin,
                  String playTime, String banReason, String muteReason, int banPoints, int mutePoints, int tokens, String notification) {
        this.name = name;
        this.uuid = uuid;
        this.rank = CloudServer.getRankManager().getRank(rank);
        this.rankEnd = Long.valueOf(rankEnd);
        this.ip = ip;
        this.teamSpeakUID = teamSpeakUID;
        this.firstLogin = Long.valueOf(firstLogin);
        this.lastLogin = Long.valueOf(lastLogin);
        this.playTime = Integer.valueOf(playTime);
        this.banReason = banReason;
        this.muteReason = muteReason;
        this.banPoints = banPoints;
        this.mutePoints = mutePoints;
        this.tokens = tokens;
        this.notification = Boolean.valueOf(notification);

        this.online = false;
        this.server = null;
    }

    public void save(boolean inDatabase) {
        if (inDatabase) {
            try {
                PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                        "UPDATE players SET name = ?, ip = ?, rank = ?, rank_end = ?, first_login = ?, last_login = ?, play_time = ?, " +
                                "teamspeak_uid = ?, ban_reason = ?, mute_reason = ?, ban_points = ?, mute_points = ?, tokens = ?, notification = ? WHERE uuid = ?");

                update.setString(1, getName());
                update.setString(2, getIp());
                update.setString(3, getRank().getName());
                update.setString(4, getRankEnd() + "");
                update.setString(5, getFirstLogin() + "");
                update.setString(6, getLastLogin() + "");
                update.setString(7, getPlayTime() + "");
                update.setString(8, getTeamSpeakUID());
                update.setString(9, getBanReason());
                update.setString(10, getMuteReason());
                update.setInt(11, getBanPoints());
                update.setInt(12, getMutePoints());
                update.setInt(13, getTokens());
                update.setString(14, notification ? "true" : "false");
                update.setString(15, getUuid());

                update.executeUpdate();
            } catch (Exception e) {
                CloudLogger.error("Player: Fehler beim Speichern (" + getName() + ") " + name, e);
            }
        } else {
            try {
                PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                        "INSERT IGNORE INTO `players` ("
                                + "`name`, `uuid`, `ip`, `rank`, `rank_end`, `first_login`, `last_login`, `play_time`, `teamspeak_uid`, " +
                                "`ban_reason`,  `mute_reason`, `ban_points`, `mute_points`, `tokens`, `notification`"
                                + ") VALUES ("
                                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?"
                                + ")");

                update.setString(1, getName());
                update.setString(2, getUuid());
                update.setString(3, getIp());
                update.setString(4, getRank().getName());
                update.setString(5, getRankEnd() + "");
                update.setString(6, getFirstLogin() + "");
                update.setString(7, getLastLogin() + "");
                update.setString(8, getPlayTime() + "");
                update.setString(9, getTeamSpeakUID());
                update.setString(10, getBanReason());
                update.setString(11, getMuteReason());
                update.setInt(12, getBanPoints());
                update.setInt(13, getMutePoints());
                update.setInt(14, getTokens());
                update.setString(15, notification ? "true" : "false");

                update.executeUpdate();
            } catch (Exception e) {
                CloudLogger.error("Player: Fehler beim Speichern (" + getName() + ") " + name, e);
            }
        }
    }

    public void updateTokens(int amount) {
        this.tokens = tokens + amount;
        if (!online) {
            save(true);
        }
    }

    public void notification(boolean notification) {
        this.notification = notification;
    }

    public boolean hasNotification() {
        return notification;
    }

    public void sendMessage(String message) {
        if (!online) {
            return;
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerMessagePacket(name, message));
    }

    public void sendMessages(List<String> messages) {
        if (!online) {
            return;
        }
        ArrayList<String> list = new ArrayList<>();
        messages.forEach(list::add);

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerMessagePacket(name, list.toArray(new String[list.size()])));
    }

    public void sendMessages(String... messages) {
        if (!online) {
            return;
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerMessagePacket(name, messages));
    }

    public void sendClickableMessage(String message, String hover, String command, String suggest) {
        server.sendPacket(new ClickMessagePacket(UUID.fromString(uuid), message, hover, command, suggest));
    }

    public void send(String serverName) {
        SpigotServer server = CloudServer.getServerManager().getServer(serverName);

        if (server == null) {
            CloudLogger.error("Server: " + serverName + " wurde nicht gefunden", new IllegalArgumentException());
            return;
        }

        // Join able
        if (server.getGameState() == GameState.SETUP) {
            if (rank.getPermissionLevel() < 111) {
                sendMessage("§cDer Server befindet sich gerade im Setup-State.");
                return;
            }
        }
        if (!server.isJoinAble()) {
            sendMessage("§cDu kannst den Server nicht betreten.");
            return;
        }

        // Full server
        if (server.getOnlinePlayers() >= server.getMaxPlayers()) {
            // Check kick permission
            if (rank.getPermissionLevel() >= 20) {
                // Check if kickable game state
                if (server.getGameState() == GameState.NONE || server.getGameState() == GameState.PREMIUM_ONLY) {
                    // Kick player
                    Player target = Util.getKickAblePlayer(server, -1, 0);
                    if (target == null) {
                        sendMessage(CloudServer.TAG + "§cDu kannst keinen kicken, da jeder den gleichen oder einen höheren Rang hat.");
                        return;
                    } else {
                        target.sendMessage("§cDu wurdest von einem Premium oder höher gekickt.");
                        target.performCommand(CommandType.LOBBY);
                    }
                } /*else {
                    sendMessage(CloudServer.TAG + "§cDer Server ist voll. §7Du kannst den Server nicht betreten.");
                    return;
                }*/
            } else {
                sendMessage(CloudServer.TAG + "§cDer Server ist voll. §bKaufe die §6Premium §bum volle Spiele joinen zu können");
                return;
            }
        }

        // Check build server
        if (server.getName().equalsIgnoreCase(CloudServer.getConfigFile().getBuildServer())) {
            if (rank.getPermissionLevel() < 100 && rank.getPermissionLevel() != 95) {
                sendMessage(CloudServer.TAG + "§cDu darfst den Bauserver nicht betreten!");
                return;
            }
        }

        // Check silent lobby
        if (server.getName().equalsIgnoreCase("LobbySilent")) {
            if (rank.getPermissionLevel() < 80 || rank.getPermissionLevel() == 96) {
                sendMessage(CloudServer.TAG + "§cDu hast keine Berechtigung um die Silentlobby zu betreten!");
                return;
            }
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new PlayerChangeServerPacket(uuid, serverName));
    }

    public void send(SpigotServer server) {
       send(server.getName());
    }

    public void performCommand(CommandType type, String... args) {
        CommandResolver.resolve(UUID.fromString(uuid), type.getCommand(), args);
    }

    public void setRank(Rank rank) {
        this.rank = rank;

        if (!online) {
            DatabaseManager.saveRank(this);
        }
        if (!Objects.equals(teamSpeakUID, "-1")) {
            if (CloudServer.getTeamspeakBot() == null) {
                return;
            }
            
            CloudServer.getTeamspeakBot().setServerGroup(
                    CloudServer.getTeamspeakBot().getApi().getDatabaseClientByUId(teamSpeakUID),
                    rank.getTeamspeakGroudId());
        }
    }

    public void setBanReason(String reason) {
        this.banReason = reason;

        if (!online) {
            DatabaseManager.saveBanReason(this);
        }
    }

    public void setMuteReason(String reason) {
        this.muteReason = reason;

        if (!online) {
            DatabaseManager.saveMuteReason(this);
        }
    }

    public void kick(String reason) {
        if (!online) {
            return;
        }

        CloudServer.getPacketServer().send(ClientType.BUNGEE, new PlayerKickPacket(name, reason));
    }

    public void playSound(String sound) {
        server.sendPacket(new PlayerSoundPacket(UUID.fromString(uuid), sound));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Player) {
            Player player = (Player) obj;

            if (player.getUuid().equals(this.uuid)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", uuid='" + uuid + '\'' +
                ", ip='" + ip + '\'' +
                ", teamSpeakUID='" + teamSpeakUID + '\'' +
                ", rank=" + rank +
                ", firstLogin=" + firstLogin +
                ", lastLogin=" + lastLogin +
                ", rankEnd=" + rankEnd +
                ", playTime=" + playTime +
                ", banReason='" + banReason + '\'' +
                ", muteReason='" + muteReason + '\'' +
                ", banPoints=" + banPoints +
                ", mutePoints=" + mutePoints +
                ", tokens=" + tokens +
                ", notification=" + notification +
                ", online=" + online +
                ", server=" + server +
                '}';
    }
}

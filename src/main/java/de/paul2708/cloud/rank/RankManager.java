package de.paul2708.cloud.rank;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import de.paul2708.common.rank.Rank;
import lombok.Getter;

import java.sql.ResultSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 08.08.2016.
 */
public class RankManager {

    public static final String DEFAULT_RANK = CloudServer.getConfigFile().getDefaultRank();

    @Getter
    private List<Rank> ranks = new CopyOnWriteArrayList<>();

    public void resolveRanks() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `ranks`");

        try {
            while (rs.next()) {
                String name = rs.getString("name");
                String tag = rs.getString("tag");
                String shortTag = rs.getString("short_tag");
                int permissionLevel = rs.getInt("permission_level");
                String permission = rs.getString("permission");
                int groupId = rs.getInt("teamspeak_id");

                Rank rank = new Rank(name, tag, shortTag, permissionLevel, permission, groupId);
                ranks.add(rank);
            }
        } catch (Exception e) {
            CloudLogger.error("Rank: Fehler beim Bekommen", e);
        }
    }

    public void startRankRemover() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                for (Player player : CloudServer.getPlayerManager().getPlayers().values()) {
                    long current = System.currentTimeMillis();

                    if (player.getRankEnd() == -1L) continue;

                    if (current >= player.getRankEnd()) {
                        Rank oldRank = player.getRank();
                        Rank newRank = CloudServer.getRankManager().getRank(RankManager.DEFAULT_RANK);

                        player.setRankEnd(-1);
                        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                                new RankUpdatePacket(player.getUuid(), oldRank,
                                        newRank, -1L));
                        CloudServer.getPacketServer().send(ClientType.SPIGOT,
                                new RankUpdatePacket(player.getUuid(), oldRank,
                                        newRank, -1L));
                        player.setRank(newRank);

                        if (player.isOnline()) {
                            player.sendMessage(CloudServer.TAG + "§cDeine Zeit als " +
                                    oldRank.getPrefix() + oldRank.getName() + " §cist abgelaufen.");
                        }

                        CloudLogger.info("Rank: " + oldRank.getName() + " -> " + newRank.getName() + " für " + player.getName() +
                                " von CloudServer (Zeit abgelaufen)");
                    }
                }
            }
        }, TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(5));
    }

    public Rank getRank(String name) {
        for (Rank rank : ranks) {
            if (rank.getName().equalsIgnoreCase(name)) {
                return rank;
            }
        }

        return null;
    }
}

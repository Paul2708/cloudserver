package de.paul2708.cloud.network;

import de.jackwhite20.cascade.shared.protocol.Protocol;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.paul2708.common.packet.PacketManager;

/**
 * Created by Paul on 07.10.2016.
 */
public class PacketServerProtocol extends Protocol {

    public PacketServerProtocol(PacketListener... listeners) {
        for (PacketListener listener : listeners) {
            registerListener(listener);
        }

        for (Class<? extends Packet> clazz : PacketManager.packets) {
            registerPacket(clazz);
        }
    }
}

package de.paul2708.cloud.network;

import de.jackwhite20.cascade.server.impl.ServerConfig;
import de.jackwhite20.cascade.shared.protocol.Protocol;
import de.paul2708.cloud.CloudServer;

import java.net.StandardSocketOptions;

/**
 * Created by Paul on 07.10.2016.
 */
public class PacketServerConfig extends ServerConfig {

    public PacketServerConfig(Protocol protocol) {
        host(CloudServer.getAddressesFile().getPacketIp());
        port(CloudServer.getAddressesFile().getPort());
        workerThreads(2);
        backlog(200);
        option(StandardSocketOptions.TCP_NODELAY, true);
        protocol(protocol);
    }
}

package de.paul2708.cloud.network;

import de.jackwhite20.cascade.server.Server;
import de.jackwhite20.cascade.server.ServerFactory;
import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.session.Session;
import de.jackwhite20.cascade.shared.session.SessionListener;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.key.Key;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.network.listener.*;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.client.ClientLoginPacket;
import de.paul2708.common.packet.client.CloudStatusPacket;
import de.paul2708.common.packet.key.KeyRedeemedPacket;
import de.paul2708.common.packet.server.BungeeRegistrationPacket;
import de.paul2708.common.packet.server.ServerRegistrationPacket;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 07.10.2016.
 */
public class PacketServer implements PacketListener {

    private Server server;
    private Map<String, Session> sessions;

    public PacketServer() {
        this.sessions = new ConcurrentHashMap<>();
    }

    public void start() {
        server = ServerFactory.create(new PacketServerConfig(new PacketServerProtocol(
                this, new PlayerPacketHandler(), new ReportPacketHandler(), new CommandPacketHandler(),
                new ApiPacketHandler(), new StatsPacketHandler(), new ServerPacketHandler())));

        server.addSessionListener(new SessionListener() {

            @Override
            public void onConnected(Session session) {
                // Logger.log("Neuer Client verbunden. Warten auf LoginPacket...");
            }

            @Override
            public void onDisconnected(Session session) {
                String name = getName(session);
                if (name.startsWith("Server")) {
                    String serverName = name.split(" ")[1];
                    CloudServer.getServerManager().removeServer(serverName);
                }

                // Logger.log(name + " hat die verbindung getrennt.");
            }

            @Override
            public void onStarted() {
                CloudLogger.info("Packet: Server wurde gestartet.");
            }

            @Override
            public void onStopped() {
                CloudLogger.info("Packet: Server wurde gestoppt.");
            }
        });

        server.start();
    }

    public void send(ClientType type, Packet packet) {
        sessions.entrySet().stream().filter(entry -> entry.getKey().startsWith(type.getName())).forEach(entry -> entry.getValue().send(packet));
    }

    public void sendToAPI(String apiName, Packet packet) {
        for (Map.Entry<String, Session> entry : sessions.entrySet()) {
            if (entry.getKey().contains("CloudAPI") && entry.getKey().contains(apiName)) {
                entry.getValue().send(packet);
                break;
            }
        }
    }

    public void sendToLobby(Packet packet) {
        for (SpigotServer server : CloudServer.getServerManager().getServers()) {
            if (server.getName().startsWith(CloudServer.getConfigFile().getLobbyTag())) {
                server.sendPacket(packet);
            }
        }
    }

    @PacketHandler
    public void onLoginPacket(Session session, ClientLoginPacket packet) {
        String name = packet.getType().getName() + ":" + packet.getName();
        sessions.put(name, session);

        if (packet.getType() == ClientType.BUNGEE) {
            CloudLogger.info("Packet: BungeeCord wurde registriert.");

            session.send(new BungeeRegistrationPacket(
                    CloudServer.getConfigFile().getMotd(),
                    CloudServer.getConfigFile().getMaxPlayers(),
                    CloudServer.getConfigFile().getTeamPermissionLevel(),
                    CloudServer.getBlacklistManager().getNamesAsArrayList(),
                    CloudServer.getMaintenanceFile().isEnabled(),
                    CloudServer.getMaintenanceFile().getReason(),
                    CloudServer.getPlayerManager().getTeamMember(),
                    CloudServer.getPlayerManager().getPremiumMember(),
                    CloudServer.getAbuseManager().getActiveAbuses())
            );
            CloudServer.getKeyManager().getKeys().stream().filter(Key::isRedeemed).forEach(key -> {
                session.send(new KeyRedeemedPacket(key.getUuid()));
            });
            session.send(new CloudStatusPacket((CloudServer.isStarted() ? "started" : "loading")));
        } else if (packet.getType() == ClientType.API) {
            // Logger.log("CloudAPI (" + name.split(":")[1] + ") hat sich registriert.");
        }
    }

    @PacketHandler
    public void onServerRegistration(Session session, ServerRegistrationPacket packet) {
        String serverName = packet.getName();
        String ip = packet.getIp();
        int port = packet.getPort();
        boolean listed = packet.isListed();
        boolean gameServer = packet.isGameServer();

        CloudLogger.info("Server: " + serverName + ", " + ip + ":" + port + " hat sich registriert");

        SpigotServer server = new SpigotServer(serverName, ip, port, listed, gameServer, session);
        server.setMotd(packet.getMotd());
        server.setMaxPlayers(packet.getMaxPlayers());

        CloudServer.getServerManager().addServer(server);
    }

    private String getName(Session session) {
        for (Map.Entry<String, Session> entry : sessions.entrySet()) {
            if (entry.getValue().equals(session)) {
                String name = entry.getKey();
                if (name.startsWith("SpigotServer")) {
                    name = "Server " + name.split("\\:")[1];
                } else if (name.startsWith("CloudAPI:server_")) {
                    name = "Server " + name.split("CloudAPI:server_")[1];
                } else if (name.startsWith("CloudAPI")) {
                    name = "API (" + name.split(":")[1] + ")";
                } else if (name.startsWith("BungeeCord")) {
                    name = "BungeeCord";
                } else {
                    name = "Unbekannter Client";
                }

                return name;
            }
        }

        return "Unbekannter Client";
    }

    public Server server() {
        return server;
    }
}

package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.report.CreateReportPacket;
import de.paul2708.common.packet.report.ListReportPacket;
import de.paul2708.common.packet.report.RemoveReportPacket;
import de.paul2708.common.report.Report;

/**
 * Created by Paul on 07.10.2016.
 */
public class ReportPacketHandler implements PacketListener {

    @PacketHandler
    public void onCreate(Session session, CreateReportPacket packet) {
        // Create
        Report report = packet.getReport();
        CloudServer.getReportManager().report(report);

        CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(player -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                player.hasNotification()).forEach(player -> {
            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                    new ListReportPacket("", CloudServer.getReportManager().getOpenReports()));
        });
    }

    @PacketHandler
    public void onRemove(Session session, RemoveReportPacket packet) {
        // Remove
        Report report = packet.getReport();
        CloudServer.getReportManager().removeReport(report);

        CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(player -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel() &&
                player.hasNotification()).forEach(player -> {
            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                    new ListReportPacket("", CloudServer.getReportManager().getOpenReports()));
        });
    }
}

package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.command.CommandResolver;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.command.CommandPacket;
import de.paul2708.common.packet.other.ChatlogPacket;
import de.paul2708.common.packet.server.MotdPacket;
import de.paul2708.common.packet.survey.SurveyResultPacket;
import de.paul2708.common.survey.Result;

import java.util.UUID;

/**
 * Created by Paul on 07.10.2016.
 */
public class CommandPacketHandler implements PacketListener {

    @PacketHandler
    public void onCommand(Session session, CommandPacket packet) {
        // Basic command
        CommandResolver.resolve(UUID.fromString(packet.getName()), packet.getCommand(), packet.getArguments());
    }

    @PacketHandler
    public void onChatlog(Session session, ChatlogPacket packet) {
        // Chatlog
        CloudServer.getChatlogManager().push(packet.getChatlog());
    }

    @PacketHandler
    public void onMotd(Session session, MotdPacket packet) {
        // Motd
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new MotdPacket(CloudServer.getConfigFile().getMotd().replaceAll("&", "§")));
        CloudLogger.info("Motd: " + CloudServer.getConfigFile().getMotd().replaceAll("&", "§"));
    }

    @PacketHandler
    public void onSurveyResult(Session session, SurveyResultPacket packet) {
        // Result
        UUID uuid = packet.getUuid();
        Result result = packet.getResult();
        Player player = CloudServer.getPlayerManager().getPlayer(uuid);

        CloudServer.getSurveyManager().vote(player, result);

        CloudLogger.info("Survey: " + player.getName() + " hat abgestimmt");
    }
}

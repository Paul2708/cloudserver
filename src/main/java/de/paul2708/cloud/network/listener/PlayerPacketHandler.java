package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.packet.player.PlayerChangeServerPacket;
import de.paul2708.common.packet.player.PlayerLoginPacket;
import de.paul2708.common.packet.player.PlayerLogoutPacket;
import de.paul2708.common.packet.rank.GetPlayerRankPacket;
import de.paul2708.common.rank.Rank;

import java.util.UUID;

/**
 * Created by Paul on 07.10.2016.
 */
public class PlayerPacketHandler implements PacketListener {

    @PacketHandler
    public void onLogin(Session session, PlayerLoginPacket packet) {
        // Login
        CloudServer.getPlayerManager().loginPlayer(packet);
    }

    @PacketHandler
    public void onLogout(Session session, PlayerLogoutPacket packet) {
        // Logout
        CloudServer.getPlayerManager().logoutPlayer(packet);
    }

    @PacketHandler
    public void onRankRequest(Session session, GetPlayerRankPacket packet) {
        // Rank request
        if (packet.getUuid().contains("-")) {
            // UUID
            UUID uuid = UUID.fromString(packet.getUuid());
            Rank rank = CloudServer.getPlayerManager().getPlayer(uuid).getRank();
            session.send(new GetPlayerRankPacket(uuid.toString(), rank));
        } else {
            // Name
            String name = packet.getUuid();
            Rank rank = CloudServer.getPlayerManager().getPlayer(name).getRank();
            session.send(new GetPlayerRankPacket(name, rank));
        }
    }

    @PacketHandler
    public void onServerChange(Session session, PlayerChangeServerPacket packet) {
        // Server change
        Player player = CloudServer.getPlayerManager().getPlayer(UUID.fromString(packet.getUuid()));
        player.setServer(CloudServer.getServerManager().getServer(packet.getServerName()));
    }

}

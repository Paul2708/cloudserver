package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.server.ServerJoinPacket;
import de.paul2708.common.packet.server.ServerPlayerPacket;
import de.paul2708.common.packet.server.ServerStatePacket;

/**
 * Created by Paul on 08.01.2017.
 */
public class ServerPacketHandler implements PacketListener {

    @PacketHandler
    public void onJoin(Session session, ServerPlayerPacket packet) {
        // Player to server
        SpigotServer server = CloudServer.getServerManager().getServer(packet.getServer());
        if (server == null) {
            CloudLogger.error("Server: " + packet.getServer() + " konnte nicht gefunden werden", new IllegalArgumentException());
            return;
        }

        if (packet.getOperation().equalsIgnoreCase("join")) {
            server.addPlayer(packet.getUuid());
        } else {
            server.removePlayer(packet.getUuid());
        }
    }

    @PacketHandler
    public void onState(Session session, ServerStatePacket packet) {
        // Player count
        CloudServer.getServerManager().getServer(packet.getServer()).setGameState(GameState.getById(packet.getState()));
    }

    @PacketHandler
    public void onJoinAble(Session session, ServerJoinPacket packet) {
        // Join able
        CloudServer.getServerManager().getServer(packet.getServer()).setJoinAble(packet.canJoin());
    }
}

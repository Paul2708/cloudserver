package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.command.CommandResolver;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.common.command.CommandType;
import de.paul2708.common.game.GameState;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.packet.api.ApiInformationPacket;
import de.paul2708.common.packet.api.ApiUpdatePacket;
import de.paul2708.common.server.ServerData;
import de.paul2708.common.stats.StatisticData;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
public class ApiPacketHandler implements PacketListener {

    @PacketHandler
    public void onInformation(Session session, ApiInformationPacket packet) {
        // Request handle
        Information[] array = packet.getInformation();
        UUID uuid;
        SpigotServer server;
        Player player;

        for (Information information : array) {
            switch (information.getType()) {
                case PLAYER_RANK:
                    uuid = information.getArgument();
                    information.setResponse(CloudServer.getPlayerManager().getPlayer(uuid).getRank());
                    break;
                case TOKENS:
                    uuid = information.getArgument();
                    information.setResponse(CloudServer.getPlayerManager().getPlayer(uuid).getTokens());
                    break;
                case PLAY_TIME:
                    uuid = information.getArgument();
                    information.setResponse(CloudServer.getPlayerManager().getPlayer(uuid).getPlayTime());
                    break;
                case ONLINE_PLAYERS:
                    information.setResponse(CloudServer.getPlayerManager().getOnlinePlayers().size());
                    break;
                case STATISTIC_DATA:
                    StatisticData oldData = information.getArgument();
                    StatisticData data = CloudServer.getStatsManager().sendUserData(oldData);
                    information.setResponse(data);
                    break;
                case SERVER_DATA:
                    server = CloudServer.getServerManager().getServer(information.getArgument());
                    if (server == null) {
                        information.setResponse(new ServerData("-/-", -1, -1, "", GameState.NONE));
                    } else {
                        information.setResponse(server.getData());
                    }

                    break;
                case AVAILABLE_SERVER:
                    server = CloudServer.getServerManager().getAvailableServer(information.getArgument());

                    if (server == null) {
                        information.setResponse(new ServerData("-/-", -1, -1, "", GameState.NONE));
                    } else {
                        information.setResponse(server.getData());
                    }

                    break;
                case INGAME_SERVER:
                    List<SpigotServer> servers = new ArrayList<>();

                    for (SpigotServer all : CloudServer.getServerManager().getServers()) {
                        if (all.getName().startsWith(information.getArgument())) {
                            if (all.getGameState() != null && all.getGameState() == GameState.INGAME) {
                                servers.add(all);
                            }
                        }
                    }

                    ServerData[] ingameServer = new ServerData[servers.size()];
                    for (int i = 0; i < servers.size(); i++) {
                        ingameServer[i] = servers.get(i).getData();
                    }

                    information.setResponse(ingameServer);
                    break;
                case NICK:
                    uuid = information.getArgument();
                    player = CloudServer.getPlayerManager().getPlayer(uuid);

                    if (CloudServer.getNickManager().isNicked(player)) {
                        information.setResponse(CloudServer.getNickManager().getNick(player));
                    } else {
                        information.setResponse(new Nick("", null));
                    }

                    break;
            }
        }

        ApiInformationPacket informationPacket = new ApiInformationPacket(packet.getId(), packet.getReason(), array);
        session.send(informationPacket);
    }

    @PacketHandler
    public void onUpdate(Session session, ApiUpdatePacket packet) {
        // Update handle
        Update[] array = packet.getUpdates();
        UUID uuid;
        String name;

        for (Update update : array) {
            switch (update.getType()) {
                case TOKENS:
                    uuid = update.getArgument(0);
                    int value = update.getArgument(1);
                    CloudServer.getPlayerManager().getPlayer(uuid).updateTokens(value);
                    break;
                case COMMAND:
                    uuid = update.getArgument(0);
                    CommandType cmd = update.getArgument(1);
                    String totalArgs = update.getArgument(2);
                    CommandResolver.resolve(uuid, cmd.getCommand(), totalArgs.split(" "));
                    break;
                case SEND:
                    uuid = update.getArgument(0);
                    name = update.getArgument(1);
                    CloudServer.getPlayerManager().getPlayer(uuid).send(name);
                    break;
                case MOTD:
                    name = update.getArgument(0);
                    String motd = update.getArgument(1);

                    SpigotServer server = CloudServer.getServerManager().getServer(name);
                    if (server != null) {
                        server.setMotd(motd);
                    }
                    break;
            }
        }
    }
}

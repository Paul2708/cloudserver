package de.paul2708.cloud.network.listener;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.common.packet.stats.*;
import de.paul2708.common.stats.GameStatistic;

/**
 * Created by Paul on 16.10.2016.
 */
public class StatsPacketHandler implements PacketListener {

    @PacketHandler
    public void onRegister(Session session, StatsRegisterPacket packet) {
        // Register
        GameStatistic statistic = packet.getGameStatistic();
        CloudServer.getStatsManager().createTable(statistic.getName(), statistic.getCategories());
        CloudServer.getStatsManager().addGame(statistic);
    }

    @PacketHandler
    public void onUpdate(Session session, StatsUpdatePacket packet) {
        // Update
        CloudServer.getStatsManager().update(packet.getUuid(), packet.getGame(), packet.getKey(), packet.getValue());
    }

    @PacketHandler
    public void onPush(Session session, StatsPushPacket packet) {
        // Push
        CloudServer.getStatsManager().push(packet.getData());
    }

    @PacketHandler
    public void onCommand(Session session, StatsCommandPacket packet) {
        // Command
        CloudServer.getPlayerManager().getPlayer(packet.getUuid()).sendMessages(packet.getResult());
    }
}

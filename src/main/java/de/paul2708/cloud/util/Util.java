package de.paul2708.cloud.util;

import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.server.SpigotServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * Created by Paul on 20.08.2016.
 */
public class Util {

    public static Random random = new Random();

    public static Player getKickAblePlayer(SpigotServer server, int index, int j) {
        if (index == -1) {
            index = Util.random.nextInt(server.getPlayers().size());
        }
        if (j >= server.getOnlinePlayers()) {
            return null;
        }
        if (index == server.getOnlinePlayers()) {
            index = 0;
        }

        Player target = server.getPlayers().get(index);

        if (target.getRank().getPermissionLevel() < 20) {
            return target;
        } else {
            index++;
            j++;
            return getKickAblePlayer(server, index, j);
        }
    }

    public static boolean containsInt(int[] array, int input) {
        for (int number : array) {
            if (number == input) {
                return true;
            }
        }

        return false;
    }

    public static void copyFiles(Path source, Path destination) {
        try (Stream<Path> stream = Files.walk(source)) {
            stream.forEach(path -> {

                try {
                    Files.copy(path, Paths.get(path.toString().replace(
                            source.toString(),
                            destination.toString())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static List<Player> sort(List<Player> list) {
        List<Player> result = new ArrayList<>();

        Map<Player, Integer> map = new ConcurrentHashMap<>();
        List<Integer> level = new ArrayList<>();
        for (Player player : list) {
            map.put(player, player.getRank().getPermissionLevel());
            level.add(player.getRank().getPermissionLevel());
        }

        Collections.sort(level);
        Collections.reverse(level);

        for (Integer aLevel : level) {
            int current = aLevel;

            map.entrySet().stream().filter(entry -> entry.getValue() == current).forEach(entry -> {
                result.add(entry.getKey());
                map.remove(entry.getKey());
            });
        }

        return result;
    }

    private static DateFormat format;

    static {
        format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    }

    public static String getTime(long time) {
        return Util.format.format(new Date(time));
    }

    public static Map<Player, Long> bugDelay = new HashMap<>();

    public static Map<Player, Long> joinDelay = new HashMap<>();

    public static Map<Player, Player> msgData = new HashMap<>();

    public static final String[] ALPHABET = new String[] { "a", "b", "c", "d", "e", "f", "g",
            "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u",
            "v", "w", "x", "y", "z" };

    public static String removeExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index == -1) {
            return fileName;
        } else {
            return fileName.substring(0, index);
        }
    }

    // by Joseph Weissman
    public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    public static String getContext(Path path) {
        String text = "";
        try {
            for (String line : Files.readAllLines(path)) {
                text += line;
            }
        } catch (Exception e) {
            CloudLogger.error("Util: Fehler beim Bekommen des Texts von " + path.toString(), e);
        }

        return text;
    }

    public static double round(double value) {
        BigDecimal decimal = new BigDecimal(value);
        decimal = decimal.setScale(2, RoundingMode.HALF_UP);
        return decimal.doubleValue();
    }

    public static String generateId() {
        Random random = new Random();
        String[] array = new String[10];
        int index;

        for (int i = 0; i < 3; i++) {
            index = random.nextInt(10);

            if (array[index] == null) {
                array[index] = "" + random.nextInt(10);
            } else {
                while (array[index] != null) {
                    index = random.nextInt(10);

                    if (array[index] == null) {
                        break;
                    }
                }

                array[index] = "" + random.nextInt(10);
            }
        }
        for (int i = 0; i < 10; i++) {
            if (array[i] != null) {
                continue;
            }
            String letter = Util.ALPHABET[random.nextInt(Util.ALPHABET.length)];
            if (random.nextBoolean()) {
                letter = letter.toUpperCase();
            }

            array[i] = letter;
        }

        String key = "";
        for (int i = 0; i < array.length; i++) {
            key += array[i];
        }

        return key;
    }
}

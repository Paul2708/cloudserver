package de.paul2708.cloud;

import de.paul2708.cloud.abuse.AbuseManager;
import de.paul2708.cloud.achievement.AchievementManager;
import de.paul2708.cloud.automessage.AutoMessageManager;
import de.paul2708.cloud.blacklist.BlacklistManager;
import de.paul2708.cloud.chatlog.ChatlogManager;
import de.paul2708.cloud.file.*;
import de.paul2708.cloud.groupme.GroupMeManager;
import de.paul2708.cloud.key.KeyManager;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.maintenance.MaintenanceManager;
import de.paul2708.cloud.network.PacketServer;
import de.paul2708.cloud.nick.NickManager;
import de.paul2708.cloud.player.PlayerManager;
import de.paul2708.cloud.rank.RankManager;
import de.paul2708.cloud.report.ReportManager;
import de.paul2708.cloud.server.ServerManager;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.cloud.sql.MySQL;
import de.paul2708.cloud.stats.StatsManager;
import de.paul2708.cloud.survey.SurveyManager;
import de.paul2708.cloud.teamspeak.TeamspeakBot;
import de.paul2708.cloud.template.TemplateManager;
import de.paul2708.cloud.twitter.TwitterManager;
import de.paul2708.cloud.web.WebServer;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.client.CloudStatusPacket;
import lombok.Getter;

import java.util.Scanner;

/**
 * Created by Paul on 07.08.2016.
 */
public class CloudServer {

    public static final String TAG = "§cSystem§8│ §r";

    @Getter
    private static LogFile logFile;
    @Getter
    private static ConfigFile configFile;
    @Getter
    private static AddressesFile addressesFile;
    @Getter
    private static AutoMessageFile autoMessageFile;
    @Getter
    private static TeamspeakFile teamspeakFile;
    @Getter
    private static MaintenanceFile maintenanceFile;
    @Getter
    private static AccountFile accountFile;

    @Getter
    private static MySQL mySQL;

    @Getter
    private static WebServer webServer;
    @Getter
    private static RankManager rankManager;
    @Getter
    private static PlayerManager playerManager;
    @Getter
    private static StatsManager statsManager;
    @Getter
    private static AbuseManager abuseManager;
    @Getter
    private static KeyManager keyManager;
    @Getter
    private static NickManager nickManager;
    @Getter
    private static TwitterManager twitterManager;
    @Getter
    private static GroupMeManager groupMeManager;

    @Getter
    private static PacketServer packetServer;

    @Getter
    private static ServerManager serverManager;
    @Getter
    private static TemplateManager templateManager;
    @Getter
    private static ChatlogManager chatlogManager;
    @Getter
    private static ReportManager reportManager;
    @Getter
    private static AutoMessageManager autoMessageManager;
    @Getter
    private static BlacklistManager blacklistManager;
    @Getter
    private static MaintenanceManager maintenanceManager;
    @Getter
    private static SurveyManager surveyManager;
    @Getter
    private static AchievementManager achievementManager;

    @Getter
    private static TeamspeakBot teamspeakBot;

    @Getter
    private static boolean started = false;

    @Getter
    private static long startTime;

    public static void startupCloud() {
        // Logger file
        CloudServer.logFile = new LogFile(System.currentTimeMillis());
        CloudServer.logFile.log("LogFile erstellt");

        // Config file
        CloudServer.configFile = new ConfigFile();
        // Adresses file
        CloudServer.addressesFile = new AddressesFile();
        // Auto message file
        CloudServer.autoMessageFile = new AutoMessageFile();
        // Teamspeak file
        CloudServer.teamspeakFile = new TeamspeakFile();
        // Maintenance file
        CloudServer.maintenanceFile = new MaintenanceFile();
        // Account file
        CloudServer.accountFile = new AccountFile();

        // Start logger
        CloudLogger.initialize();
        CloudLogger.info("Starte Cloud-Server...");

        // MySQL connection
        CloudServer.mySQL = new MySQL(CloudServer.addressesFile.getIp(),
                "cloudserver", CloudServer.addressesFile.getUser(), CloudServer.addressesFile.getPassword());
        CloudServer.mySQL.openConnection();
        DatabaseManager.setupTables();

        // Web server
        CloudServer.webServer = new WebServer();

        // Rank manager
        CloudServer.rankManager = new RankManager();
        CloudServer.rankManager.resolveRanks();
        CloudServer.rankManager.startRankRemover();

        // Player manager
        CloudServer.playerManager = new PlayerManager();
        CloudServer.playerManager.resolvePlayers();
        CloudServer.playerManager.startPlayTimeObserver();

        // Stats manager
        CloudServer.statsManager = new StatsManager();

        // Abuse manager
        CloudServer.abuseManager = new AbuseManager();
        CloudServer.abuseManager.resolveAbuses();
        CloudServer.abuseManager.startAbuseObserver();

        // Key manager
        CloudServer.keyManager = new KeyManager();
        CloudServer.keyManager.resolveKeys();
        CloudServer.keyManager.startKeyObserver();

        // Nick manager
        CloudServer.nickManager = new NickManager();
        CloudServer.nickManager.resolveNicks();
        CloudServer.nickManager.resolveSkins();

        // Twitter manager
        CloudServer.twitterManager = new TwitterManager();
        CloudServer.twitterManager.login();

        // GroupMe manager
        CloudServer.groupMeManager = new GroupMeManager();
        CloudServer.groupMeManager.login();

        // Packet server
        CloudServer.packetServer = new PacketServer();
        CloudServer.packetServer.start();

        // Server manager
        CloudServer.serverManager = new ServerManager();
        CloudServer.serverManager.startRestartObserver();

        // Template manager
        CloudServer.templateManager = new TemplateManager();
        CloudServer.templateManager.resolveTemplates();

        // Chatlog manager
        CloudServer.chatlogManager = new ChatlogManager();
        CloudServer.chatlogManager.createFileAndRead();

        // Report manager
        CloudServer.reportManager = new ReportManager();
        CloudServer.reportManager.startReportBroadcast();

        // Auto messge manager
        CloudServer.autoMessageManager = new AutoMessageManager();
        CloudServer.autoMessageManager.start();

        // Blacklist manager
        CloudServer.blacklistManager = new BlacklistManager();
        CloudServer.blacklistManager.resolveNames();

        // Maintenance manager
        CloudServer.maintenanceManager = new MaintenanceManager();

        // Survey manager
        CloudServer.surveyManager = new SurveyManager();
        CloudServer.surveyManager.resolveSurvey();

        // Achievement manager
        CloudServer.achievementManager = new AchievementManager();

        // Teamspeak bot
        if (CloudServer.teamspeakFile.isEnabled()) {
            CloudServer.teamspeakBot = new TeamspeakBot();
            CloudServer.teamspeakBot.connect();
        }

        // Started
        CloudServer.started = true;
        CloudServer.packetServer.send(ClientType.BUNGEE, new CloudStatusPacket("started"));
        CloudServer.startTime = System.currentTimeMillis();

        CloudLogger.info("CloudServer erfolgreich gestartet!");

        Scanner scanner = new Scanner(System.in);
        String line;

        while (true) {
            line = scanner.nextLine();

            if (line.contains("exit")) {
                System.exit(0);
            }
        }
    }
}

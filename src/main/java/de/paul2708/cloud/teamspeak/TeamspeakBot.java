package de.paul2708.cloud.teamspeak;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClient;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by Paul on 04.09.2016.
 */
public class TeamspeakBot {

    private TeamspeakBot instance;

    @Getter
    private TS3Query query;

    @Getter
    private TS3Api api;

    @Getter
    private LinkManager linkManager;

    public void connect() {
        this.instance = this;

        TS3Config config = new TS3Config();
        config.setHost(CloudServer.getTeamspeakFile().getIp());
        config.setQueryPort(CloudServer.getTeamspeakFile().getPort());
        config.setDebugLevel(Level.OFF);
        config.setFloodRate(TS3Query.FloodRate.UNLIMITED);

        config.setConnectionHandler(new ConnectionHandler() {

            @Override
            public void onConnect(TS3Query ts3Query) {
                api = query.getApi();
                api.login(CloudServer.getTeamspeakFile().getUserName(), CloudServer.getTeamspeakFile().getPassword());
                api.selectVirtualServerById(1);
                api.selectVirtualServerByPort(CloudServer.getTeamspeakFile().getVirtualPort());
                api.setNickname("Cloud-Bot");

                api.registerAllEvents();
                api.addTS3Listeners(linkManager = new LinkManager(instance));
            }

            @Override
            public void onDisconnect(TS3Query ts3Query) {
                
            }
        });

        config.setReconnectStrategy(ReconnectStrategy.exponentialBackoff());

        query = new TS3Query(config);
        query.connect();

        api = query.getApi();
        api.login(CloudServer.getTeamspeakFile().getUserName(), CloudServer.getTeamspeakFile().getPassword());
        api.selectVirtualServerById(1);
        api.selectVirtualServerByPort(CloudServer.getTeamspeakFile().getVirtualPort());
        api.setNickname("Cloud-Bot");

        api.registerAllEvents();
        api.addTS3Listeners(linkManager = new LinkManager(this));

        CloudLogger.info("TS3U: Bot hat Server betreten");

        for (Client client : api.getClients()) {
            if (client.getNickname().equalsIgnoreCase("Cloud-Bot")) continue;

            linkManager.request(client);
        }
    }

    public void write(Client client, String message) {
        api.sendPrivateMessage(client.getId(), message);
    }

    public void write(int clientId, String message) {
        api.sendPrivateMessage(clientId, message);
    }

    public boolean haveSameIP(Player player, Client client) {
        return player.getIp().equalsIgnoreCase(client.getIp());
    }

    public boolean isOnline(String uid) {
        for (Client client : api.getClients()) {
            if (client.getUniqueIdentifier().equalsIgnoreCase(uid)) {
                return true;
            }
        }

        return false;
    }

    public String getName(String uid) {
        for (Client client : api.getClients()) {
            if (client.getUniqueIdentifier().equalsIgnoreCase(uid)) {
                return client.getNickname();
            }
        }

        return "-/-";
    }

    public void setServerGroup(DatabaseClient client, int id) {
        final List<ServerGroup> oldGroups = api.getServerGroupsByClientId(client.getDatabaseId());
        final int[] array = CloudServer.getTeamspeakFile().getDeletingIds();
        for (ServerGroup old : oldGroups) {
            if (Util.containsInt(array, old.getId())) continue;

            api.removeClientFromServerGroup(old.getId(), client.getDatabaseId());
        }

        api.addClientToServerGroup(id, client.getDatabaseId());
    }

    public boolean hasServerGroup(Client client, int id) {
        for (int group : client.getServerGroups()) {
            if (group == id) {
                return true;
            }
        }

        return false;
    }

    public void changeDescription(Client client, String desc) {
        Map<ClientProperty, String> property = new HashMap<>();
        property.put(ClientProperty.CLIENT_DESCRIPTION, desc);

        api.editClient(client.getId(), property);
    }

    public void editSupportChannel(boolean open) {
        ChannelInfo channel = api.getChannelInfo(CloudServer.getTeamspeakFile().getSupportChannelId());
        Map<ChannelProperty, String> properties = new HashMap<>();
        if (open) {
            properties.put(ChannelProperty.CHANNEL_NAME, "┏ Support ▪ Geöffnet");
            properties.put(ChannelProperty.CHANNEL_DESCRIPTION, "Der Support ist geöffnet.");
            api.addChannelPermission(channel.getId(), "i_channel_needed_join_power",
                    CloudServer.getTeamspeakFile().getSupportChannelJoinPower(true));
        } else {
            api.getClients().stream().filter(client -> client.getChannelId() == channel.getId()).forEach(client -> api.kickClientFromChannel("Der Support wurde geschlossen.", client.getId()));

            properties.put(ChannelProperty.CHANNEL_NAME, "┏ Support ▪ Geschlossen");
            properties.put(ChannelProperty.CHANNEL_DESCRIPTION, "Der Support ist geschlossen.");
            api.addChannelPermission(channel.getId(), "i_channel_needed_join_power",
                    CloudServer.getTeamspeakFile().getSupportChannelJoinPower(false));
        }

        api.editChannel(channel.getId(), properties);
    }

    public void disconnect() {
        api.logout();
        query.exit();

        CloudLogger.info("TS3: Bot hat Server verlassen");
    }
}

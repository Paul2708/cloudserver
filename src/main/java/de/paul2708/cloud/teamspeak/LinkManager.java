package de.paul2708.cloud.teamspeak;

import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 04.09.2016.
 */
public class LinkManager implements TS3Listener {

    private TeamspeakBot bot;

    private List<String> requested;

    public LinkManager(TeamspeakBot bot) {
        this.bot = bot;

        this.requested = new ArrayList<>();
    }

    public void request(Client client) {
        Player player = CloudServer.getPlayerManager().getPlayer(client);
        if (player != null) {
            bot.changeDescription(client, "Minecraft-Acc.: " + player.getName());

            if (bot.hasServerGroup(client, player.getRank().getTeamspeakGroudId())) {
                return;
            }

            bot.setServerGroup(bot.getApi().getDatabaseClientByUId(client.getUniqueIdentifier()), player.getRank().getTeamspeakGroudId());
            return;
        }
        if (!bot.hasServerGroup(client, CloudServer.getTeamspeakFile().getDefaultGroupId())) {
            return;
        }

        bot.changeDescription(client, "nicht verlinkt");
        requested.add(client.getUniqueIdentifier());
        bot.write(client, "Hallo [B]" + client.getNickname() + "[/B]. Du willst deinen Client mit deinem Minecraft-Account verlinken? " +
                "Dann schreibe mich mit deinem [B]Ingame-Namen[/B] an.");
    }

    @Override
    public void onTextMessage(TextMessageEvent e) {
        if (e.getTargetMode() == TextMessageTargetMode.CLIENT && e.getInvokerId() != bot.getApi().whoAmI().getId()) {
            String message = e.getMessage();
            Client client = bot.getApi().getClientByUId(e.getInvokerUniqueId());

            // Check valid arguments
            if (!requested.contains(client.getUniqueIdentifier())) {
                return;
            }

            Player target = CloudServer.getPlayerManager().getPlayer(message);
            if (target == null || !target.isOnline()) {
                bot.write(client,
                        "Du bist nicht online. Bitte joine auf ozeangames.net und schreibe nochmal deinen Namen.");
                return;
            }
            if (!bot.haveSameIP(target, client)) {
                bot.write(client,
                        "Die IP stimmt nicht mit der verbundenen IP auf dem Netzwerk überein.");
                return;
            }

            // Link player
            bot.changeDescription(client, "Minecraft-Acc.: " + target.getName());
            target.setTeamSpeakUID(e.getInvokerUniqueId());
            bot.setServerGroup(bot.getApi().getDatabaseClientByUId(client.getUniqueIdentifier()), target.getRank().getTeamspeakGroudId());

            requested.remove(client.getUniqueIdentifier());
            bot.getApi().pokeClient(client.getId(), "Du hast dich erfolgreich mit [B]" + target.getName() + "[/B] verlinkt.");
            target.sendMessage(CloudServer.TAG + "§7Du hast dich mit §e" + e.getInvokerName() + " §7verlinkt.");

            CloudLogger.info("TS3: " + client.getNickname() + " hat sich mit " + target.getName() + " verlinkt");
        }
    }

    @Override
    public void onClientJoin(ClientJoinEvent e) {
        Client client = bot.getApi().getClientByUId(e.getUniqueClientIdentifier());
        request(client);
    }

    @Override
    public void onClientLeave(ClientLeaveEvent e) {
        Client client = bot.getApi().getClientByUId(e.getInvokerUniqueId());

        if (requested.contains(client.getUniqueIdentifier())) {
            requested.remove(client.getUniqueIdentifier());
        }
    }

    @Override
    public void onServerEdit(ServerEditedEvent e) {

    }

    @Override
    public void onChannelEdit(ChannelEditedEvent e) {

    }

    @Override
    public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {

    }

    @Override
    public void onClientMoved(ClientMovedEvent e) {
        if (e.getTargetChannelId() == CloudServer.getTeamspeakFile().getSupportChannelId()) {
            Player target = CloudServer.getPlayerManager().getPlayer(bot.getApi().getClientInfo(e.getClientId()));
            final String name = (target == null ? "§e" :
                    target.getRank().getPrefix()) +
                    bot.getApi().getClientInfo(e.getClientId()).getNickname() + "§7(" +
                    target.getRank().getPrefix() + target.getName() + "§7)";
            CloudServer.getPlayerManager().getOnlinePlayers().stream().filter(player -> player.getRank().getPermissionLevel() >= CloudServer.getConfigFile().getTeamPermissionLevel()).filter(player -> player.hasNotification()).forEach(player -> {
                player.sendMessage(CloudServer.TAG +
                        name + " §7hat den TS-Warteraum betreten.");
                player.playSound("level_up");
            });

            bot.write(e.getClientId(),
                    "Ein Supporter wird sich gleich um dich kümmern. Bitte warte bis du gemoved wirst. Danke :)");
        }
    }

    @Override
    public void onChannelCreate(ChannelCreateEvent e) {

    }

    @Override
    public void onChannelDeleted(ChannelDeletedEvent e) {

    }

    @Override
    public void onChannelMoved(ChannelMovedEvent e) {

    }

    @Override
    public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {

    }

    @Override
    public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent e) {

    }
}

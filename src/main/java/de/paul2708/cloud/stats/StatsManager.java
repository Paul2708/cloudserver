package de.paul2708.cloud.stats;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.common.stats.Category;
import de.paul2708.common.stats.GameStatistic;
import de.paul2708.common.stats.StatisticData;
import de.paul2708.common.util.Util;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 16.10.2016.
 */
public class StatsManager {

    private Map<UUID, List<StatisticData>> statistics = new ConcurrentHashMap<>();
    private List<GameStatistic> games = new CopyOnWriteArrayList<>();

    public void createTable(String name, List<Category> categories) {
        String column = "";

        for (Category category : categories) {
            column += "`" + category.getDataName() + "` " + category.getType().getName() + "("
                + category.getSize() + ") NOT NULL DEFAULT '" + category.getDefaultValue() + "',";
        }

        String query = "CREATE TABLE IF NOT EXISTS `" + name.toLowerCase() + "_stats` ("
                + "`name` varchar(200) NOT NULL,"
                + "`uuid` varchar(200) NOT NULL,"
                + column
                + "UNIQUE KEY `uuid` (`uuid`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        CloudServer.getMySQL().queryUpdate(query);
    }

    public void update(UUID uuid, String game, String key, Object value) {
        List<StatisticData> datas = statistics.get(uuid);
        StatisticData statisticData = getStatisticData(uuid, game);
        if (statisticData == null) {
            StatisticData data = new StatisticData(uuid, game);
            for (Category category : getCategories(game)) {
                data.set(category.getName(), Util.getValueByString(category, category.getDefaultValue()));
            }

            data.set(key, value);
            datas.add(data);
        } else {
            datas.remove(statisticData);
            statisticData.set(key, value);
            datas.add(statisticData);
        }
    }

    public void push(StatisticData data) {
        List<String>[] sorted = sorted(data);
        String column = "";
        String values = "";

        for (List<String> aSorted : sorted) {
            column += "`" + aSorted.get(0) + "`, ";
            values += "'" + aSorted.get(1) + "', ";
        }

        column = column.substring(0, column.length() - 2);
        values = values.substring(0, values.length() - 2);

        String query = "INSERT INTO `" + data.getGame().toLowerCase() + "_stats` (`name`, `uuid`, " + column + ") " +
                "VALUES (" +
                "'" + CloudServer.getPlayerManager().getPlayer(data.getUuid()).getName() + "', " +
                "'" + data.getUuid() + "', " + values + ") " +
                "ON DUPLICATE KEY " +
                "UPDATE " + updateValues(sorted);

        CloudServer.getMySQL().queryUpdate(query);
    }

    public void addGame(GameStatistic statistic) {
        if (!containsGame(statistic)) {
            games.add(statistic);

            CloudLogger.info("Stats: " + statistic.getName() + " wurden registriert");
        }
    }

    public StatisticData sendUserData(StatisticData statisticData) {
        StatisticData gameData = getStatisticData(statisticData.getUuid(), statisticData.getGame());
        StatisticData data;
        if (gameData == null) {
            data = resolve(statisticData.getUuid(), statisticData.getGame());
        } else {
            data = new StatisticData(statisticData.getUuid(), statisticData.getGame());
            data.setStats(gameData.getStats());

            for (Map.Entry<Category, Object> entry : gameData.getStats().entrySet()) {
                data.set(entry.getKey().getName(), entry.getValue());
            }
        }

        return data;
    }

    public StatisticData resolve(UUID uuid, String game) {
        statistics.putIfAbsent(uuid, new ArrayList<>());

        StatisticData data = new StatisticData(uuid, game);

        Map<Category, Object> defaultStats = new HashMap<>();
        for (Category category : getCategories(game)) {
            defaultStats.put(category, Util.getValueByString(category, category.getDefaultValue()));
        }
        data.setStats(defaultStats);

        for (Map.Entry<String, Object> entry : resolveData(game, uuid).entrySet()) {
            data.set(entry.getKey(), entry.getValue());
        }
        List<StatisticData> datas = statistics.get(uuid);
        datas.add(data);
        statistics.put(uuid, datas);

        return data;
    }

    public StatisticData getStatisticData(UUID uuid, String game) {
        if (statistics.get(uuid) == null) {
            return null;
        }

        for (StatisticData statisticData : statistics.get(uuid)) {
            if (statisticData.getGame().equalsIgnoreCase(game)) {
                return statisticData;
            }
        }

        return null;
    }

    public GameStatistic getGameStatistic(String server) {
        for (GameStatistic statistic : games) {
            if (server.startsWith(statistic.getServerTag())) {
                return statistic;
            }
        }

        return null;
    }

    // Server has stats
    public boolean isGameServer(String serverName) {
        for (GameStatistic game : games) {
            if (serverName.startsWith(game.getServerTag())) {
                return true;
            }
        }

        return false;
    }

    // Categories from game stats
    private List<Category> getCategories(String game) {
        for (GameStatistic statistic : games) {
            if (statistic.getName().equalsIgnoreCase(game)) {
                return statistic.getCategories();
            }
        }

        return null;
    }

    // Game stats in list
    private boolean containsGame(GameStatistic gameStatistic) {
        for (GameStatistic game : games) {
            if (game.getName().equalsIgnoreCase(gameStatistic.getName()) &&
                    game.getServerTag().equalsIgnoreCase(game.getServerTag())) {
                return true;
            }
        }

        return false;
    }

    private List<String>[] sorted(StatisticData data) {
        List<String>[] array = new List[data.getStats().size()];
        int i = 0;
        for (Map.Entry<Category, Object> entry : data.getStats().entrySet()) {
            List<String> list = new ArrayList<>();
            list.add(entry.getKey().getDataName());
            list.add(entry.getValue().toString());

            array[i] = list;
            i++;
        }

        return array;
    }

    private String updateValues(List<String>[] array) {
        String update = "";
        for (List<String> anArray : array) {
            update += "`" + anArray.get(0) + "` = '" + anArray.get(1) + "', ";
        }
        update = update.substring(0, update.length() - 2);

        return update;
    }

    private Map<String, Object> resolveData(String game, UUID uuid) {
        Map<String, Object> values = new HashMap<>();
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `" + game.toLowerCase() + "_stats` WHERE `uuid` LIKE '" + uuid + "' LIMIT 1");

        try {
            while (rs.next()) {
                for (Category category : getCategories(game)) {
                    if (category.getType() == Category.Type.INTEGER) {
                        values.put(category.getName(), rs.getInt(category.getDataName()));
                    } else if (category.getType() == Category.Type.VARCHAR) {
                        values.put(category.getName(), rs.getString(category.getDataName()));
                    } else if (category.getType() == Category.Type.LONG) {
                        values.put(category.getName(), rs.getLong(category.getDataName()));
                    }
                }
            }
        } catch (Exception e) {
            CloudLogger.error("Stats: Fehler beim Bekommen für " + uuid.toString() + " (" + game + ")", e);
        }

        return values;
    }
}

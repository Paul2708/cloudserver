package de.paul2708.cloud.chatlog;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.chatlog.Chatlog;
import lombok.Getter;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 03.09.2016.
 */
public class ChatlogManager {

    // TODO: log more actions

    private Map<String, Long> delay = new HashMap<>();

    @Getter
    private List<Chatlog> chatlogs = new CopyOnWriteArrayList<>();

    private List<String> ids = new CopyOnWriteArrayList<>();

    public void report(Player player) {
        delay.put(player.getName(), System.currentTimeMillis());
    }

    public void push(Chatlog chatlog) {
        chatlogs.add(chatlog);

        // Generate id
        String id = generateId();

        // Main object
        JsonObject object = new JsonObject();

        // Data
        JsonObject info = new JsonObject();
        info.put("id", id);
        info.put("time", Util.getTime(chatlog.getTime()));

        JsonObject abuserData = new JsonObject();
        abuserData.put("name", chatlog.getTargetName());
        abuserData.put("uuid", chatlog.getTargetUuid());

        JsonObject reporterData = new JsonObject();
        reporterData.put("name", chatlog.getPlayerName());
        reporterData.put("uuid", chatlog.getPlayerUuid());

        JsonArray array = new JsonArray();

        for (String log : chatlog.getChatLog()) {
            array.add(resolve(log));
        }

        // Put into main object
        object.put("info", info);
        object.put("abuser", abuserData);
        object.put("reporter", reporterData);
        object.put("chatlog", array);

        // Write to file
        try {
            String path = CloudServer.getConfigFile().getPathToCloud() + "chatlogs/" + id + ".json";

            Files.createFile(Paths.get(path));
            FileWriter writer = new FileWriter(path);
            writer.write(Jsoner.prettyPrint(object.toJson()));
            writer.flush();

            this.ids.add(id);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String url = "ozeangames.net/chatlog.php?id=" + id;

        Player request = CloudServer.getPlayerManager().getPlayer(chatlog.getPlayerName());

        request.sendMessage(CloudServer.TAG + "§7Du kannst den Chatlog unter §e" + url + " §7einsehen.");

        CloudLogger.info("Chatlog: neuer Chatlog mit id=" + id + " wurde erstellt");
    }

    public void createFileAndRead() {
        try {
            if (!Files.exists(Paths.get("chatlogs"))) {
                Files.createDirectories(Paths.get("chatlogs"));
            } else {
                ids.addAll(resolveIds());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean canReport(Player player) {
        if (delay.get(player.getName()) == null) {
            return true;
        } else {
            long timeStamp = delay.get(player.getName());
            return timeStamp + (1000 * 60) <= System.currentTimeMillis();
        }
    }

    private JsonObject resolve(String input) {
        JsonObject object = new JsonObject();
        String[] array = input.split(":");

        object.put("action", array[0]);
        object.put("message", array[1]);
        object.put("time", Util.getTime(Long.valueOf(array[2])));

        return object;
    }

    public String generateId() {
        Random random = new Random();
        String id = "";

        for (int i = 0; i < 5; i++) {
            if (random.nextBoolean()) {
                id += random.nextInt(10);
            } else {
                String letter = Util.ALPHABET[random.nextInt(Util.ALPHABET.length)];
                if (random.nextBoolean()) {
                    letter = letter.toUpperCase();
                }

                id += letter;
            }
        }

        for (String all : ids) {
            if (all.equalsIgnoreCase(id)) {
                return generateId();
            }
        }

        return id;
    }

    private List<String> resolveIds() {
        List<String> names = new ArrayList<>();
        Path path = Paths.get("chatlogs");

        try {
            Files.list(path).forEach(p -> names.add(Util.removeExtension(p.toFile().getName())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }
}

package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 20.08.2016.
 */
public class ConfigFile extends BasicFile {

    public ConfigFile() {
        super("config.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("default cloud config");

        set("default_rank", "Spieler");
        set("admin_permission_level", 120);
        set("team_permission_level", 90);
        set("max_players", 300);
        set("motd", "&aThis ist the first line" + "\\n" + "&cThis is the second. &klel");
        set("lobby-tag", "lobby");
        set("build_event_server", "none");
        set("build_server", "bauserver");
        set("available_ports", "30000-30100");
        set("path_to_cloud", "/home/pi/Cloud/");

        save();
    }

    public String getDefaultRank() {
        return get("default_rank");
    }

    public int getAdminPermissionLevel() {
        return get("admin_permission_level");
    }

    public int getTeamPermissionLevel() {
        return get("team_permission_level");
    }

    public int getMaxPlayers() {
        return get("max_players");
    }

    public String getMotd() {
        return get("motd");
    }

    public String getLobbyTag() {
        return get("lobby-tag");
    }

    public String getBuildEventServer() {
        return get("build_event_server");
    }

    public String getBuildServer() {
        return get("build_server");
    }

    public int getMinPort() {
        return Integer.valueOf(((String) get("available_ports")).split("-")[0]);
    }

    public int getMaxPort() {
        return Integer.valueOf(((String) get("available_ports")).split("-")[1]);
    }

    public String getPathToCloud() {
        return get("path_to_cloud");
    }
}

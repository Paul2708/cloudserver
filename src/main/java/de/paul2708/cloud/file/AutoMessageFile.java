package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Paul on 02.09.2016.
 */
public class AutoMessageFile extends BasicFile {

    public AutoMessageFile() {
        super("messages.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for auto messages broadcasted by cloud system");

        set("enable", false);
        set("messages", Arrays.asList("&cIch werde alle &a10 Minuten &cgebroadcastet.",
                "&bTwitter: https://twitter.com/theplayerpaul",
                "&cMit /loadmessages kann man die Nachrichten neu laden. 2 Minuten"));
        set("delay", 5);

        save();
    }

    public boolean isEnabled() {
        return get("enable");
    }

    public List<String> getMessages() {
        return get("messages");
    }

    public int getDelay() {
        return get("delay");
    }

}

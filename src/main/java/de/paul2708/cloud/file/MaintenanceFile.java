package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 06.09.2016.
 */
public class MaintenanceFile extends BasicFile {

    public MaintenanceFile() {
        super("maintenance.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for maintenance");

        set("enabled", false);
        set("reason", "none");

        save();
    }

    public boolean isEnabled() {
        return get("enabled");
    }

    public String getReason() {
        return get("reason");
    }
}

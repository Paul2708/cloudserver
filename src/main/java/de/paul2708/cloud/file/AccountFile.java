package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 02.01.2017.
 */
public class AccountFile extends BasicFile {

    // TODO: add keys and tokens

    public AccountFile() {
        super("account.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for accounts used by cloud system");

        set("twitter_oauthconsumerkey", "");
        set("twitter_oauthconsumersecret", "");
        set("twitter_oauthaccesstoken", "");
        set("twitter_oauthaccesstokensecret", "");

        set("groupme_token", "");

        save();
    }

    public String getTwitterConsumerKey() {
        return get("twitter_oauthconsumerkey");
    }

    public String getTwitterConsumerSecret() {
        return get("twitter_oauthconsumersecret");
    }

    public String getTwitterAccessToken() {
        return get("twitter_oauthaccesstoken");
    }

    public String getTwitterAccessTokenSecret() {
        return get("twitter_oauthaccesstokensecret");
    }

    public String getGroupMeToken() {
        return get("groupme_token");
    }
}

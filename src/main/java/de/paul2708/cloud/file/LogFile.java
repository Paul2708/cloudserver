package de.paul2708.cloud.file;

import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.util.Util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 19.08.2016.
 */
public class LogFile {

    private String pathName;

    private Path path;

    private List<String> lines;

    public LogFile(long time) {
        this.pathName = "logs/" + Util.getTime(time).replaceAll(":", "-") + ".txt";
        this.lines = new CopyOnWriteArrayList<>();
        load();
    }

    public String getPath() {
        return pathName;
    }

    private void load() {
        this.path = Paths.get(this.pathName);
        File file = path.toFile();
        if(file == null || !file.exists()) {
            try {
                Files.createDirectories(path.getParent());
                Files.createFile(path);
            } catch (IOException e) {
                System.out.println("Datei '" + getFileName() + "' konnte nicht gespeichert werden: " + e.getMessage());
                System.out.println("Grund: " + e.getLocalizedMessage());
            }
        }
    }

    public void log(String message) {
        lines.add(message);
        save();
    }

    private void save() {
        String text = "";
        for (String line : lines) {
            if (line.endsWith("\n")) {
                text = text + line;
            } else {
                text = text + line + "\n";
            }
        }

        try {
            Files.write(path, text.getBytes("utf-8"));
        } catch (IOException e) {
            CloudLogger.error("File: Datei '" + getFileName() + "' konnte nicht beschrieben werden", e);
        }
    }

    private String getFileName() {
        String[] array = this.pathName.split("/");
        return array[array.length - 1];
    }
}

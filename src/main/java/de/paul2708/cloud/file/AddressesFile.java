package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 14.10.2016.
 */
public class AddressesFile extends BasicFile {

    public AddressesFile() {
        super("addresses.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for different ips and keys");

        set("mysql_ip", "localhost");
        set("mysql_user", "root");
        set("mysql_password", "");
        set("packet_ip", "localhost");
        set("packet_port", 2000);

        save();
    }

    public String getIp() {
        return get("mysql_ip");
    }

    public String getUser() {
        return get("mysql_user");
    }

    public String getPassword() {
        return get("mysql_password");
    }

    public String getPacketIp() {
        return get("packet_ip");
    }

    public int getPort() {
        return get("packet_port");
    }
}

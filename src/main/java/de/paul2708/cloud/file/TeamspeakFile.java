package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 05.09.2016.
 */
public class TeamspeakFile extends BasicFile {

    // TODO: Add pw

    public TeamspeakFile() {
        super("teamspeak.yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for teamspeak settings");

        set("enable", false);
        set("ip", "localhost");
        set("query_port", 10011);
        set("username", "serveradmin");
        set("password", "");
        set("virtual_server_port", 1649);
        set("default_group_id", 11);
        set("non_deleting_group_ids", "6,6");
        set("support_queue_channel_id", 4);
        set("open_support_channel_join_power", 0);
        set("closed_support_channel_join_power", 10000);

        save();
    }

    public boolean isEnabled() {
        return get("enable");
    }

    public String getIp() {
        return get("ip");
    }

    public int getPort() {
        return get("query_port");
    }

    public String getUserName() {
        return get("username");
    }

    public String getPassword() {
        return get("password");
    }

    public int getVirtualPort() {
        return get("virtual_server_port");
    }

    public int getDefaultGroupId() {
        return get("default_group_id");
    }

    public int[] getDeletingIds() {
        String list = get("non_deleting_group_ids");
        if (list.equalsIgnoreCase("none")) {
            return new int[] { };
        }

        if (!list.contains(",")) {
            return new int[] { Integer.valueOf(list) };
        } else {
            String[] listArray = list.split(",");
            int[] array = new int[listArray.length];

            for (int i = 0; i < listArray.length; i++) {
                array[i] = Integer.valueOf(listArray[i]);
            }

            return array;
        }
    }

    public int getSupportChannelId() {
        return get("support_queue_channel_id");
    }

    public int getSupportChannelJoinPower(boolean open) {
        if (open) {
            return get("open_support_channel_join_power");
        } else {
            return get("closed_support_channel_join_power");
        }
    }
}

package de.paul2708.cloud.file;

import de.paul2708.common.file.BasicFile;

/**
 * Created by Paul on 20.08.2016.
 */
public class TemplateFile extends BasicFile {

    public TemplateFile(String name) {
        super("templates/" + name + ".yml");
    }

    @Override
    public void createDefaultValue() {
        comment("config for multiple templates");

        set("name", "Example");
        set("ram", 520);
        set("min_server", 1);
        set("max_server", 3);

        save();
    }

    public String getName() {
        return get("name");
    }

    public int getRAM() {
        return get("ram");
    }

    public int getMinServer() {
        return get("min_server");
    }

    public int getMaxServer() {
        return get("max_server");
    }
}

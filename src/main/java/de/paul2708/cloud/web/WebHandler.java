package de.paul2708.cloud.web;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Paul on 28.06.2017.
 */
public class WebHandler extends AbstractHandler {

    // localhost:1337?log=ID

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        try {
            if(request.getParameter("log") != null) {
                String id = request.getParameter("log");

                String log = "<html><head><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">";
                log += "<style>@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,600'); body {font-family: 'Open Sans'>}</style></head><body><div class='container-fluid' style='padding-top:15px'><pre><code>";

                String content = CloudServer.getWebServer().getLog(id);

                if (content == null) {
                    log += "Die ID konnte nicht gefunden werden.";
                } else {
                    log += content;
                }

                log += "</code><br><hr>" +
                        "<p>Hilfe von L_Leon (<a href='https://twitter.com/L_LeonDE' target='_blank'>@L_Leon</a>) in Gedenken an sbin.tk</p></pre></div></body></html>";

                httpServletResponse.getWriter().println(log);
            }

            httpServletResponse.setContentType("text/html;charset=utf-8");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            request.setHandled(true);
        } catch (IOException e) {
            CloudLogger.error("WebServer: Fehler beim Abrufen des Handlers", e);
        }
    }
}

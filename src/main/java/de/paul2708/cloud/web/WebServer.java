package de.paul2708.cloud.web;

import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.util.Util;
import org.eclipse.jetty.server.Server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 28.06.2017.
 */
public class WebServer {

    private Server server;

    private Map<String, String> cachedLogs = new ConcurrentHashMap<>();

    public WebServer() {
        new Thread(() -> {

            this.server = new Server(1337);
            server.setHandler(new WebHandler());

            try {
                server.start();
                server.join();
            } catch (Exception e) {
                CloudLogger.error("WebServer: Fehler beim Starten", e);
            }
        }).start();
    }

    public String addToCache(String log) {
        String id = Util.generateId();

        if (isUsed(id)) {
            return addToCache(log);
        }

        this.cachedLogs.put(id, log);

        return id;
    }

    public String getLog(String id) {
        return cachedLogs.get(id);
    }

    private boolean isUsed(String id) {
        for (String all : cachedLogs.keySet()) {
            if (all.equalsIgnoreCase(id)) {
                return true;
            }
        }

        return false;
    }
}

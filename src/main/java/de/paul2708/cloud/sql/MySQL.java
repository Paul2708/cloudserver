package de.paul2708.cloud.sql;

import de.paul2708.cloud.logger.CloudLogger;
import lombok.Getter;

import java.sql.*;

/**
 * Created by Paul on 07.08.2016.
 */
public class MySQL {

    @Getter
    private Connection connection;
    @Getter
    private String host = "localhost", database, user, password;
    @Getter
    private int port = 3306;

    public MySQL(String host, String database, String user, String password) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public Connection openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password + "&autoReconnect=true");
            CloudLogger.info("SQL: Verbindung zur Datenbank '" + this.database + "' hergestellt");
        } catch (SQLException e) {
            CloudLogger.error("SQL: Verbindung zur Datenbank '" + this.database + "' konnte nicht hergestellt werden", e);
        } catch (ClassNotFoundException e) {
            CloudLogger.error("SQL: MySQL-Treiber wurde nicht gefunden", e);
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return this.connection;
    }

    public boolean hasConnection() {
        try {
            return (this.connection != null) || (this.connection.isValid(1));
        } catch (SQLException e) {
            CloudLogger.error("SQL: Fehler beim Abfragen der Verbindung", e);
        }

        return false;
    }

    public void queryUpdate(String query) {
        Connection con = MySQL.this.connection;
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(query);
            st.executeUpdate();
        } catch(SQLException e) {
            CloudLogger.error("SQL: Fehler beim Ausfuehren einer Query", e);
        } finally {
            MySQL.this.closeResources(null, st);
        }
    }

    public ResultSet getQuery(String query) {
        try {
            PreparedStatement stmt = this.connection.prepareStatement(query);
            return stmt.executeQuery();
        } catch(Exception e) {
            CloudLogger.error("SQL: Fehler beim Bekommen eines ResultSets", e);
        }

        return null;
    }

    public void closeResources(ResultSet rs, PreparedStatement st) {
        try {
            if(rs != null) rs.close();
            if(st != null) st.close();
        } catch (SQLException e) {
            CloudLogger.error("SQL: Fehler beim Schließen der Resourcen", e);
        }
    }

    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            CloudLogger.error("SQL: Fehler beim Schließen der Verbindung", e);
        } finally {
            this.connection = null;
        }
    }
}

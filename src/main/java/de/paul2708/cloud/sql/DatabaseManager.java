package de.paul2708.cloud.sql;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.common.abuse.Abuse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by Paul on 07.08.2016.
 */
public class DatabaseManager {

    // Tables
    public static void setupTables() {
        String query = "CREATE TABLE IF NOT EXISTS `players` ("
                + "`name` varchar(200) NOT NULL,"
                + "`uuid` varchar(200) NOT NULL,"
                + "`ip` varchar(200) NOT NULL,"
                + "`rank` varchar(200) NOT NULL,"
                + "`rank_end` varchar(200) NOT NULL,"
                + "`first_login` varchar(200) NOT NULL,"
                + "`last_login` varchar(200) NOT NULL,"
                + "`play_time` varchar(200) NOT NULL,"
                + "`teamspeak_uid` varchar(200) NOT NULL,"
                + "`ban_reason` varchar(200) NOT NULL,"
                + "`mute_reason` varchar(200) NOT NULL,"
                + "`ban_points` integer(200) NOT NULL,"
                + "`mute_points` integer(200) NOT NULL,"
                + "`tokens` integer(200) NOT NULL,"
                + "`notification` varchar(200) NOT NULL,"
                + "UNIQUE KEY `uuid` (`uuid`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `ranks` ("
                + "`name` varchar(200) NOT NULL,"
                + "`tag` varchar(200) NOT NULL,"
                + "`short_tag` varchar(200) NOT NULL,"
                + "`permission_level` integer(200) NOT NULL,"
                + "`permission` varchar(1000) NOT NULL,"
                + "`teamspeak_id` integer(200) NOT NULL,"
                + "UNIQUE KEY `name` (`name`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `blacklist` ("
                + "`name` varchar(200) NOT NULL,"
                + "UNIQUE KEY `name` (`name`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `abuses` ("
                + "`id` integer(200) NOT NULL AUTO_INCREMENT,"
                + "`action` varchar(200) NOT NULL,"
                + "`staff` varchar(200) NOT NULL,"
                + "`abuser` varchar(200) NOT NULL,"
                + "`reason` varchar(200) NOT NULL,"
                + "`timestamp` varchar(200) NOT NULL,"
                + "`last_to` varchar(200) NOT NULL,"
                + "`staff_uuid` varchar(200) NOT NULL,"
                + "`abuser_uuid` varchar(200) NOT NULL,"
                + "PRIMARY KEY (`id`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `beta_keys` ("
                + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                + "`code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,"
                + "`name` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,"
                + "`uuid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,"
                + "`timestamp` datetime DEFAULT NULL,"
                + "PRIMARY KEY (`id`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `skins` ("
                + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                + "`uuid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,"
                + "`value` TEXT COLLATE utf8_unicode_ci NOT NULL,"
                + "`signature` TEXT COLLATE utf8_unicode_ci NOT NULL,"
                + "PRIMARY KEY (`id`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        CloudServer.getMySQL().queryUpdate(query);
        query = "CREATE TABLE IF NOT EXISTS `nicklist` ("
                + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                + "`name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,"
                + "PRIMARY KEY (`id`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        CloudServer.getMySQL().queryUpdate(query);
    }

    // Rank
    public static void addRank(String name, String tag, String shortTag, int level, int groudId) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "INSERT IGNORE INTO `ranks` ("
                            + "`name`, `tag`, `short_tag`, `permission_level`, `permission`, `teamspeak_id`"
                            + ") VALUES ("
                            + "?, ?, ?, ?, ?, ?"
                            + ")");

            update.setString(1, name);
            update.setString(2, tag);
            update.setString(3, shortTag);
            update.setInt(4, level);
            update.setString(5, "");
            update.setInt(6, groudId);

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Rank: Fehler beim Speichern (" + name + ")", e);
        }
    }

    public static void removeRank(String name) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "DELETE FROM ranks WHERE name = ?");

            update.setString(1, name);

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Rank: Fehler beim Speichern (" + name + ")", e);
        }
    }

    // Permission
    public static boolean containsPermission(String rankName, String permission) {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT permission FROM `players` WHERE name = " + rankName);

        try {
            if (rs.next()) {
                String permissions = rs.getString("permission");

                for (String aPermission : permissions.split(";")) {
                    if (aPermission.equalsIgnoreCase(permission)) {
                        return true;
                    }
                }

                return false;
            }
        } catch (Exception e) {
            CloudLogger.error("Permission: Fehler beim Prüfen (" + permission + ")", e);
            return false;
        }

        return false;
    }

    public static void updatePermissions(String rankName) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "UPDATE ranks SET permission = ? WHERE name = ?");
            String permissionString = CloudServer.getRankManager().getRank(rankName).getPermission();
            update.setString(1, permissionString);
            update.setString(2, rankName);

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Permission: Fehler beim Speichern (" + rankName + ")", e);
        }
    }

    // Blacklist
    public static void addBlacklistName(String name) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "INSERT IGNORE INTO `blacklist` ("
                            + "`name`"
                            + ") VALUES ("
                            + "?"
                            + ")");
            update.setString(1, name);
            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Blacklist: Fehler beim Speichern (" + name + ")", e);
        }
    }

    public static void removeBlacklistName(String name) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "DELETE FROM blacklist WHERE name = ?");
            update.setString(1, name);
            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Blacklist: Fehler beim Löschen (" + name + ")", e);
        }
    }

    // Abuse
    public static void addAbuse(Abuse abuse) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "INSERT IGNORE INTO `abuses` ("
                            + "`action`, `staff`, `abuser`, `reason`, `timestamp`, `last_to`, `staff_uuid`, `abuser_uuid`"
                            + ") VALUES ("
                            + "?, ?, ?, ?, ?, ?, ?, ?"
                            + ")");
            update.setString(1, abuse.getAction().getData());
            update.setString(2, abuse.getStaff());
            update.setString(3, abuse.getAbuser());
            update.setString(4, abuse.getReason());
            update.setString(5, abuse.getTimeStamp() + "");
            update.setString(6, abuse.getLastTo() + "");
            update.setString(7, abuse.getStaffUuid().toString());
            update.setString(8, abuse.getAbuserUuid().toString());

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Abuse: Fehler beim Speichern (" + abuse.getAction() + ")", e);
        }
    }

    // Player
    public static void saveRank(Player player) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "UPDATE players SET rank = ?, rank_end = ? WHERE uuid = ?");

            update.setString(1, player.getRank().getName());
            update.setString(2, player.getRankEnd() + "");
            update.setString(3, player.getUuid());

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Rank: Fehler beim Speichern (" + player.getName() + ")", e);
        }
    }

    public static void saveBanReason(Player player) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "UPDATE players SET ban_reason = ? WHERE uuid = ?");

            update.setString(1, player.getBanReason());
            update.setString(2, player.getUuid());

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Abuse: Fehler beim Speichern des Bangrundes (" + player.getName() + ")", e);
        }
    }

    public static void saveMuteReason(Player player) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "UPDATE players SET mute_reason = ? WHERE uuid = ?");

            update.setString(1, player.getMuteReason());
            update.setString(2, player.getUuid());

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Abuse: Fehler beim Speichern des Mutegrundes (" + player.getName() + ")", e);
        }
    }

    // Key
    public static void addKey(String key) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "INSERT IGNORE INTO `beta_keys` ("
                            + "`code`, `name`, `uuid`, `timestamp`"
                            + ") VALUES ("
                            + "?, ?, ?, ?"
                            + ")");
            update.setString(1, key);
            update.setNull(2, Types.VARCHAR);
            update.setNull(3, Types.VARCHAR);
            update.setNull(4, Types.DATE);

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Key: Fehler beim Speichern (" + key + ")", e);
        }
    }

    // Skin
    public static void addSkin(String uuid, String value, String signature) {
        try {
            PreparedStatement update = CloudServer.getMySQL().getConnection().prepareStatement(
                    "INSERT IGNORE INTO `skins` ("
                            + "`uuid`, `value`, `signature`"
                            + ") VALUES ("
                            + "?, ?, ?"
                            + ")");
            update.setString(1, uuid);
            update.setString(2, value);
            update.setString(3, signature);

            update.executeUpdate();
        } catch (Exception e) {
            CloudLogger.error("Nick: Fehler beim Speichern des Skins (" + uuid + ")", e);
        }
    }
}

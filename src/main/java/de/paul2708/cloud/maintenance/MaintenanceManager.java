package de.paul2708.cloud.maintenance;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.other.MaintenancePacket;

/**
 * Created by Paul on 06.09.2016.
 */
public class MaintenanceManager {

    private boolean started = false;

    public void start(long startIn, String reason) {
        started = true;

        Thread thread = new Thread(() -> {
            CloudServer.getPacketServer().send(ClientType.SPIGOT,
                    new MaintenancePacket("start", reason, startIn));
            CloudLogger.info("Maintenance: Wartungen in " + (startIn/60/1000) + " Minuten");

            try {
                Thread.sleep(startIn);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            started = false;
            activate(reason);
            CloudServer.getPlayerManager().kickAll(CloudServer.getMaintenanceFile().getReason());
        });
        thread.start();
    }

    public void activate(String reason) {
        CloudServer.getMaintenanceFile().set("enabled", true);
        CloudServer.getMaintenanceFile().set("reason", reason);
        CloudServer.getMaintenanceFile().save();
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new MaintenancePacket("enable", reason, -1L));

        CloudLogger.info("Maintenance: Wartungen wurden aktiviert. Grund: " + reason);
    }

    public void disable() {
        CloudServer.getMaintenanceFile().set("enabled", false);
        CloudServer.getMaintenanceFile().set("reason", "none");
        CloudServer.getMaintenanceFile().save();
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new MaintenancePacket("disable", "", -1L));
        CloudServer.getPacketServer().send(ClientType.SPIGOT,
                new MaintenancePacket("disable", "", -1L));

        CloudLogger.info("Maintenance: Wartungen wurden beendet");
    }

    public boolean isStarting() {
        return started;
    }
}

package de.paul2708.cloud.twitter;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Paul on 12.11.2016.
 */
public class TwitterManager {

    private Twitter twitter;

    public void login() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(CloudServer.getAccountFile().getTwitterConsumerKey())
                .setOAuthConsumerSecret(CloudServer.getAccountFile().getTwitterConsumerSecret())
                .setOAuthAccessToken(CloudServer.getAccountFile().getTwitterAccessToken())
                .setOAuthAccessTokenSecret(CloudServer.getAccountFile().getTwitterAccessTokenSecret());

        TwitterFactory tf = new TwitterFactory(cb.build());
        this.twitter = tf.getInstance();
    }

    public void post(Player player, String message) {
        try {
            String token;

            switch (player.getUuid()) {
                case "c46381c4-0e69-4621-92c4-a6b2712c1c86":
                    token = "^MT";
                    break;
                case "5d7031f2-5440-43ac-a14f-e25f2f0141bc":
                    token = "^DN";
                    break;
                case "c89b0629-b73a-4b4a-ac7e-e531f115d1d7":
                    token = "^KJ";
                    break;
                default:
                    token = "^C";
                    break;
            }

            int length = (message + " " + token).length();
            if (length > 140) {
                player.sendMessage(CloudServer.TAG + "Der Tweet ist zu lange. (" + length + " > 140)");
                return;
            }

            twitter.updateStatus(message + " " + token);

            player.sendMessage(CloudServer.TAG + "§e...und wurde §babgeschickt§e.");

            CloudLogger.info("Twitter: " + message);
        } catch (TwitterException e) {
            CloudLogger.error("Twitter: Fehler beim Posten", e);
            player.sendMessage(CloudServer.TAG + "§cEs ist ein Fehler aufgetreten. " +
                    "(" + e.getErrorCode() + ": " + e.getErrorMessage() + ")");
        }
    }
}

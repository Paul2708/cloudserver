package de.paul2708.cloud.nick;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.nick.Skin;
import de.paul2708.common.packet.player.PlayerNickPacket;
import lombok.Getter;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 28.05.2017.
 */
public class NickManager {

    // TODO: add random skins (from bungee?)
    // TODO: random nick

    @Getter
    private List<Nick> nicks = new CopyOnWriteArrayList<>();

    @Getter
    private List<Skin> skins = new CopyOnWriteArrayList<>();

    @Getter
    private Map<Player, Nick> nickedPlayers = new ConcurrentHashMap<>();

    public void resolveNicks() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `nicklist`");

        try {
            while (rs.next()) {
                String name = rs.getString("name");

                nicks.add(new Nick(name, null));
            }

            CloudLogger.info("Nick: " + nicks.size() + " Namen wurden geladen");
        } catch (Exception e) {
            CloudLogger.error("Nick: Fehler beim Bekommen der Namen", e);
        }
    }

    public void resolveSkins() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `skins`");

        try {
            while (rs.next()) {
                String uuid = rs.getString("uuid");
                String value = rs.getString("value");
                String signature = rs.getString("signature");

                // Convert string to UUID
                StringBuilder str = new StringBuilder(uuid);

                str.insert(8, '-');
                str.insert(13, '-');
                str.insert(18, '-');
                str.insert(23, '-');

                // Add skin
                skins.add(new Skin(UUID.fromString(str.toString()), value, signature));
            }
        } catch (Exception e) {
            CloudLogger.error("Nick: Fehler beim Bekommen der Skins", e);
        }
    }

    public void generateSkin(String uuid) {
        uuid = uuid.replaceAll("-", "");

        try {
            String website = Util.getText("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");

            JsonObject object = Jsoner.deserialize(website, new JsonObject());
            JsonArray array = (JsonArray) object.get("properties");
            JsonObject properties = (JsonObject) array.get(0);

            CloudLogger.info("Name: " + object.get("name"));

            DatabaseManager.addSkin(uuid, properties.getString("value"), properties.getString("signature"));
        } catch (Exception e) {
            CloudLogger.error("Nick: Skin (" + uuid + ") konnte nicht geladen werden.", e);
        }
    }

    public void nick(Player player, Nick nick) {
        nick.setSkin(getRandomSkin());

        nickedPlayers.put(player, nick);

        player.getServer().sendPacket(new PlayerNickPacket(UUID.fromString(player.getUuid()), nick));
    }

    public void unnick(Player player) {
        nickedPlayers.remove(player);

        player.getServer().sendPacket(new PlayerNickPacket(UUID.fromString(player.getUuid()), new Nick("", null)));
    }

    public Nick getNick(Player player) {
        return nickedPlayers.get(player);
    }

    public boolean isNicked(Player player) {
        return nickedPlayers.containsKey(player) && nickedPlayers.get(player) != null;
    }

    public Nick getFreeNick() {
        for (Nick all : nicks) {
            boolean found = false;

            for (Nick used : nickedPlayers.values()) {
                if (used.equals(all)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                return all;
            }
        }

        return null;
    }

    private Skin getRandomSkin() {
        if (skins.size() == 0) {
            return null;
        }

        int index = Util.random.nextInt(skins.size());

        return skins.get(index);
    }
}

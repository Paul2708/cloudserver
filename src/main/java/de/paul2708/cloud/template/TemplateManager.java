package de.paul2708.cloud.template;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.file.TemplateFile;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.server.SpigotServer;
import de.paul2708.cloud.util.Util;
import de.paul2708.common.packet.server.ServerKillPacket;
import lombok.Getter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 02.11.2016.
 */
public class TemplateManager {

    // TODO: Block ports Verbieten: ufw deny <Port>/udp Verbieten: ufw deny <Port>/tcp

    private List<Template> templates = new CopyOnWriteArrayList<>();

    @Getter
    private Map<String, Integer> pids = new ConcurrentHashMap<>();
    @Getter
    private List<String> killedServer = new CopyOnWriteArrayList<>();
    @Getter
    // Port: free to use
    private Map<Integer, Boolean> ports = new ConcurrentHashMap<>();

    private Queue queue;

    public void resolveTemplates() {
        createFile();

        int minPort = CloudServer.getConfigFile().getMinPort();
        int maxPort = CloudServer.getConfigFile().getMaxPort();

        for (int i = minPort; i <= maxPort; i++) {
            ports.put(i, true);
        }

        for (String name : getFiles()) {
            TemplateFile file = new TemplateFile(name);
            Template template = new Template(file.getName(), file.getRAM(), file.getMinServer(), file.getMaxServer());

            templates.add(template);
        }

        createLogFiles();

        CloudLogger.info("Template: " + templates.size() + " Templates wurden geladen");

        this.queue = new Queue();
        this.queue.start();
    }

    public void startAll() {
        int i = 0;
            for (Template template : templates) {
            int min = template.getMinServer();

            for (SpigotServer server : CloudServer.getServerManager().getServers()) {
                if (server.getTemplate() != null && template.getName().equalsIgnoreCase(server.getTemplate().getName())) {
                    i++;
                }
            }

            if (i < min) {
                for (int j = 0; j < (min - i); j++) {
                    int port = getFreePort();
                    if (port == -1) {
                        CloudLogger.info("Template: kein freier Port verfügbar");
                    } else {
                        template.start(j + 1 + i, getFreePort());
                    }
                }
            }

            i = 0;
        }
    }

    public void start(Template template, int amount) {
        int count = getServerCount(template);
        for (int i = 0; i < amount; i++) {
            int port = getFreePort();
            if (port == -1) {
                CloudLogger.info("Template: kein freier Port verfügbar");
            } else {
                count++;
                template.start(count, getFreePort());
            }
        }
    }

    public void kill(Template template) {
        for (SpigotServer server : CloudServer.getServerManager().getServers()) {
            if (server.getTemplate() != null && template.getName().equalsIgnoreCase(server.getTemplate().getName())) {
                this.killedServer.add(server.getName());
                server.sendPacket(new ServerKillPacket(server.getName()));
            }
        }
    }

    public void killAndDelete(SpigotServer server, boolean kill, boolean delete) {
        int pid = pids.get(server.getName());

        String[] killCommand = new String[] {
                "kill",
                "-9",
                pid + ""
        };
        String[] deleteCommand = new String[] {
                "bash",
                "-c",
                "rm -r " + CloudServer.getConfigFile().getPathToCloud() + "running/" + server.getName()
        };
        try {
            Process pKill = Runtime.getRuntime().exec(killCommand);
            pKill.waitFor();
            Process pDelete = Runtime.getRuntime().exec(deleteCommand);
            pDelete.waitFor();

            // Files.delete(Paths.get(CloudServer.getConfigFile().getPathToCloud() + "running/" + server.getName()));

            CloudLogger.info("Template: " + server.getName() + " wurde gekillt und gelöscht");
        } catch (Exception e) {
            CloudLogger.error("Template: Fehler beim Stoppen und Löschen (" + server.getName() + ")", e);
        }
    }

    public void copyLog(String name) {
        try {
            String template = CloudServer.getServerManager().getServer(name).getTemplate().getName();

            String[] command = new String[] {
                    "bash",
                    "-c",
                    "cp -r " + CloudServer.getConfigFile().getPathToCloud() + "running/" + name + "/logs/latest.log " +
                    "-R " + CloudServer.getConfigFile().getPathToCloud() + "serverlogs/" + template
            };
            Process copy = Runtime.getRuntime().exec(command);
            copy.waitFor();

            Files.move(Paths.get("running/" + name + "/logs/latest.log"), Paths.get("serverlogs/" + template + "/" + name + " " + Util.getTime(System.currentTimeMillis()) + ".log"),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getPID(String server) {
        if (pids.containsKey(server)) {
            return pids.get(server);
        }

        return -1;
    }

    public Template getTemplate(String name) {
        for (Template template : templates) {
            if (template.getName().equalsIgnoreCase(name)) {
                return template;
            }
        }

        return null;
    }

    public int getServerCount(Template template) {
        int count = 0;
        for (SpigotServer server : CloudServer.getServerManager().getServers()) {
            if (server.getTemplate() != null && template.getName().equalsIgnoreCase(server.getTemplate().getName())) {
                count++;
            }
        }

        return count;
    }

    public Template getTemplate(SpigotServer server) {
        for (Template template : templates) {
            if (server.getName().startsWith(template.getName())) {
                return template;
            }
        }

        return null;
    }

    public Queue getQueue() {
        return queue;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    private void createFile() {
        Path path = Paths.get("templates");

        try {
            // Create dirs
            if (!Files.exists(Paths.get("templates"))) {
                Files.createDirectories(Paths.get("templates"));
            }
            if (!Files.exists(Paths.get("running"))) {
                Files.createDirectories(Paths.get("running"));
            }
            if (!Files.exists(Paths.get("static"))) {
                Files.createDirectories(Paths.get("static"));
            }
            if (!Files.exists(Paths.get("serverlogs"))) {
                Files.createDirectories(Paths.get("serverlogs"));
            }

            // Create file
            if (path.toFile().list().length == 0) {
                new TemplateFile("example");
            }

            // Clear
            String[] command = new String[] {
                    "bash",
                    "-c",
                    "rm -r " + CloudServer.getConfigFile().getPathToCloud() + "running/*"
            };
            Process delete = Runtime.getRuntime().exec(command);
            delete.waitFor();

        } catch (Exception e) {
            CloudLogger.error("Template: Ordner konnte nicht erstellt werden", e);
        }
    }

    private void createLogFiles() {
        try {
            for (Template template : templates) {
                if (!Files.exists(Paths.get("serverlogs/" + template.getName()))) {
                    Files.createDirectories(Paths.get("serverlogs/" + template.getName()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getFreePort() {
        List<Integer> keys = new ArrayList<>(ports.keySet());
        Collections.sort(keys);

        int freePort = -1;

        for (int i = 0; i < keys.size(); i++) {
            int port = keys.get(i);

            if (ports.get(port)) {
                freePort = port;
                break;
            }
        }

        if (freePort != -1) {
            ports.put(freePort, false);
        }

        return freePort;
    }

    private List<String> getFiles() {
        List<String> names = new ArrayList<>();
        Path path = Paths.get("templates");

        try {
            Files.list(path).filter(p -> !p.toFile().isDirectory()).forEach(p -> {
                names.add(Util.removeExtension(p.toFile().getName()));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

}

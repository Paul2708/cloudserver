package de.paul2708.cloud.template;

/**
 * Created by Paul on 21.02.2017.
 */
abstract public class StartRunnable implements Runnable {

    private String name;
    // 0 = not started, 1 = running, 2 = done
    private int status;

    public StartRunnable(String name) {
        this.name = name;
        this.status = 0;
    }

    public void status(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }
}

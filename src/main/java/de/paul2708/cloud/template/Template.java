package de.paul2708.cloud.template;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.util.Util;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Paul on 02.11.2016.
 */
public class Template {

    private String name;
    private int ram;
    private int minServer;
    private int maxServer;

    private String path;

    public Template(String name, int ram, int minServer, int maxServer) {
        this.name = name;
        this.ram = ram;
        this.minServer = minServer;
        this.maxServer = maxServer;
    }

    public void start(int id, int port) {
        String serverName = this.name + "-" + id;
        CloudLogger.info("Template: neuer " + name + " Server wird erstellt...");

        StartRunnable startRunnable = new StartRunnable(serverName) {

            @Override
            public void run() {
                status(1);

                try {
                    // Copy
                    Path templatePath = Paths.get("templates/" + name);
                    Path serverPath = Paths.get(path = "running/" + serverName);
                    Util.copyFiles(templatePath, serverPath);

                    // Edit
                    // CloudSpigot
                    List<String> lines = Files.readAllLines(Paths.get(path + "/plugins/CloudSpigot/config.yml"));
                    lines.remove(0);
                    lines.add(0, "server_name: '" + serverName + "'");
                    lines.remove(2);
                    lines.add(2, "port: " + port + "");
                    Files.write(Paths.get(path + "/plugins/CloudSpigot/config.yml"), lines);

                    // Start server
                    String[] command = new String[] {
                            "screen",
                            "-dmS",
                            serverName.toLowerCase(),
                            "java",
                            "-Xms512M",
                            "-Xmx" + ram + "M",
                            "-Dcom.mojang.eula.agree=true",
                            "-jar",
                            "spigot.jar",
                            "-p" + port,
                    };

                    Process process = new ProcessBuilder(command).directory(new File("./running/" + serverName)).start();

                    Class<?> cProcessImpl = process.getClass();
                    Field fPid = cProcessImpl.getDeclaredField("pid");
                    if (!fPid.isAccessible()) {
                        fPid.setAccessible(true);
                    }

                    int processID = fPid.getInt(process);
                    CloudServer.getTemplateManager().getPids().put(serverName, processID);

                    CloudLogger.info("Template: " + serverName + " (port=" + port + ", pid=" + processID + ") wurde gestartet");
                    status(2);
                } catch (Exception e) {
                    CloudLogger.error("Template: Fehler beim Starten des Servers " + serverName, e);
                }
            }
        };

        CloudServer.getTemplateManager().getQueue().add(startRunnable);
        // Logger.log("[Queue] Server " + serverName + " wurde der Liste hinzugefügt.");
    }

    public String getName() {
        return name;
    }

    public int getRam() {
        return ram;
    }

    public int getMinServer() {
        return minServer;
    }

    public int getMaxServer() {
        return maxServer;
    }
}

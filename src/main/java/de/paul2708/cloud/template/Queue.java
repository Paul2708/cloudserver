package de.paul2708.cloud.template;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 21.02.2017.
 */
public class Queue extends Thread {

    private List<StartRunnable> queue = new CopyOnWriteArrayList<>();

    @Override
    public void run() {
        while (true) {
            if (queue.size() > 0) {
                StartRunnable runnable = queue.get(0);

                switch (runnable.getStatus()) {
                    case 0:
                        runnable.run();
                        // Logger.log("[Queue] Server " + runnable.getName() + " wird gestartet...");
                        break;
                    case 1:
                        break;
                    case 2:
                        queue.remove(runnable);
                        break;
                }
            }

            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void add(StartRunnable runnable) {
        queue.add(runnable);
    }
}

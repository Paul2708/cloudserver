package de.paul2708.cloud.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.player.Player;
import de.paul2708.cloud.template.Template;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.server.ServerCommandPacket;
import de.paul2708.common.server.ServerData;
import lombok.Getter;
import lombok.Setter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 25.08.2016.
 */
public class SpigotServer {

    @Getter
    private String name;

    @Getter
    private String ipName;

    @Getter
    private InetAddress ip;

    @Getter
    private int port;

    @Getter
    private boolean gameServer;

    @Getter
    private boolean listed;

    @Getter
    @Setter
    private int onlinePlayers, maxPlayers;

    @Getter
    @Setter
    private String motd;

    @Getter
    @Setter
    private GameState gameState;

    @Getter
    @Setter
    private boolean joinAble;

    @Getter
    @Setter
    private Template template;

    @Getter
    private Session session;

    @Getter
    private List<Player> players;

    public SpigotServer(String name, String ip, int port, boolean listed, boolean gameServer, Session session) {
        this.name = name;
        this.ipName = ip;
        
        try {
            this.ip = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            CloudLogger.error("Server: Adresse " + ip + ":" + port + " konnte nicht gefunden werden", e);
        }

        this.gameServer = gameServer;
        this.listed = listed;
        this.port = port;
        this.session = session;

        this.gameState = GameState.NONE;

        this.joinAble = true;

        this.motd = "default cloud spigot motd";

        this.template = CloudServer.getTemplateManager().getTemplate(this);

        this.players = new CopyOnWriteArrayList<>();
    }

    public void command(String command) {
        sendPacket(new ServerCommandPacket(name, command));
    }

    public void addPlayer(UUID uuid) {
        Player player = CloudServer.getPlayerManager().getPlayer(uuid);
        if (player == null) {
            CloudLogger.info("Server: " + uuid + " ist nicht bekannt (" + name + ")");
            return;
        }

        players.add(player);
        onlinePlayers = players.size();
    }

    public void removePlayer(UUID uuid) {
        Player player = CloudServer.getPlayerManager().getPlayer(uuid);
        if (player == null) {
            CloudLogger.info("Server: " + uuid + " ist nicht bekannt (" + name + ")");
            return;
        }

        players.remove(player);
        onlinePlayers = players.size();
    }

    public void sendPacket(Packet packet) {
        session.send(packet);
    }

    public ServerData getData() {
        return new ServerData(name, onlinePlayers, maxPlayers, motd, gameState);
    }
}

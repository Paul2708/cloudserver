package de.paul2708.cloud.server;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.common.game.GameState;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.command.BroadcastPacket;
import de.paul2708.common.packet.server.AddServerPacket;

import java.io.File;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 25.08.2016.
 */
public class ServerManager {

    private List<SpigotServer> servers = new CopyOnWriteArrayList<>();

    private boolean restarted;

    public void addServer(SpigotServer server) {
        servers.add(server);

        if (!server.isListed()) {
            CloudServer.getPacketServer().send(ClientType.BUNGEE,
                    new AddServerPacket(server.getName(), server.getIpName(), server.getPort()));

            CloudLogger.info("Server: " + server.getName() + " (" + server.getIp() + ":" + server.getPort() + ") wurde hinzugefügt");
        } else {
            CloudLogger.info("Server: " + server.getName() + " (" + server.getIp() + ":" + server.getPort() + ") ist bereits eingetragen");
        }
    }

    public void removeServer(String name) {
        SpigotServer server = getServer(name);

        CloudLogger.info("Server: " + server.getName() + " (" + server.getIp() + ":" + server.getPort() + ") wurde abgemeldet");

        if (server.getTemplate() != null) {
            CloudServer.getTemplateManager().copyLog(server.getName());

            if (server.isGameServer()) {
                if (CloudServer.getTemplateManager().getKilledServer().contains(server.getName())) {
                    CloudServer.getTemplateManager().getPorts().put(server.getPort(), true);

                    CloudServer.getTemplateManager().killAndDelete(server, true, true);

                    CloudServer.getTemplateManager().getKilledServer().remove(server.getName());
                } else {
                    startAgain(server);
                }
            } else {
                CloudServer.getTemplateManager().getPorts().put(server.getPort(), true);

                CloudServer.getTemplateManager().killAndDelete(server, true, true);
            }
        }

        servers.remove(server);
    }

    public void startRestartObserver() {
        this.restarted = false;

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                int hour = LocalDateTime.now().getHour();

                if (hour == 3 || hour == 4) {
                    CloudServer.getPacketServer().send(ClientType.BUNGEE,
                            new BroadcastPacket("§cUm 5 Uhr werden alle Game-Server neu gestartet."));
                } else if (hour == 5 && !restarted) {
                    getServers().stream().filter(SpigotServer::isGameServer).forEach(server -> server.command("stop"));

                    restarted = true;
                } else if (hour == 6 && restarted) {
                    restarted = false;
                }
            }
        }, TimeUnit.MINUTES.toMillis(45), TimeUnit.MINUTES.toMillis(45));
    }

    public SpigotServer getLobby() {
        for (SpigotServer server : servers) {
            if (isLobbyServer(server)) {
                return server;
            }
        }

        return null;
    }

    public boolean isLobbyServer(SpigotServer server) {
        return server.getName().startsWith(CloudServer.getConfigFile().getLobbyTag());
    }

    public SpigotServer getServer(String name) {
        for (SpigotServer server : servers) {
            if (server.getName().equalsIgnoreCase(name)) {
                return server;
            }
        }

        return null;
    }

    public SpigotServer getAvailableServer(String group) {
        for (SpigotServer server : servers) {
            if (server.getName().startsWith(group)) {
                if (server.getGameState() == null ||
                        server.getGameState() != GameState.INGAME) {
                    return server;
                }
            }
        }

        return null;
    }

    public List<SpigotServer> getServers() {
        return servers;
    }

    private void startAgain(SpigotServer server) {
        // Start server
        String[] command = new String[] {
                "screen",
                "-dmS",
                server.getName().toLowerCase(),
                "java",
                "-Xms512M",
                "-Xmx" + server.getTemplate().getRam() + "M",
                "-Dcom.mojang.eula.agree=true",
                "-jar",
                "spigot.jar",
                "-p" + server.getPort(),
        };

        try {
            Process process = new ProcessBuilder(command).directory(new File("./running/" + server.getName())).start();

            Class<?> cProcessImpl = process.getClass();
            Field fPid = cProcessImpl.getDeclaredField("pid");
            if (!fPid.isAccessible()) {
                fPid.setAccessible(true);
            }

            int processID = fPid.getInt(process);
            CloudServer.getTemplateManager().getPids().put(server.getName(), processID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

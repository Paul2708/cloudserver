package de.paul2708.cloud.blacklist;

import de.paul2708.cloud.CloudServer;
import de.paul2708.cloud.logger.CloudLogger;
import de.paul2708.cloud.sql.DatabaseManager;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.other.BlacklistPacket;
import lombok.Getter;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 05.09.2016.
 */
public class BlacklistManager {

    @Getter
    private List<String> names = new CopyOnWriteArrayList<>();

    public void resolveNames() {
        ResultSet rs = CloudServer.getMySQL().getQuery("SELECT * FROM `blacklist`");

        try {
            while (rs.next()) {
                String name = rs.getString("name");
                names.add(name);
            }

            CloudLogger.info("Blacklist: " + names.size() + " Namen geladen");
        } catch (Exception e) {
            CloudLogger.error("Blacklist: Fehler beim Bekommen der Namen", e);
        }
    }

    public void addName(String name) {
        names.add(name);
        DatabaseManager.addBlacklistName(name);
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new BlacklistPacket("add", name));

        CloudLogger.info("Blacklist: Added " + name);
    }

    public void removeName(String name) {
        names.remove(name);
        DatabaseManager.removeBlacklistName(name);
        CloudServer.getPacketServer().send(ClientType.BUNGEE,
                new BlacklistPacket("remove", name));

        CloudLogger.info("Blacklist: Removed " + name);
    }

    public String correct(String name) {
        for (String n : names) {
            if (n.equalsIgnoreCase(name)) {
                return n;
            }
        }

        return name;
    }

    public ArrayList<String> getNamesAsArrayList() {
        ArrayList<String> list = new ArrayList<>();
        names.forEach(list::add);
        return list;
    }
}
